#!/usr/bin/env python
from sqlalchemy import or_, func, exc, text
from datetime import datetime
import uuid
from core_app_middleware.models.database import suite_crm_db_session, kyc_db_session,isf_uat_db_session, apex_db_session, premia_db_session, actisure_db_session
from core_app_middleware.models.kyc_models import Customer as KYCCustomer, CustomerDetails as KYCCustomerDetails, Policy as KYCPolicy, PaymentDetails
from core_app_middleware.models.suite_crm_models import ACLActions, ACLRoles, ACLRolesActions, User, ASOLCommonConfig, Lead, Case, CaseCSTM, Account, AccountCSTM, AccountsOpportunities, EmailAddress, EmailAddressBeanRel, Policy, PolicyAccount, Payments, PaymentAccount, Communications, PolicyCSTM
from core_app_middleware.models.isf_models import LifePolicy
from core_app_middleware.models.apex_models import PensionAccountInfo
from core_app_middleware.models.premia_models import PolicyDetails
from core_app_middleware.models.actisure_models import MedicalInformation

def load_cases_from_db():
    cases = Case.query.all()
    for case in cases:
        payload = {}

        id = case.id
        name = case.name
        date_entered = case.date_entered
        date_modified = case.date_modified
        modified_user_id = case.modified_user_id
        created_by = case.created_by
        description = case.description
        assigned_user_id = case.assigned_user_id
        case_number = case.case_number
        type = case.type
        status = case.status
        priority = case.priority
        resolution = case.resolution
        work_log = case.work_log
        account_id = case.account_id
        state = case.state
        contact_created_by_id = case.contact_created_by_id
        crm_regn_ctry_name = case.crm_regn_ctry_name

        if status == 'Closed':
            print ("payload is " + str(case) + status)


def capitalize_crm_accounts_names(offset_from, offset_to):
    for offset in range(offset_from, offset_to):
        #offset = 60700
        accounts = Account.query.offset(offset).limit(1).all()
        for account in accounts:
            payload = {}

            id = account.id
            name = account.name
            date_entered = account.date_entered
            date_modified = account.date_modified
            modified_user_id = account.modified_user_id
            created_by = account.created_by
            description = account.description
            assigned_user_id = account.assigned_user_id
            mobile_phone = account.mobile_phone
            crm_regn_ctry_name = account.crm_regn_ctry_name

            upper_cased_name = name.upper()
            suite_crm_db_session.query(Account).filter_by(id=id).update({"name": upper_cased_name})
            suite_crm_db_session.commit()

            #print ("payload is " + str(account) + " account name is " + upper_cased_name)
            print ("payload is " + str(id) + " name is " + upper_cased_name)


def remove_special_char_from_crm_accounts_names():
    accounts = Account.query.filter(Account.name.like("-%")).all()
    for account in accounts:
        payload = {}

        id = account.id
        name = account.name
        date_entered = account.date_entered
        date_modified = account.date_modified
        modified_user_id = account.modified_user_id
        created_by = account.created_by
        description = account.description
        assigned_user_id = account.assigned_user_id
        mobile_phone = account.mobile_phone
        crm_regn_ctry_name = account.crm_regn_ctry_name

        name_without_dash = name.replace("-", "")
        suite_crm_db_session.query(Account).filter_by(id=id).update({"name": name_without_dash})
        suite_crm_db_session.commit()

        #print ("payload is " + str(account) + " account name is " + upper_cased_name)
        print ("payload is " + str(id) + " name is " + name + " name_without_dash " + name_without_dash)


    accounts = Account.query.filter(Account.name.like("%-")).all()
    for account in accounts:
        payload = {}

        id = account.id
        name = account.name
        date_entered = account.date_entered
        date_modified = account.date_modified
        modified_user_id = account.modified_user_id
        created_by = account.created_by
        description = account.description
        assigned_user_id = account.assigned_user_id
        mobile_phone = account.mobile_phone
        crm_regn_ctry_name = account.crm_regn_ctry_name

        name_without_dash = name.replace("-", "")
        suite_crm_db_session.query(Account).filter_by(id=id).update({"name": name_without_dash})
        suite_crm_db_session.commit()

        #print ("payload is " + str(account) + " account name is " + upper_cased_name)
        print ("payload is " + str(id) + " name is " + name + " name_without_dash " + name_without_dash)


    accounts = Account.query.filter(Account.name.like("%.%")).all()
    for account in accounts:
        payload = {}

        id = account.id
        name = account.name
        date_entered = account.date_entered
        date_modified = account.date_modified
        modified_user_id = account.modified_user_id
        created_by = account.created_by
        description = account.description
        assigned_user_id = account.assigned_user_id
        mobile_phone = account.mobile_phone
        crm_regn_ctry_name = account.crm_regn_ctry_name

        name_without_dot = name.replace(".", "")
        suite_crm_db_session.query(Account).filter_by(id=id).update({"name": name_without_dot})
        suite_crm_db_session.commit()

        #print ("payload is " + str(account) + " account name is " + upper_cased_name)
        print ("payload is " + str(id) + " name is " + name + " name_without_dot " + name_without_dot)


def add_07_to_phone_numbers_begining_with_7():
    accounts = Account.query.filter(Account.mobile_phone.like("7%")).all()
    for account in accounts:
        payload = {}

        id = account.id
        name = account.name
        date_entered = account.date_entered
        date_modified = account.date_modified
        modified_user_id = account.modified_user_id
        created_by = account.created_by
        description = account.description
        assigned_user_id = account.assigned_user_id
        mobile_phone = account.mobile_phone
        crm_regn_ctry_name = account.crm_regn_ctry_name

        mobile_number_with_07 = "0" + mobile_phone
        suite_crm_db_session.query(Account).filter_by(id=id).update({"mobile_phone": mobile_number_with_07})
        suite_crm_db_session.commit()

        #print ("payload is " + str(account) + " account name is " + upper_cased_name)
        print ("payload is " + str(id) + " mobile_phone is " + mobile_phone + " name_without_dash " + mobile_number_with_07)


def remove_special_char_from_phone_numbers():
    #accounts = Account.query.filter(Account.mobile_phone.like("-%")).filter(Account.mobile_phone.like("07%")).all()
    accounts = Account.query.filter(Account.mobile_phone.like("07%")).filter(or_(Account.mobile_phone.like("% %"),
                                                          Account.mobile_phone.like("%-%"),
                                                          Account.mobile_phone.like("%.%"),
                                                          Account.mobile_phone.like("%,%"),
                                                          Account.mobile_phone.like("%;%"),
                                                          Account.mobile_phone.like("%<%"),
                                                          Account.mobile_phone.like("%>%"))).limit(50).all()
    for account in accounts:
        payload = {}

        id = account.id
        name = account.name
        date_entered = account.date_entered
        date_modified = account.date_modified
        modified_user_id = account.modified_user_id
        created_by = account.created_by
        description = account.description
        assigned_user_id = account.assigned_user_id
        mobile_phone = account.mobile_phone
        crm_regn_ctry_name = account.crm_regn_ctry_name

        mobile_number_with_no_special_char = mobile_phone.replace(" ", "").replace("-", "").replace(".", "").replace(";", "").replace(">", "").replace("<", "").replace(",", "")
        suite_crm_db_session.query(Account).filter_by(id=id).update({"mobile_phone": mobile_number_with_no_special_char})
        suite_crm_db_session.commit()

        #print ("payload is " + str(account) + " account name is " + upper_cased_name)
        print ("payload is " + str(id) + " mobile_phone is " + mobile_phone + " name_without_dash " + mobile_number_with_no_special_char)



def remove_special_char_from_254_phone_numbers():

    accounts = Account.query.filter(Account.mobile_phone.like("+254%")).filter(or_(Account.mobile_phone.like("% %"),
                                                          Account.mobile_phone.like("%-%"),
                                                          Account.mobile_phone.like("%.%"),
                                                          Account.mobile_phone.like("%,%"),
                                                          Account.mobile_phone.like("%;%"),
                                                          Account.mobile_phone.like("%<%"),
                                                          Account.mobile_phone.like("%>%"))).all()
    for account in accounts:
        payload = {}

        id = account.id
        name = account.name
        date_entered = account.date_entered
        date_modified = account.date_modified
        modified_user_id = account.modified_user_id
        created_by = account.created_by
        description = account.description
        assigned_user_id = account.assigned_user_id
        mobile_phone = account.mobile_phone
        crm_regn_ctry_name = account.crm_regn_ctry_name

        mobile_number_with_no_special_char = "0" + mobile_phone.replace("+254", "").replace(" ", "").replace("-", "").replace(".", "").replace(";", "").replace(">", "").replace("<", "").replace(",", "")
        suite_crm_db_session.query(Account).filter_by(id=id).update({"mobile_phone": mobile_number_with_no_special_char})
        suite_crm_db_session.commit()

        #print ("payload is " + str(account) + " account name is " + upper_cased_name)
        print ("payload is " + str(id) + " mobile_phone is " + mobile_phone + " name_without_dash " + mobile_number_with_no_special_char)


    accounts = Account.query.filter(Account.mobile_phone.like("254%")).all()
    for account in accounts:
        payload = {}

        id = account.id
        name = account.name
        date_entered = account.date_entered
        date_modified = account.date_modified
        modified_user_id = account.modified_user_id
        created_by = account.created_by
        description = account.description
        assigned_user_id = account.assigned_user_id
        mobile_phone = account.mobile_phone
        crm_regn_ctry_name = account.crm_regn_ctry_name

        mobile_number_with_no_special_char = mobile_phone.replace(" ", "").replace("2547", "07").replace("-", "").replace(".", "").replace(";", "").replace(">", "").replace("<", "").replace(",", "")
        suite_crm_db_session.query(Account).filter_by(id=id).update({"mobile_phone": mobile_number_with_no_special_char})
        suite_crm_db_session.commit()

        #print ("payload is " + str(account) + " account name is " + upper_cased_name)
        print ("payload is " + str(id) + " mobile_phone is " + mobile_phone + " name_without_dash " + mobile_number_with_no_special_char)


    accounts = Account.query.filter(Account.mobile_phone.like("254%")).filter(or_(Account.mobile_phone.like("% %"),
                                                          Account.mobile_phone.like("%-%"),
                                                          Account.mobile_phone.like("%.%"),
                                                          Account.mobile_phone.like("%,%"),
                                                          Account.mobile_phone.like("%;%"),
                                                          Account.mobile_phone.like("%<%"),
                                                          Account.mobile_phone.like("%>%"))).limit(50).all()
    for account in accounts:
        payload = {}

        id = account.id
        name = account.name
        date_entered = account.date_entered
        date_modified = account.date_modified
        modified_user_id = account.modified_user_id
        created_by = account.created_by
        description = account.description
        assigned_user_id = account.assigned_user_id
        mobile_phone = account.mobile_phone
        crm_regn_ctry_name = account.crm_regn_ctry_name

        mobile_number_with_no_special_char = mobile_phone.replace(" ", "").replace("2547", "07").replace("2542", "02").replace("-", "").replace(".", "").replace(";", "").replace(">", "").replace("<", "").replace(",", "")
        suite_crm_db_session.query(Account).filter_by(id=id).update({"mobile_phone": mobile_number_with_no_special_char})
        suite_crm_db_session.commit()

        #print ("payload is " + str(account) + " account name is " + upper_cased_name)
        print ("payload is " + str(id) + " mobile_phone is " + mobile_phone + " name_without_dash " + mobile_number_with_no_special_char)


def remove_spaces_from_phone_numbers_looking_like_email():

    accounts = Account.query.filter(Account.mobile_phone.like("%@%")).all()
    for account in accounts:
        payload = {}

        id = account.id
        name = account.name
        date_entered = account.date_entered
        date_modified = account.date_modified
        modified_user_id = account.modified_user_id
        created_by = account.created_by
        description = account.description
        assigned_user_id = account.assigned_user_id
        mobile_phone = account.mobile_phone
        crm_regn_ctry_name = account.crm_regn_ctry_name

        formatted_mobile_number_that_looks_like_email = mobile_phone.replace(" ", "").replace("'", "").lower()
        suite_crm_db_session.query(Account).filter_by(id=id).update({"mobile_phone": formatted_mobile_number_that_looks_like_email})
        suite_crm_db_session.commit()

        #print ("payload is " + str(account) + " account name is " + upper_cased_name)
        print ("payload is " + str(id) + " mobile_phone is " + mobile_phone + " formatted is " + formatted_mobile_number_that_looks_like_email)


def remove_spaces_from_phone_numbers_in_office_phone():
    count = 0
    accounts = Account.query.filter(Account.date_entered.isnot(None)).filter(Account.mobile_phone.like('+2547%')).all()
    for account in accounts:
        payload = {}
        count += 1
        id = account.id
        name = account.name
        date_entered = account.date_entered
        date_modified = account.date_modified
        modified_user_id = account.modified_user_id
        created_by = account.created_by
        description = account.description
        assigned_user_id = account.assigned_user_id
        mobile_phone = account.mobile_phone
        crm_regn_ctry_name = account.crm_regn_ctry_name

        formatted_mobile_number = "0" + str(mobile_phone[4:])
        print (name + " phone is " + str(formatted_mobile_number))
        suite_crm_db_session.query(Account).filter_by(id=id).update({"mobile_phone": formatted_mobile_number})
        suite_crm_db_session.commit()
        #
        # #print ("payload is " + str(account) + " account name is " + upper_cased_name)
        print ("payload is " + str(id) + " mobile_phone is " + mobile_phone + " formatted is " + formatted_mobile_number)
        #
    print ("total count is " + str(count))


def clean_up_emails():
    def have_a_look_at_email_addresses(offset):
        email_addresses = EmailAddress.query.offset(offset).limit(1).all()
        count = 0
        for email_address_details in email_addresses:
            id = email_address_details.id
            email_address = email_address_details.email_address
            email_address_caps = email_address_details.email_address_caps
            date_created = email_address_details.date_created
            date_modified = email_address_details.date_modified
            crm_regn_ctry_name = email_address_details.crm_regn_ctry_name
            count += 1
            lower_case_email = email_address.lower()
            upper_case_email = email_address_caps.upper()
            suite_crm_db_session.query(EmailAddress).filter_by(id=id).update({"email_address": lower_case_email,
                                                                              "email_address_caps": upper_case_email})
            suite_crm_db_session.commit()
            print ("id is " + id + " count is " + str(count) +" addresses " + email_address + " is now " + lower_case_email+ " upper is "+upper_case_email )
    for offset in range(10000, 11000):
        have_a_look_at_email_addresses(offset)


def clean_up_account_names_with_phone_number_dashes():

    accounts = Account.query.filter(Account.name.like("%7%")).filter(Account.name.like("%- 02%")).limit(5).all()
    count = 0
    for account in accounts:
        payload = {}

        id = account.id
        name = account.name
        date_entered = account.date_entered
        date_modified = account.date_modified
        modified_user_id = account.modified_user_id
        created_by = account.created_by
        description = account.description
        assigned_user_id = account.assigned_user_id
        mobile_phone = account.mobile_phone
        crm_regn_ctry_name = account.crm_regn_ctry_name
        count += 1
        valid_name =  name.split('- 02')[0].upper()
        print ('id is '+id+' count is '+str(count)+' name is ' + name + " split is " + valid_name + " phone is " + mobile_phone)

        suite_crm_db_session.query(Account).filter_by(id=id).update({"name": valid_name})
        suite_crm_db_session.commit()
        #
        # #print ("payload is " + str(account) + " account name is " + upper_cased_name)
        # print ("payload is " + str(id) + " mobile_phone is " + mobile_phone + " formatted is " + formatted_mobile_number_that_looks_like_email)


def clean_up_account_names_guised_as_phone_number():

    accounts = Account.query.filter(Account.name.like("07%")).limit(1).all()
    count = 0
    for account in accounts:
        payload = {}

        id = account.id
        name = account.name
        date_entered = account.date_entered
        date_modified = account.date_modified
        modified_user_id = account.modified_user_id
        created_by = account.created_by
        description = account.description
        assigned_user_id = account.assigned_user_id
        mobile_phone = account.mobile_phone
        crm_regn_ctry_name = account.crm_regn_ctry_name
        count += 1
        print ("id is " + str(id) + "name is "+name+ " mobile_phone is "+ str(mobile_phone))

        suite_crm_db_session.query(Account).filter_by(id=id).update({"name": "NAME NOT PROVIDED", "mobile_phone": name})
        suite_crm_db_session.commit()
        #
        # #print ("payload is " + str(account) + " account name is " + upper_cased_name)
        # print ("payload is " + str(id) + " mobile_phone is " + mobile_phone + " formatted is " + formatted_mobile_number_that_looks_like_email)


def clean_up_7_account_names_guised_as_phone_number():

    accounts = Account.query \
        .filter(Account.id !='efb9db07-558a-a042-c200-56355f30266f')\
        .filter(Account.name.like("7%")).limit(1).all()

    count = 0
    for account in accounts:
        payload = {}

        id = account.id
        name = account.name
        date_entered = account.date_entered
        date_modified = account.date_modified
        modified_user_id = account.modified_user_id
        created_by = account.created_by
        description = account.description
        assigned_user_id = account.assigned_user_id
        mobile_phone = account.mobile_phone
        crm_regn_ctry_name = account.crm_regn_ctry_name
        count += 1
        print ("id is " + str(id) + "name is "+name+ " mobile_phone is "+ str(mobile_phone))

        suite_crm_db_session.query(Account).filter_by(id=id).update({"name": "NAME NOT PROVIDED"})
        suite_crm_db_session.commit()
        #
        # #print ("payload is " + str(account) + " account name is " + upper_cased_name)
        # print ("payload is " + str(id) + " mobile_phone is " + mobile_phone + " formatted is " + formatted_mobile_number_that_looks_like_email)


def detect_duplicate_names_in_crm_db_and_merge_them(offset):

    grand_total_duplicates = 0
    all_duplicate_mobile_phones = []
    t = suite_crm_db_session.query(Account.mobile_phone, func.count(Account.mobile_phone).label('no_of_duplicates'))\
        .filter(Account.mobile_phone.isnot(None))\
        .filter(func.length(Account.mobile_phone) == 10)\
        .filter(Account.mobile_phone != '0700000000')\
        .filter(Account.mobile_phone.like('07%')).filter(Account.created_by != 1).group_by(Account.mobile_phone).offset(offset).limit(1)
    total_duplicates = 0
    duplicates_dict = {}
    for mobile_phone, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_mobile_phones.append(mobile_phone)

            crm_kyc_ids = []
            crm_kyc_ids_matching_record = suite_crm_db_session.query(Account).filter(Account.mobile_phone == mobile_phone).all()
            for crm_kyc_id in crm_kyc_ids_matching_record:
                crm_kyc_ids.append({crm_kyc_id.name: crm_kyc_id.id})

            duplicates_dict[mobile_phone] = crm_kyc_ids

    for duplicated_number, name_kyc_id_values in duplicates_dict.items():
        print ("============================================")
        print (".................PROCESSING................." + str(duplicated_number))
        print (duplicated_number)

        '''
        Generate lists that look like
        ==================================================
        full_names is ['PHENNY NYABOKE', 'PHENNY NYABOKE NYABUTO', 'PHENNY NYABOKE NYABUTO']
        kyc_ids is ['1c559af2-ba9b-adc9-c3fe-55ddc0d82437', '9c3b1b34-5ff7-40d8-a713-bcb20d9ed805', 'd9d3358b-848b-366c-05eb-55b07c327770']
        ===================================================
        '''
        full_names = []
        kyc_ids = []
        for value in name_kyc_id_values:
            full_names.append(value.keys()[0])
            kyc_ids.append(value.values()[0])

        # get the longest name
        longest_name_size = 0
        elected_kyc_id = None
        longest_names = []
        for index, _full_names in enumerate(full_names):
            # give something like ['PHILIP', 'MEELI', 'OLE', 'KAMWARO']
            individual_names = _full_names.split()
            name_count = individual_names.__len__()

            if longest_name_size < name_count:
                longest_name_size = name_count
                longest_names = individual_names
                elected_kyc_id = kyc_ids[index]

        # Check if any of the selected kyc_ids is a core system account and if it is give it preferential treatment
        for kyc_id in kyc_ids:
            core_system_count = AccountCSTM.query.filter(AccountCSTM.id == kyc_id).filter(AccountCSTM.core_system_customer == 1).count()
            if core_system_count == 1:
                elected_kyc_id = kyc_id
                longest_names = full_names[kyc_ids.index(elected_kyc_id)].split()
                print ("core_system_count is " + str(core_system_count) + " kyc_id is " + kyc_id)

        print ("longest_names " + str(longest_names) + " other names " + str(full_names))
        approved_kyc_ids_for_merge = []
        # Check if names are similar
        for name in longest_names:
            for index, _full_name in enumerate(full_names):
                if kyc_ids[index] != elected_kyc_id:
                    if kyc_ids[index] not in approved_kyc_ids_for_merge:
                        print ("Trying to see if we can find " + name + " of " + kyc_ids[index] + " in " + str(_full_name))
                        if name in _full_name.split():
                            print(" name " + name + " has been found in " + str(_full_name))
                            approved_kyc_ids_for_merge.append(kyc_ids[index])

        if approved_kyc_ids_for_merge.__len__() > 0:
            print("approved_kyc_ids_for_merge " + str(approved_kyc_ids_for_merge) + " will be merged into " + elected_kyc_id)

            # find records relating to each individual kyc_ids that is to be merged
            for approved_kyc_id in approved_kyc_ids_for_merge:
                # find if this approved_kyc_id has ever logged cases in crm
                cases = Case.query.filter(Case.account_id == approved_kyc_id).all()
                for case in cases:
                    #print ("case id found is "+case.id + " case status is " + case.status + " case desc is "+ case.description + " case account_id is " + case.account_id)
                    # run a few updates here and there
                    suite_crm_db_session.query(Case).filter_by(account_id=approved_kyc_id).update({"account_id": elected_kyc_id})
                    suite_crm_db_session.commit()

                accounts_opportunities = AccountsOpportunities.query.filter(AccountsOpportunities.account_id == approved_kyc_id).all()
                for account_opportunity in accounts_opportunities:
                    #print ("account_opportunity account_id is " + account_opportunity.account_id + " opportunity id is " + account_opportunity.opportunity_id)
                    suite_crm_db_session.query(AccountsOpportunities).filter_by(account_id=approved_kyc_id).update({"account_id": elected_kyc_id})
                    suite_crm_db_session.commit()

                leads = Lead.query.filter(Lead.account_id == approved_kyc_id).all()
                for lead in leads:
                    print ("Lead id " + lead.id)
                    suite_crm_db_session.query(Lead).filter_by(account_id=approved_kyc_id).update({"account_id": elected_kyc_id})
                    suite_crm_db_session.commit()

                #now delete the following
                print ("deletting account id "+ approved_kyc_id)
                suite_crm_db_session.query(Account).filter(Account.id == approved_kyc_id).delete()
                suite_crm_db_session.commit()

                print ("deletting accountsctm account id " + approved_kyc_id)
                suite_crm_db_session.query(AccountCSTM).filter(AccountCSTM.id == approved_kyc_id).delete()
                suite_crm_db_session.commit()

    print ("total premia duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates


def detect_non_kyc_accounts_that_dont_have_any_leads_opportunities_or_cases_and_remove_them(min_offset, max_offset):
    for offset in range(min_offset, max_offset):
        interval = max_offset - min_offset
        current_offset_pos = (offset+1)-min_offset
        percentage = float(float(current_offset_pos)/float(interval)*100)
        print ("detect_non_kyc_accounts_that_dont_have_any_leads_opportunities_or_cases_and_remove_them offset is %d of %d is %f " % (offset, max_offset, percentage)
               + "% complete")

        non_core_system_customers = suite_crm_db_session.query(AccountCSTM).filter(
            AccountCSTM.core_system_customer == 0).offset(offset).limit(1).all()
        for non_core_system_customer in non_core_system_customers:
            account_id = non_core_system_customer.id
            account = suite_crm_db_session.query(Account).filter(Account.id == account_id).first()
            if account is not None:
                leads = suite_crm_db_session.query(Lead).filter(Lead.account_id == account_id).all()
                cases = suite_crm_db_session.query(Case).filter(Case.account_id == account_id).all()
                accounts_opportunities = suite_crm_db_session.query(AccountsOpportunities).filter(AccountsOpportunities.account_id == account_id).all()
                email_address = suite_crm_db_session.query(EmailAddressBeanRel)\
                    .filter(EmailAddressBeanRel.bean_id == account_id)\
                    .filter(EmailAddressBeanRel.bean_module == "Accounts").first()

                no_of_leads = leads.__len__()
                no_of_cases = cases.__len__()
                no_of_accounts_opportunities = accounts_opportunities.__len__()
                name = account.name

                if no_of_cases == 0 and no_of_leads == 0 and no_of_accounts_opportunities == 0 and email_address is None:
                    print "%s has %s account_id  %d leads and %d cases" % (name, account_id, no_of_leads, no_of_cases)
                    #now delete the following
                    print ("deleting account id "+ account_id)
                    suite_crm_db_session.query(Account).filter(Account.id == account_id).delete()
                    suite_crm_db_session.commit()

                    print ("deleting accountsctm account id " + account_id)
                    suite_crm_db_session.query(AccountCSTM).filter(AccountCSTM.id == account_id).delete()
                    suite_crm_db_session.commit()


def detect_duplicate_names_in_crm_db_that_dont_have_metadata_and_merge_them_with_kyc_ones():
    #print "detect_duplicate_names_in_crm_db_that_dont_have_metadata_and_merge_them_with_kyc_ones offset %d" % offset

    non_core_system_customers = suite_crm_db_session.query(AccountCSTM).filter(AccountCSTM.core_system_customer == 0).all()
    for non_core_system_customer in non_core_system_customers:
        account_id = non_core_system_customer.id
        #print "==========Account==%s==================" % account_id
        cases = suite_crm_db_session.query(Case).filter(Case.account_id == account_id).all()
        for case in cases:
            case_cstm = suite_crm_db_session.query(CaseCSTM).filter(CaseCSTM.id == case.id)\
                .filter(CaseCSTM.policy_no.isnot(None))\
                .filter(CaseCSTM.business_type != 'Medical').first()

            # Validate policy number
            if case_cstm is not None:
                policy_no = case_cstm.policy_no
                if policy_no is not None:
                    if " " not in str(policy_no) and "/" not in str(policy_no) and "&" not in str(policy_no) and "," not in str(policy_no):
                        if str(policy_no).replace(" ", ""):
                            # Try and see if you can find the kyc id from customers kyc db
                            kyc_policy = kyc_db_session.query(KYCPolicy).filter(KYCPolicy.policy_number == policy_no).first()
                            if kyc_policy is not None:
                                kyc_id = kyc_policy.kyc_id
                                account_name = suite_crm_db_session.query(Account.name).filter(
                                    Account.id == account_id).first()
                                kyc_account_name = kyc_db_session.query(KYCCustomerDetails.name).filter(
                                    KYCCustomerDetails.id == kyc_id).first()
                                if account_name[0] == kyc_account_name[0]:
                                    print "case number " + case_cstm.id +" of " + policy_no + " of " + account_name[0] + " is to have kyc id " + kyc_id + " current account id is " + account_id
                                    # run a few updates here and there
                                    suite_crm_db_session.query(Case).filter_by(id=case_cstm.id).update({"account_id": kyc_id})
                                    suite_crm_db_session.commit()


        # for non_core_system_customer_case_cstm in non_core_system_customer_cases_cstm:
        #     print non_core_system_customer_case_cstm
        # print "=============================="
    # grand_total_duplicates = 0
    # all_duplicate_mobile_phones = []
    # t = suite_crm_db_session.query(Account.mobile_phone, func.count(Account.mobile_phone).label('no_of_duplicates'))\
    #     .filter(Account.mobile_phone.isnot(None))\
    #     .filter(func.length(Account.mobile_phone) == 10)\
    #     .filter(Account.mobile_phone != '0700000000')\
    #     .filter(Account.mobile_phone.like('07%')).filter(Account.created_by != 1).group_by(Account.mobile_phone).offset(offset).limit(1)

    # total_duplicates = 0
    # duplicates_dict = {}
    # for mobile_phone, number_of_times_duplicated in t:
    #     if number_of_times_duplicated > 1:
    #         total_duplicates += number_of_times_duplicated
    #         all_duplicate_mobile_phones.append(mobile_phone)
    #
    #         crm_kyc_ids = []
    #         crm_kyc_ids_matching_record = suite_crm_db_session.query(Account).filter(Account.mobile_phone == mobile_phone).all()
    #         for crm_kyc_id in crm_kyc_ids_matching_record:
    #             crm_kyc_ids.append({crm_kyc_id.name: crm_kyc_id.id})
    #
    #         duplicates_dict[mobile_phone] = crm_kyc_ids
    #
    # for duplicated_number, name_kyc_id_values in duplicates_dict.items():
    #     print ("============================================")
    #     print (".................PROCESSING................." + str(duplicated_number))
    #     print (duplicated_number)
    #
    #     '''
    #     Generate lists that look like
    #     ==================================================
    #     full_names is ['PHENNY NYABOKE', 'PHENNY NYABOKE NYABUTO', 'PHENNY NYABOKE NYABUTO']
    #     kyc_ids is ['1c559af2-ba9b-adc9-c3fe-55ddc0d82437', '9c3b1b34-5ff7-40d8-a713-bcb20d9ed805', 'd9d3358b-848b-366c-05eb-55b07c327770']
    #     ===================================================
    #     '''
    #     full_names = []
    #     kyc_ids = []
    #     for value in name_kyc_id_values:
    #         full_names.append(value.keys()[0])
    #         kyc_ids.append(value.values()[0])
    #
    #     # get the longest name
    #     longest_name_size = 0
    #     elected_kyc_id = None
    #     longest_names = []
    #     for index, _full_names in enumerate(full_names):
    #         # give something like ['PHILIP', 'MEELI', 'OLE', 'KAMWARO']
    #         individual_names = _full_names.split()
    #         name_count = individual_names.__len__()
    #
    #         if longest_name_size < name_count:
    #             longest_name_size = name_count
    #             longest_names = individual_names
    #             elected_kyc_id = kyc_ids[index]
    #
    #     # Check if any of the selected kyc_ids is a core system account and if it is give it preferential treatment
    #     for kyc_id in kyc_ids:
    #         core_system_count = AccountCSTM.query.filter(AccountCSTM.id == kyc_id).filter(AccountCSTM.core_system_customer == 1).count()
    #         if core_system_count == 1:
    #             elected_kyc_id = kyc_id
    #             longest_names = full_names[kyc_ids.index(elected_kyc_id)].split()
    #             print ("core_system_count is " + str(core_system_count) + " kyc_id is " + kyc_id)
    #
    #     print ("longest_names " + str(longest_names) + " other names " + str(full_names))
    #     approved_kyc_ids_for_merge = []
    #     # Check if names are similar
    #     for name in longest_names:
    #         for index, _full_name in enumerate(full_names):
    #             if kyc_ids[index] != elected_kyc_id:
    #                 if kyc_ids[index] not in approved_kyc_ids_for_merge:
    #                     print ("Trying to see if we can find " + name + " of " + kyc_ids[index] + " in " + str(_full_name))
    #                     if name in _full_name.split():
    #                         print(" name " + name + " has been found in " + str(_full_name))
    #                         approved_kyc_ids_for_merge.append(kyc_ids[index])
    #
    #     if approved_kyc_ids_for_merge.__len__() > 0:
    #         print("approved_kyc_ids_for_merge " + str(approved_kyc_ids_for_merge) + " will be merged into " + elected_kyc_id)
    #
    #         # find records relating to each individual kyc_ids that is to be merged
    #         for approved_kyc_id in approved_kyc_ids_for_merge:
    #             # find if this approved_kyc_id has ever logged cases in crm
    #             cases = Case.query.filter(Case.account_id == approved_kyc_id).all()
    #             for case in cases:
    #                 #print ("case id found is "+case.id + " case status is " + case.status + " case desc is "+ case.description + " case account_id is " + case.account_id)
    #                 # run a few updates here and there
    #                 suite_crm_db_session.query(Case).filter_by(account_id=approved_kyc_id).update({"account_id": elected_kyc_id})
    #                 suite_crm_db_session.commit()
    #
    #             accounts_opportunities = AccountsOpportunities.query.filter(AccountsOpportunities.account_id == approved_kyc_id).all()
    #             for account_opportunity in accounts_opportunities:
    #                 #print ("account_opportunity account_id is " + account_opportunity.account_id + " opportunity id is " + account_opportunity.opportunity_id)
    #                 suite_crm_db_session.query(AccountsOpportunities).filter_by(account_id=approved_kyc_id).update({"account_id": elected_kyc_id})
    #                 suite_crm_db_session.commit()
    #
    #             leads = Lead.query.filter(Lead.account_id == approved_kyc_id).all()
    #             for lead in leads:
    #                 print ("Lead id " + lead.id)
    #                 suite_crm_db_session.query(Lead).filter_by(account_id=approved_kyc_id).update({"account_id": elected_kyc_id})
    #                 suite_crm_db_session.commit()
    #
    #             #now delete the following
    #             print ("deletting account id "+ approved_kyc_id)
    #             suite_crm_db_session.query(Account).filter(Account.id == approved_kyc_id).delete()
    #             suite_crm_db_session.commit()
    #
    #             print ("deletting accountsctm account id " + approved_kyc_id)
    #             suite_crm_db_session.query(AccountCSTM).filter(AccountCSTM.id == approved_kyc_id).delete()
    #             suite_crm_db_session.commit()
    #
    # print ("total premia duplicates is " + str(total_duplicates))
    # grand_total_duplicates += total_duplicates

def load_kyc_policies_into_crm():
    # delete policies first
    # suite_crm_db_session.query(Policy).delete()
    # suite_crm_db_session.commit()

    policies = []
    kyc_policies = kyc_db_session.query(KYCPolicy).all()
    for kyc_policy in kyc_policies:
        policy = Policy()
        policy.id = kyc_policy.id
        policy.policy_number = kyc_policy.policy_number
        policy.pol_class = kyc_policy.pol_class
        policy.sub_class = kyc_policy.pol_sub_class
        policy.status = kyc_policy.status
        policy.kyc_id = kyc_policy.kyc_id
        policy.date_of_purchase = kyc_policy.date_of_purchase
        policy.renewal_date = kyc_policy.renewal_date
        policy.maturity_date = kyc_policy.maturity_date
        policy.benefit_due_date = kyc_policy.benefit_due_date
        policy.last_updated = kyc_policy.last_updated
        policy.start_date = kyc_policy.start_date
        policy.sum_assured = kyc_policy.sum_assured
        policy.surrender_value = kyc_policy.surrender_value
        policy.term_in_years = kyc_policy.term_in_years
        policy.type = kyc_policy.type
        policy.line_of_business = kyc_policy.line_of_business
        try:
            print "Adding Policy " + str(kyc_policy.policy_number)
            suite_crm_db_session.add(policy)
            suite_crm_db_session.commit()
        except exc.DataError, e:
            suite_crm_db_session.rollback()
            print "Unable to add Policy " + str(kyc_policy.policy_number)
            continue
        except exc.IntegrityError, e:
            suite_crm_db_session.rollback()
            print "Unable to add Policy " + str(kyc_policy.policy_number)
            continue
        except exc.OperationalError, e:
            suite_crm_db_session.rollback()
            print "Unable to add Policy " + str(kyc_policy.policy_number)
            continue

            #print policy_number
        # crm_policy = suite_crm_db_session.query(Policy).filter(Policy.policy_number == kyc_policy_number).first()
        # if crm_policy is None:
        #     print "Policy number " + str(kyc_policy_number) + " to be added"
            #policies.append(policy)
    # kyc_policies = KYCPolicy.query.limit(10).all()
    # for kyc_policy in kyc_policies:
    #     policy = Policy()
    #     policy.id = kyc_policy.id
    #     policy.policy_number = kyc_policy.policy_number
    #     policy.pol_class = kyc_policy.pol_class
    #     policy.sub_class = kyc_policy.pol_sub_class
    #     policy.status = kyc_policy.status
    #     policy.kyc_id = kyc_policy.kyc_id
    #     policy.date_of_purchase = kyc_policy.date_of_purchase
    #     policy.renewal_date = kyc_policy.renewal_date
    #     policy.maturity_date = kyc_policy.maturity_date
    #     policy.benefit_due_date = kyc_policy.benefit_due_date
    #     policy.last_updated = kyc_policy.last_updated
    #     policy.start_date = kyc_policy.start_date
    #     policy.sum_assured = kyc_policy.sum_assured
    #     policy.surrender_value = kyc_policy.surrender_value
    #     policy.term_in_years = kyc_policy.term_in_years
    #     policy.type = kyc_policy.type
    #     policy.line_of_business = kyc_policy.line_of_business
    #
    #     crm_policy = suite_crm_db_session.query(Policy).filter(Policy.policy_number == kyc_policy.policy_number).first()
    #     if crm_policy is None:
    #         print "Policy number " + str(kyc_policy.policy_number) + " to be added"
    #         policies.append(policy)

    # split into smaller sections n loop
    # mega_policies = even_divide(policies)
    # for index, policies in enumerate(mega_policies):
    #     print ("loading kyc policies into crm %d of %d" % (index+1, mega_policies.__len__()))
    #     suite_crm_db_session.bulk_save_objects(policies, return_defaults=True)
    #     suite_crm_db_session.commit()
    #
    # match_up_policy_accounts()
    print("==============DONE=================")


def match_up_email_addresses():
    account_email_addresses = suite_crm_db_session.query(EmailAddressBeanRel).filter(EmailAddressBeanRel.bean_module == "Accounts").all()
    for account_email_address in account_email_addresses:
        email_address = suite_crm_db_session.query(EmailAddress).filter(EmailAddress.id == account_email_address.email_address_id).first()
        email = email_address.email_address
        account_id = account_email_address.bean_id
        print "email is %s account id is %s" % (email, account_id)


def load_email_addresses_from_kyc_into_crm():
    emails_dict = {}

    actisure_customers_with_emails = kyc_db_session.query(KYCCustomer).filter(KYCCustomer.actisure_email.isnot(None)).all()
    for actisure_customer_with_email in actisure_customers_with_emails:
        emails_dict[actisure_customer_with_email.kyc_id] = actisure_customer_with_email.actisure_email

    isf_customers_with_emails = kyc_db_session.query(KYCCustomer).filter(KYCCustomer.isf_email.isnot(None)).all()
    for isf_customer_with_email in isf_customers_with_emails:
        emails_dict[isf_customer_with_email.kyc_id] = isf_customer_with_email.isf_email

    apex_customers_with_emails = kyc_db_session.query(KYCCustomer).filter(KYCCustomer.apex_email.isnot(None)).all()
    for apex_customer_with_email in apex_customers_with_emails:
        emails_dict[apex_customer_with_email.kyc_id] = apex_customer_with_email.apex_email

    kycs = emails_dict.keys()
    for kyc_id, email in emails_dict.items():
        unique_email_id = str(uuid.uuid4())

        email_address = EmailAddress()
        email_address.email_address = email
        email_address.email_address_caps = email.upper()
        email_address.id = unique_email_id
        email_address.crm_regn_ctry_name = 'kenya'

        email_address_bean_rel = EmailAddressBeanRel()
        email_address_bean_rel.id = str(uuid.uuid4()) # unique in its respect
        email_address_bean_rel.email_address_id = unique_email_id
        email_address_bean_rel.bean_id = kyc_id
        email_address_bean_rel.bean_module = "Accounts"
        email_address_bean_rel.primary_address = 1
        email_address_bean_rel.crm_regn_ctry_name = 'kenya'

        suite_crm_db_session.add(email_address_bean_rel)
        suite_crm_db_session.commit()
        suite_crm_db_session.add(email_address)
        suite_crm_db_session.commit()
        print str(kycs.index(kyc_id)) + ". KYC ID is " + kyc_id + " email is " + email


def license_users_to_reports():
    users = suite_crm_db_session.query(User).all()
    for user in users:
        report_user = suite_crm_db_session.query(ASOLCommonConfig).filter(ASOLCommonConfig.modified_user_id == user.id).first()
        if report_user is None:
            asol_common_config = ASOLCommonConfig()
            asol_common_config.id = str(uuid.uuid4())
            asol_common_config.name = user.first_name + " " + user.last_name
            asol_common_config.modified_user_id = user.id
            asol_common_config.created_by = user.id
            asol_common_config.deleted = 0
            asol_common_config.category = "user"
            asol_common_config.config = "YTo2OntzOjE1OiJmaXNjYWxNb250aEluaXQiO3M6MToiMSI7czoxNDoiZW50cmllc1BlclBhZ2UiO3M6MjoiMTUiO3M6MTQ6InBkZk9yaWVudGF0aW9uIjtzOjk6ImxhbmRzY2FwZSI7czoxMzoicGRmUGFnZUZvcm1hdCI7czoyOiJBNCI7czoxOToicGRmSW1nU2NhbGluZ0ZhY3RvciI7czo0OiIxLjIwIjtzOjEyOiJ3ZWVrU3RhcnRzT24iO3M6MToiMSI7fQ=="
            asol_common_config.date_entered = datetime.utcnow()
            asol_common_config.date_modified = datetime.utcnow()
            asol_common_config.de = datetime.utcnow()

            suite_crm_db_session.add(asol_common_config)
            suite_crm_db_session.commit()

            print asol_common_config.name


def match_up_policy_accounts():
    # delete policies first
    suite_crm_db_session.query(PolicyAccount).delete()
    suite_crm_db_session.commit()

    policy_ids = []
    policies = Policy.query.all()
    policy_accounts = []

    for policy in policies:
        policy_ids.append(policy.id)
        policy_account = PolicyAccount()
        policy_account.id = str(uuid.uuid4())
        policy_account.account_id = policy.kyc_id
        policy_account.policy_id = policy.id
        policy_accounts.append(policy_account)

    # send it to db
    mega_policy_accounts = even_divide(policy_accounts)
    for index, policies in enumerate(mega_policy_accounts):
        suite_crm_db_session.bulk_save_objects(policies, return_defaults=True)
        suite_crm_db_session.commit()
        print ("match_up_policy_accounts %d of %d" % (index + 1, mega_policy_accounts.__len__()))

    print ("Total policies are %d" % policy_accounts.__len__())


def reverse_match_up_policy_accounts():
    policy_ids = suite_crm_db_session.query(Policy.id).all()
    policy_account_ids = suite_crm_db_session.query(PolicyAccount.policy_id).all()
    policy_accounts = []
    print "original policy_ids number " + str(policy_ids.__len__())
    final_policy_ids = []
    final_policy_accounts = []
    for policy_id in policy_ids:
        if policy_id not in policy_account_ids:
            policy = Policy.query.filter(Policy.id == str(policy_id[0])).first()
            if policy is not None:
                print "processing " + policy_id[0]
                #final_policy_ids.append(policy_id[0])
                policy_account = PolicyAccount()
                policy_account.id = str(uuid.uuid4())
                policy_account.account_id = policy.kyc_id
                policy_account.policy_id = policy.id
                suite_crm_db_session.add(policy_account)
                suite_crm_db_session.commit()
    #
    # # send it to db
    # mega_policy_accounts = even_divide(final_policy_accounts)
    # for index, policies in enumerate(mega_policy_accounts):
    #     suite_crm_db_session.bulk_save_objects(policies, return_defaults=True)
    #     suite_crm_db_session.commit()
    #     print ("match_up_policy_accounts %d of %d" % (index + 1, mega_policy_accounts.__len__()))
    #
    # print ("Total policies are %d" % policy_accounts.__len__())
    #
    # print "late policy_ids number " + str(policy_ids.__len__())

        # policies = []
        # for policy in policies:
        #     policy_ids.append(policy.id)
        #     policy_account = PolicyAccount()
        #     policy_account.id = str(uuid.uuid4())
        #     policy_account.account_id = policy.kyc_id
        #     policy_account.policy_id = policy.id
        #     policy_accounts.append(policy_account)
        #
        # # send it to db
        # mega_policy_accounts = even_divide(policy_accounts)
        # for index, policies in enumerate(mega_policy_accounts):
        #     suite_crm_db_session.bulk_save_objects(policies, return_defaults=True)
        #     suite_crm_db_session.commit()
        #     print ("match_up_policy_accounts %d of %d" % (index + 1, mega_policy_accounts.__len__()))
        #
        # print ("Total policies are %d" % policy_accounts.__len__())


def match_up_payments_accounts(min_offset, max_offset):
    for offset in range(min_offset, max_offset):
        interval = max_offset - min_offset
        current_offset_pos = (offset + 1) - min_offset
        percentage = float(float(current_offset_pos) / float(interval) * 100)
        print ("match_up_payments_accounts offset is %d of %d is %f " % (
        offset, max_offset, percentage)
               + "% complete")

        # delete policies first
        # suite_crm_db_session.query(PaymentAccount).delete()
        # suite_crm_db_session.commit()

        #payment_ids = []
        policies = Payments.query.offset(offset).limit(1).all()
        #payment_accounts = []

        for payment in policies:
            #payment_ids.append(payment.id)
            payment_account = PaymentAccount()
            payment_account.id = str(uuid.uuid4())
            payment_account.account_id = payment.kyc_id
            payment_account.payment_id = payment.id
            suite_crm_db_session.add(payment_account)
            suite_crm_db_session.commit()
            #payment_accounts.append(payment_account)

        # # send it to db
        # mega_payment_accounts = even_divide(payment_accounts)
        # for index, policies in enumerate(mega_payment_accounts):
        #     suite_crm_db_session.bulk_save_objects(policies, return_defaults=True)
        #     suite_crm_db_session.commit()
        #     print ("match_up_payment_accounts %d of %d" % (index + 1, mega_payment_accounts.__len__()))

    #print ("Total policies are %d" % payment_accounts.__len__())




def even_divide(lst, num_piece=14):
    return [
        [lst[i] for i in range(len(lst)) if (i % num_piece) == r]
        for r in range(num_piece)
    ]


def load_payments_from_kyc_to_crm(min_offset, max_offset):
    for offset in range(min_offset, max_offset):
        interval = max_offset - min_offset
        percentage = float(float((offset + 1) - min_offset) / float(interval) * 100)
        print ("load_payments_from_kyc_to_crm is %d of %d is %f " % (offset, max_offset, percentage) + "% complete")

        payment_details = kyc_db_session.query(PaymentDetails).offset(offset).limit(1).all()
        for payment_detail in payment_details:
            if payment_detail is not None:
                payment_id = payment_detail.id
                # Instantiate payment
                payments = Payments()
                payments.id = payment_id
                payments.deleted = 0
                payments.status = payment_detail.status
                payments.date_of_last_payment = payment_detail.date_of_last_payment
                payments.mode_of_payment = payment_detail.channel_of_payment
                payments.next_pay_due_date = payment_detail.next_pay_due_date
                payments.last_updated = datetime.utcnow()
                payments.receipt_number = payment_detail.receipt_number
                payments.amount = payment_detail.amount
                payments.policy_number = payment_detail.policy_number
                payments.date_of_payment = payment_detail.date_of_payment
                payments.kyc_id = payment_detail.kyc_id
                payments.line_of_business = payment_detail.line_of_business

                db_payment = suite_crm_db_session.query(Payments).filter(Payments.id == payment_id).first()
                if db_payment is None:
                    suite_crm_db_session.add(payments)
                    suite_crm_db_session.commit()

                    # append to lists
                    #payments_list[payment_id] = payments
                    print "appending" + str(payment_id)

    # # send all payments to crm db be ware of memory overload
    # mega_payment_accounts = even_divide(payments_list.values())
    # for index, payments in enumerate(mega_payment_accounts):
    #     suite_crm_db_session.bulk_save_objects(payments, return_defaults=True)
    #     suite_crm_db_session.commit()
    #     print ("payments %d of %d" % (index + 1, mega_payment_accounts.__len__()))


def load_premia_policy_start_date_from_premia_to_crm(min_offset, max_offset):
    # List of dictionary including primary key
    policy_mappings = []

    for offset in range(min_offset, max_offset):
        interval = max_offset - min_offset
        current_offset_pos = (offset + 1) - min_offset
        percentage = float(float(current_offset_pos) / float(interval) * 100)
        print ("load_premia_policy_start_date_from_premia_to_crm offset is %d of %d is %f " % (offset, max_offset, percentage) + "% complete")
        print "offset is " + str(offset)

        crm_life_policy = suite_crm_db_session.query(Policy).filter(Policy.line_of_business == "GENERAL").filter(
            Policy.start_date == None).offset(offset).first()
        if crm_life_policy is not None:
            print crm_life_policy
            general_policy = premia_db_session.query(PolicyDetails).filter(
                PolicyDetails.policy_no == crm_life_policy.policy_number).first()
            data = {
                "id": crm_life_policy.id,
                "start_date": general_policy.from_date,
            }
            policy_mappings.append(data)
            print data
            # suite_crm_db_session.query(Policy).filter_by(id=str(crm_life_policy.id)).update({"pol_sub_class": life_policy.pol_class})
            # suite_crm_db_session.commit()

    suite_crm_db_session.bulk_update_mappings(Policy, policy_mappings)
    suite_crm_db_session.commit()


def run_customer_segmentation_based_on_no_of_policies():
    account_cstms = suite_crm_db_session.query(AccountCSTM).limit(10000).offset(240000).all()
    for account_cstm in account_cstms:
        print account_cstm.id
        policies_count = KYCPolicy.query.filter(KYCPolicy.kyc_id == account_cstm.id).count()

        category = "low_value"
        if policies_count <= 1:
            category = "low_value"
        elif policies_count >= 2 and policies_count <= 5:
            category = "medium_value"
        elif policies_count >= 6:
            category = "high_value"
        account_cstm.category = category
        account_cstm.no_of_policies = policies_count
        suite_crm_db_session.commit()


def reverse_run_customer_segmentation_based_on_no_of_policies():
    account_cstms = suite_crm_db_session.query(AccountCSTM).filter(AccountCSTM.no_of_policies.is_(None)).all()
    for account_cstm in account_cstms:
        print account_cstm.id
        policies_count = KYCPolicy.query.filter(KYCPolicy.kyc_id == account_cstm.id).count()

        category = "low_value"
        if policies_count <= 1:
            category = "low_value"
        elif policies_count >= 2 and policies_count <= 5:
            category = "medium_value"
        elif policies_count >= 6:
            category = "high_value"
        #Some return big int
        if policies_count < 100:
            account_cstm.category = category
            account_cstm.no_of_policies = policies_count
            suite_crm_db_session.commit()

if __name__ == '__main__':
    #load_cases_from_db()
    #capitalize_crm_accounts_names(90000, 100000)
    #remove_special_char_from_crm_accounts_names()
    #add_07_to_phone_numbers_begining_with_7()
    #remove_special_char_from_phone_numbers()
    #remove_special_char_from_254_phone_numbers()
    #remove_spaces_from_phone_numbers_looking_like_email()
    #clean_up_emails()
    #clean_up_account_names_with_phone_number_dashes()
    #clean_up_account_names_guised_as_phone_number()
    # while True:
    #     clean_up_7_account_names_guised_as_phone_number()
    #remove_spaces_from_phone_numbers_in_office_phone()
    # for offset in range(100000, 130000):
    #     detect_duplicate_names_in_crm_db_and_merge_them(offset)
    #load_kyc_policies_into_crm()
    #for offset in range (0, 10):
    #detect_duplicate_names_in_crm_db_that_dont_have_metadata_and_merge_them_with_kyc_ones()
    #detect_non_kyc_accounts_that_dont_have_any_leads_opportunities_or_cases_and_remove_them(15000, 15500)
    #match_up_email_addresses()
    #load_email_addresses_from_kyc_into_crm()
    #license_users_to_reports()

    # roles = suite_crm_db_session.query(ACLRoles).all()
    # for role in roles:
    #     print "============================ " + role.name + " == of " + role.id + " =========================="
    #     acl_actions = suite_crm_db_session.query(ACLActions).filter(ACLActions.category == 'AlineaSol_Reports').all()
    #     for acl_action in acl_actions:
    #         print "role id is " + role.id + " action id is " + acl_action.id + " action name is " + acl_action.name + " action cat is " + acl_action.category
    #         acl_roles_actions = ACLRolesActions()
    #         acl_roles_actions.id = str(uuid.uuid4())
    #         acl_roles_actions.access_override = 0
    #         acl_roles_actions.action_id = acl_action.id
    #         acl_roles_actions.crm_regn_ctry_name = 'kenya'
    #         acl_roles_actions.deleted = 0
    #         acl_roles_actions.role_id = role.id
    #         acl_roles_actions.date_modified = datetime.utcnow()
    #
    #         suite_crm_db_session.add(acl_roles_actions)
    #         suite_crm_db_session.commit()

    # roles = suite_crm_db_session.query(ACLRoles).all()
    # for role in roles:
    #     print "============================ "+role.name+" == of " + role.id +" =========================="
    #     acl_roles_actions = suite_crm_db_session.query(ACLRolesActions).filter(ACLRolesActions.role_id == role.id).all()
    #     for acl_roles_action in acl_roles_actions:
    #         action_id = acl_roles_action.action_id
    #         acl_action = suite_crm_db_session.query(ACLActions).filter(ACLActions.id == action_id).first()
    #         if acl_action is not None:
    #             print "action name is " + acl_action.name + " action cat is " + acl_action.category

    #load_payments_from_kyc_to_crm(1125000, 1135000)
    #match_up_payments_accounts(119013, 200000)

    # general_policies = suite_crm_db_session.query(Policy).filter(Policy.line_of_business == "GENERAL").filter(Policy.pol_sub_class == None).all()
    # for general_policy in general_policies:
    #     policy_number = general_policy.policy_number
    #
    #     kyc_policy = kyc_db_session.query(KYCPolicy).filter(KYCPolicy.policy_number == policy_number).first()
    #     if kyc_policy is not None:
    #         suite_crm_db_session.query(Policy).filter_by(id=kyc_policy.id).update({"pol_sub_class": kyc_policy.pol_sub_class})
    #         suite_crm_db_session.commit()
    #
    #         print kyc_policy.id


    def update_life_policies(min_offset, max_offset):

        # List of dictionary including primary key
        policy_mappings = []

        for offset in range(min_offset, max_offset):
            interval = max_offset - min_offset
            current_offset_pos = (offset + 1) - min_offset
            percentage = float(float(current_offset_pos) / float(interval) * 100)
            print ("update_life_policies offset is %d of %d is %f " % (offset, max_offset, percentage)+ "% complete")
            print "offset is " + str(offset)

            crm_life_policy = suite_crm_db_session.query(Policy).filter(Policy.line_of_business == "LIFE").filter(Policy.pol_class == None).offset(offset).first()
            if crm_life_policy is not None:
                print crm_life_policy
                life_policy = isf_uat_db_session.query(LifePolicy).filter(LifePolicy.policy_no == crm_life_policy.policy_number).first()
                data = {
                    "id": crm_life_policy.id,
                    "pol_class": life_policy.pol_class,
                    "surrender_value": life_policy.surrender_value,
                    "status": life_policy.status,
                    "sum_assured": life_policy.sum_assured,
                    "benefit_due_date": life_policy.benefit_due_date,
                    "maturity_date": life_policy.maturity_date,
                    "start_date": life_policy.start_date,
                    }
                policy_mappings.append(data)
                # suite_crm_db_session.query(Policy).filter_by(id=str(crm_life_policy.id)).update({"pol_sub_class": life_policy.pol_class})
                # suite_crm_db_session.commit()

        suite_crm_db_session.bulk_update_mappings(Policy, policy_mappings)
        suite_crm_db_session.commit()

    def update_life_policies_with_missing_date_of_purchase(min_offset, max_offset):

        # List of dictionary including primary key
        policy_mappings = []

        for offset in range(min_offset, max_offset):
            interval = max_offset - min_offset
            current_offset_pos = (offset + 1) - min_offset
            percentage = float(float(current_offset_pos) / float(interval) * 100)
            crm_life_policy = suite_crm_db_session.query(Policy).filter(Policy.line_of_business == "LIFE").filter(
                Policy.date_of_purchase == None).offset(offset).first()
            if crm_life_policy is not None:
                print ("update_life_policies_with_missing_date_of_purchase offset is %d of %d is %f " % (offset, max_offset, percentage) + "% complete")
                print "offset is " + str(offset)
                print crm_life_policy
                life_policy = isf_uat_db_session.query(LifePolicy).filter(
                    LifePolicy.policy_no == crm_life_policy.policy_number).first()
                data = {
                    "id": str(crm_life_policy.id),
                    "date_of_purchase": str(life_policy.date_of_purchase),
                    "benefit_due_date": str(life_policy.renewal_date),
                    "next_due_date": str(life_policy.next_out_date),
                    "term_in_years": str(life_policy.term_in_years),
                    "renewal_date": str(life_policy.renewal_date),
                }
                policy_mappings.append(data)
                # suite_crm_db_session.query(Policy).filter_by(id=str(crm_life_policy.id)).update({"pol_sub_class": life_policy.pol_class})
                # suite_crm_db_session.commit()

        suite_crm_db_session.bulk_update_mappings(Policy, policy_mappings)
        suite_crm_db_session.commit()


    def update_pension_policies_with_missing_start_date_and_scheme_name(min_offset, max_offset):

        # List of dictionary including primary key
        policy_mappings = []

        for offset in range(min_offset, max_offset):
            interval = max_offset - min_offset
            current_offset_pos = (offset + 1) - min_offset
            percentage = float(float(current_offset_pos) / float(interval) * 100)
            crm_life_policy = suite_crm_db_session.query(Policy).filter(Policy.line_of_business == "PENSIONS").filter(
                Policy.start_date == None).offset(offset).first()
            if crm_life_policy is not None:
                print ("update_pension_policies_with_missing_start_date_and_scheme_name offset is %d of %d is %f " % (
                offset, max_offset, percentage) + "% complete")
                print "offset is " + str(offset)
                print crm_life_policy
                pension_policy = apex_db_session.query(PensionAccountInfo).filter(
                    PensionAccountInfo.policy_no == crm_life_policy.policy_number).first()

                if pension_policy is not None:
                    data = {
                        "id": str(crm_life_policy.id),
                        "pol_class": str(pension_policy.product),
                        "start_date": str(datetime.strptime(pension_policy.policy_start_date, '%d/%m/%Y')),
                    }
                    print (str(data))
                    policy_mappings.append(data)
                # suite_crm_db_session.query(Policy).filter_by(id=str(crm_life_policy.id)).update({"pol_sub_class": life_policy.pol_class})
                # suite_crm_db_session.commit()

        suite_crm_db_session.bulk_update_mappings(Policy, policy_mappings)
        suite_crm_db_session.commit()


            #
            # life_policy = isf_uat_db_session.query(LifePolicy).offset(offset).first()
            # if life_policy is not None:
            #     print life_policy.policy_no

            #
                # suite_crm_db_session.query(Policy).filter_by(policy_number=life_policy.policy_no).update({
                #      "pol_class": life_policy.pol_class,
                #     "surrender_value": life_policy.surrender_value,
                #     "status": life_policy.status,
                #     "sum_assured": life_policy.sum_assured,
                #     "benefit_due_date": life_policy.benefit_due_date,
                #     "maturity_date": life_policy.maturity_date,
                #     "start_date": life_policy.start_date,
                #     })
                # suite_crm_db_session.commit()

                #print life_policy.policy_no

            #suite_crm_db_session.query(Policy)

    # while True:
    #     update_life_policies_with_missing_date_of_purchase(0, 10)
    #update_pension_policies_with_missing_start_date_and_scheme_name(0, 10000)
    # while True:
    #     load_premia_policy_start_date_from_premia_to_crm(0, 100)

    # offset=1
    # crm_life_policy = suite_crm_db_session.query(Policy).filter(Policy.line_of_business == "MEDICAL").filter(
    #     Policy.member_number == None).offset(offset).first()
    # if crm_life_policy is not None:
    #     medical_information = actisure_db_session.query(MedicalInformation).filter(MedicalInformation.).all()
    #     if medical_information is not None:
    #         print medical_information

    # connection = suite_crm_db_session.connection()
    # sql = text("INSERT INTO payme_payments (id, policy_number, receipt_number, receipt_date, amount, ilanga_receipt_no, ilanga_receipt_date, status, line_of_business, kyc_id) SELECT MID(UUID(),1,36) uu_id, V_POLICY_NO, V_RECEIPT_NO, D_RECEIPT_DATE, N_RECEIPT_AMT, V_OTHER_REF_NO, D_OTHER_REF_DATE, V_RECEIPT_STATUS, lob, kyc_id FROM temp_payments,polic_policies where V_POLICY_NO=policy_number  LIMIT 10000 OFFSET 1001;")
    # connection.execute(sql)
    #run_customer_segmentation_based_on_no_of_policies()
    #reverse_run_customer_segmentation_based_on_no_of_policies()
    #reverse_match_up_policy_accounts()

    # kyc_policies_with_premium_amount = kyc_db_session.query(KYCPolicy).filter(KYCPolicy.premium_amount.isnot(None)).all()
    # for policy in kyc_policies_with_premium_amount:
    #
    #     try:
    #         suite_crm_db_session.query(Policy).filter(Policy.policy_number == policy.policy_number).update({"premium_amount": policy.premium_amount, "premium_frequency": policy.premium_frequency})
    #         suite_crm_db_session.commit()
    #         print policy.premium_amount
    #     except exc.DataError, e:
    #         suite_crm_db_session.rollback()
    #         print "Unable to add Policy " + str(policy.premium_amount)
    #         continue
    #     except exc.IntegrityError, e:
    #         suite_crm_db_session.rollback()
    #         print "Unable to add Policy " + str(policy.premium_amount)
    #         continue
    #     except exc.OperationalError, e:
    #         suite_crm_db_session.rollback()
    #         print "Unable to add Policy " + str(policy.premium_amount)
    #         continue

    customer_policy_amounts_dict = {}
    customer_policies = suite_crm_db_session.query(Policy).filter(Policy.premium_amount.isnot(None)).all()
    for customer_policy in customer_policies:
        total_premium_amount = 0
        if customer_policy.premium_frequency == "MONTHLY":
            total_premium_amount += float(customer_policy.premium_amount) * 12
        elif customer_policy.premium_frequency == "HALF YEARLY":
            total_premium_amount += float(customer_policy.premium_amount) * 2
        else:
            total_premium_amount += float(customer_policy.premium_amount)

        #print str(customer_policy.kyc_id) + "  " + str(total_premium_amount)

        if customer_policy_amounts_dict.has_key(customer_policy.kyc_id):
            customer_policy_amounts_dict[customer_policy.kyc_id] = float(customer_policy_amounts_dict[customer_policy.kyc_id]) + float(total_premium_amount)
        else:
            customer_policy_amounts_dict[customer_policy.kyc_id] = float(total_premium_amount)

    for cust_kyc in customer_policy_amounts_dict.keys():
        with open("customer_policies_value_update.csv", "ab") as text_file:
            update_statement = "UPDATE jubilee_suitecrm_db.accounts_cstm SET total_premium_value = "+ str(customer_policy_amounts_dict[cust_kyc])+ "  WHERE id = \'"+cust_kyc+"\'; \n"
            text_file.write(update_statement)

        # try:
        #     print str(cust_kyc) + ' ' + str({"total_premium_value": customer_policy_amounts_dict[cust_kyc]})
        #     suite_crm_db_session.query(AccountCSTM).filter_by(id=cust_kyc).update({"total_premium_value": customer_policy_amounts_dict[cust_kyc]})
        #     suite_crm_db_session.commit()
        # except exc.DataError, e:
        #     suite_crm_db_session.rollback()
        #     print "Unable to add Policy " #+ str(kyc_policy.policy_no)
        #     continue
        # except exc.IntegrityError, e:
        #     suite_crm_db_session.rollback()
        #     print "Unable to add Policy " #+ str(kyc_policy.policy_no)
        #     continue
        # except exc.OperationalError, e:
        #     suite_crm_db_session.rollback()
        #     print "Unable to add Policy " #+ str(kyc_policy.policy_no)
        #     continue

    #print customer_policy_amounts_dict

    # core_system_customers = suite_crm_db_session.query(AccountCSTM).filter(AccountCSTM.core_system_customer==1).all()
    # for core_system_customer in core_system_customers:
    #     print core_system_customer.id
    #     customer_policies = suite_crm_db_session.query(Policy).filter(Policy.kyc_id == core_system_customer.id).filter(Policy.line_of_business == 'LIFE').all()
    #     for customer_policy in customer_policies:
    #         print customer_policy.premium_amount
    print("dsdsdsds")