"""
This file has been automatically generated with workbench_alchemy v0.2.3
For more details please check here:
https://github.com/PiTiLeZarD/workbench_alchemy
"""
from sqlalchemy.sql.sqltypes import CHAR
from sqlalchemy.sql.sqltypes import BIGINT


from sqlalchemy import Column, DateTime, ForeignKey, Integer, String, func, Enum as ENUM, DateTime as TIMESTAMP,  String as VARCHAR, Date as DATE, Float as DOUBLE, DateTime as DATETIME, String as TEXT, Integer
from sqlalchemy.orm import backref, relationship
from database import Base

class INTEGER(Integer):
    def __init__(self, *args, **kwargs):super(Integer, self).__init__()

