import graphene
from graphene import resolve_only_args, relay
from graphene_sqlalchemy import SQLAlchemyConnectionField, SQLAlchemyObjectType
from models import CustomerDetails as CustomerDetailsModel


def connection_for_type(_type):
    class Connection(graphene.Connection):
        total_count = graphene.Int()

        class Meta:
            name = _type._meta.name + 'Connection'
            node = _type

        def resolve_total_count(self, args, context, info):
            return self.length

    return Connection

class CustomerDetails(SQLAlchemyObjectType):

    class Meta:
        model = CustomerDetailsModel
        interfaces = (relay.Node, )

class Query(graphene.ObjectType):
    customer_details = SQLAlchemyConnectionField(CustomerDetails,  vehicle_reg_no=graphene.String())
    customer_detail = relay.Node.Field(CustomerDetails)
    node = relay.Node.Field()
    get_customer_detail = relay.Node.Field(CustomerDetails, vehicle_reg_no=graphene.String())

    def resolve_customer_details(self, args, context, info):
        query = CustomerDetails.get_query(context) # SQLAlchemy query
        print(str(args))
        vehicleRegNo = args.get('vehicle_reg_no')
        print('Vehicle Re number is ' + str(vehicleRegNo))
        #return query.all()
        if vehicleRegNo==None:
            return query.all()
        else:
            return query.filter_by(vehicle_reg_no=vehicleRegNo).all()

        #return query.filter(CustomerDetailsModel.policy_no == policyNo)

premia_schema = graphene.Schema(query=Query, types=[CustomerDetails])
