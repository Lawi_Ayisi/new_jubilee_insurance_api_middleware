#!/usr/bin/env python

from flask import Flask

from models.database import premia_db_session

app = Flask(__name__)
app.debug = True





@app.teardown_appcontext
def shutdown_session(exception=None):
    premia_db_session.remove()

if __name__ == '__main__':
    #init_db()
    app.run(host='0.0.0.0')
