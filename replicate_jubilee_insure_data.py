#!/usr/bin/env python
from __future__ import print_function
import pymysql
import pymssql

mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugar', as_dict=True)
mysqlconn = '' #pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

#always append  'crm_regn_ctry_name' to columns_names
def importDataIntoTableNotes(table_name, columns_names):
    columns_tuple = "INSERT INTO "+table_name+" ("
    s_percentages = []
    for columns_name in columns_names:
        s_percentages.append("%s")
        if columns_tuple == "INSERT INTO "+table_name+" (":
            columns_tuple+=columns_name
        else:
            columns_tuple+= ","+columns_name
    columns_tuple+= ") "
    insert_query = columns_tuple+ "VALUES "+ str(tuple(s_percentages)).replace("'", "")
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print('Deleting '+ table_name)
    # Prepare mysql for this import
    if table_name == 'users':
        mysqlcur.execute("DELETE FROM "+table_name+ " WHERE id <> '1'")

    else:
        mysqlcur.execute("DELETE FROM "+table_name)
    mysqlcur.connection.commit()

    if table_name == 'users':
        mssqlcur.execute("SELECT * FROM "+table_name+ " WHERE id <> '1'")
        print("Selecting "+ table_name)
        leads_count = 0
        for row in mssqlcur:
            leads_count += 1
            a_list = []
            for columns_name in columns_names[:-1] :
                a_list.append(row[columns_name])
            a_list.append('kenya')
            a_list = tuple(a_list)

            mysqlcur.execute(insert_query, a_list)
            mysqlcur.connection.commit()
            print("Inserting "+table_name+ " count  " + str(leads_count))
    else:
        mssqlcur.execute("SELECT * FROM "+table_name)
        print("Selecting "+ table_name)
        leads_count = 0
        for row in mssqlcur:
            leads_count += 1
            a_list = []
            for columns_name in columns_names[:-1] :
                a_list.append(row[columns_name])
            a_list.append('kenya')
            a_list = tuple(a_list)

            mysqlcur.execute(insert_query, a_list)
            mysqlcur.connection.commit()
            print("Inserting "+table_name+ " count  " + str(leads_count))

    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()

def importUsers():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    #Prepare mysql for this import
    #mysqlcur.execute("DELETE FROM users WHERE user_name!='wasambo'")
    #mysqlcur.connection.commit()
    mysqlcur.execute("DELETE FROM users")
    mysqlcur.connection.commit()

    #Import Users
    #mssqlcur.execute("SELECT * FROM users WHERE user_name!='wasambo'")
    mssqlcur.execute("SELECT * FROM users")

    for row in mssqlcur:
        id = row['id']
        user_name = row['user_name']
        user_hash = row['user_hash']
        system_generated_password = row['system_generated_password']
        pwd_last_changed = row['pwd_last_changed']
        authenticate_id = row['authenticate_id']
        sugar_login = row['sugar_login']
        first_name = row['first_name']
        last_name = row['last_name']
        is_admin = row['is_admin']
        external_auth_only = row['external_auth_only']
        receive_notifications = row['receive_notifications']
        description = row['description']
        date_entered = row['date_entered']
        date_modified = row['date_modified']
        modified_user_id = row['modified_user_id']
        created_by = row['created_by']
        title = row['title']
        department = row['department']
        phone_home = row['phone_home']
        phone_mobile = row['phone_mobile']
        phone_work = row['phone_work']
        phone_other = row['phone_other']
        phone_fax = row['phone_fax']
        status = row['status']
        address_street = row['address_street']
        address_city = row['address_city']
        address_state = row['address_state']
        address_country =  row['address_country']
        address_postalcode = row['address_postalcode']
        deleted = row['deleted']
        portal_only = row['portal_only']
        show_on_employees = row['show_on_employees']
        employee_status = row['employee_status']
        messenger_id = row['messenger_id']
        messenger_type = row['messenger_type']
        reports_to_id = row['reports_to_id']
        is_group = row['is_group']
        crm_regn_ctry_name = 'kenya'


        query = "INSERT INTO users (id,user_name,user_hash,system_generated_password,pwd_last_changed,authenticate_id,sugar_login,first_name, last_name, is_admin, external_auth_only, receive_notifications,description,date_entered,date_modified, modified_user_id,  created_by, title, department, phone_home, phone_mobile, phone_work, phone_other, phone_fax, status, address_street, address_city, address_state,  address_country, address_postalcode, deleted, portal_only,show_on_employees, employee_status,  messenger_id, messenger_type, reports_to_id, is_group,crm_regn_ctry_name) " \
                "VALUES (%s,%s,%s,%s,%s,%s,%s,%s, %s, %s,%s, %s,%s,%s,%s, %s,  %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,  %s, %s, %s, %s,%s, %s,  %s, %s, %s, %s)"

        mysqlcur.execute(query, (id,user_name,user_hash,system_generated_password,pwd_last_changed,
        authenticate_id,sugar_login,first_name, last_name, is_admin, external_auth_only, receive_notifications,description,
        date_entered,date_modified, modified_user_id,  created_by, title, department, phone_home, phone_mobile, phone_work, phone_other,
        phone_fax, status, address_street, address_city, address_state,  address_country, address_postalcode, deleted, portal_only,
        show_on_employees, employee_status,  messenger_id, messenger_type, reports_to_id, is_group,crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print ("Inserting %s, %s into users table" % (first_name, last_name))

    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()

def importAccounts():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print ('Deleting accounts')
    #Prepare mysql for this import
    mysqlcur.execute("DELETE FROM accounts")
    mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM accounts")
    all_accounts = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM accounts')
    print ('Selecting accounts')
    accounts_count = 0
    for row in mssqlcur:
        accounts_count +=1
        rating = row['rating']
        billing_address_state = row['billing_address_state']
        billing_address_city = row['billing_address_city']
        campaign_id = row['campaign_id']
        assigned_user_id = row['assigned_user_id']
        phone_fax = row['phone_fax']
        shipping_address_street = row['shipping_address_street']
        ticker_symbol = row['ticker_symbol']
        id = row['id']
        date_entered = row['date_entered']
        phone_alternate = row['phone_alternate']
        billing_address_postalcode = row['billing_address_postalcode']
        shipping_address_country =  row['shipping_address_country']
        created_by = row['created_by']
        parent_id = row['parent_id']
        website = row['website']
        account_type = row['account_type']
        description = row['description']
        shipping_address_city = row['shipping_address_city']
        shipping_address_state = row['shipping_address_state']
        deleted = row['deleted']
        phone_office = row['phone_office']
        billing_address_country =  row['billing_address_country']
        ownership = row['ownership']
        billing_address_street = row['billing_address_street']
        sic_code = row['sic_code']
        modified_user_id = row['modified_user_id']
        name = row['name']
        date_modified = row['date_modified']
        industry = row['industry']
        annual_revenue = row['annual_revenue']
        employees = row['employees']
        shipping_address_postalcode = row['shipping_address_postalcode']
        crm_regn_ctry_name = 'kenya'

        query = "INSERT INTO accounts (rating,billing_address_state,billing_address_city,campaign_id, assigned_user_id, phone_fax,shipping_address_street,ticker_symbol, id, date_entered, phone_alternate, billing_address_postalcode,shipping_address_country, created_by, parent_id, website, account_type, description,shipping_address_city, shipping_address_state,deleted,phone_office,billing_address_country,ownership,billing_address_street,sic_code,modified_user_id,name,date_modified,industry,annual_revenue,employees,shipping_address_postalcode,crm_regn_ctry_name) " \
                "VALUES (%s,%s,%s,%s, %s, %s,%s,%s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s,%s,%s,%s, %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

        mysqlcur.execute(query, (rating,billing_address_state,billing_address_city,campaign_id, assigned_user_id, phone_fax,
        shipping_address_street,ticker_symbol, id, date_entered, phone_alternate, billing_address_postalcode,
        shipping_address_country, created_by, parent_id, website, account_type, description,
        shipping_address_city, shipping_address_state,deleted,phone_office,billing_address_country,
        ownership,billing_address_street,sic_code,modified_user_id,name,date_modified,industry,
        annual_revenue,employees,shipping_address_postalcode,crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print ("Inserting account count  "+ str(accounts_count))
        print ("ID=%s, Name=%s" % (id, crm_regn_ctry_name))
        print ("ID=%s, Name=%s" % (row['id'], row['name']))


    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()


def importCases():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugar', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    #print ('Deleting cases')
    #Prepare mysql for this import
    #mysqlcur.execute("DELETE FROM cases")
    #mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM cases WHERE date_entered BETWEEN '2017-01-29 00:00:00.000' AND '2017-02-8 00:00:00.000' ")
    all_cases = len(list(mssqlcur.fetchall()))

    mssqlcur.execute("SELECT * FROM cases WHERE date_entered BETWEEN '2017-01-29 00:00:00.000' AND '2017-02-8 00:00:00.000' ")
    print ('Selecting cases')
    cases_count = 0
    for row in mssqlcur:
        cases_count +=1

        id = row['id']
        name = row['name']
        date_entered = row['date_entered']
        date_modified = row['date_modified']
        modified_user_id = row['modified_user_id']
        created_by = row['created_by']
        description = row['description']
        deleted = row['deleted']
        assigned_user_id = row['assigned_user_id']
        case_number = row['case_number']
        type = row['type']
        status = row['status']
        priority = row['priority']
        resolution = row['resolution']
        work_log = row['work_log']
        account_id = row['account_id']
        crm_regn_ctry_name =  'kenya'

        query = "INSERT INTO cases (id,name ,date_entered,date_modified ,modified_user_id , created_by , description, deleted , assigned_user_id, case_number, type,status,priority, resolution, work_log ,account_id,crm_regn_ctry_name) "\
                "VALUES (%s,%s ,%s,%s ,%s , %s , %s, %s , %s,%s, %s,%s,%s, %s, %s ,%s,%s)" \
                "ON DUPLICATE KEY UPDATE name=VALUES(name);"


        mysqlcur.execute(query, (id,name ,date_entered,date_modified ,modified_user_id , created_by , description, deleted , assigned_user_id, case_number, type,status,priority, resolution, work_log ,account_id,crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print ("Inserting case count  "+ str(cases_count))
        print ("ID=%s, Status=%s" % (id, status))
        print ("ID=%s, Name=%s" % (row['id'], row['name']))


    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()


def importCasesCSTM():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print ('Deleting cases_cstm')
    #Prepare mysql for this import
    mysqlcur.execute("DELETE FROM cases_cstm")
    mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM cases_cstm")
    all_cases = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM cases_cstm')
    print ('Selecting cases_cstm')
    cases_count = 0
    for row in mssqlcur:
        cases_count +=1

        id_c = row['id_c']
        business_type_c = row['business_type_c']
        channel_type_c = row['channel_type_c']
        product_type_c = row['product_type_c']
        location_c = row['location_c']
        subject_type_c = row['subject_type_c']
        hospitals_c = row['hospitals_c']
        policy_no_c = row['policy_no_c']
        crm_regn_ctry_name =  'kenya'

        query = "INSERT INTO cases_cstm (id_c, business_type_c, channel_type_c , product_type_c , location_c, subject_type_c ,  hospitals_c, policy_no_c ,crm_regn_ctry_name) " \
                "VALUES (%s, %s, %s , %s , %s, %s ,  %s, %s ,%s)"

        mysqlcur.execute(query, (id_c, business_type_c, channel_type_c , product_type_c , location_c, subject_type_c ,  hospitals_c, policy_no_c ,crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print ("Inserting case_cstm count  "+ str(cases_count))
        print ("business_type_c=%s, Status=%s" % (business_type_c, policy_no_c))
        print ("business_type_c=%s, policy_no_c=%s" % (row['business_type_c'], row['policy_no_c']))


    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()


def importOpportunities():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugar', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print ('Deleting opportunities')
    #Prepare mysql for this import
    #mysqlcur.execute("DELETE FROM opportunities")
    #mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM opportunities WHERE date_entered BETWEEN '2017-01-29 00:00:00.000' AND '2017-02-8 00:00:00.000' ")
    all_opportunities = len(list(mssqlcur.fetchall()))

    mssqlcur.execute("SELECT * FROM opportunities WHERE date_entered BETWEEN '2017-01-29 00:00:00.000' AND '2017-02-8 00:00:00.000' ")
    print ('Selecting opportunities')
    opportunities_count = 0
    for row in mssqlcur:
        opportunities_count +=1

        id = row['id']
        name = row['name']
        date_entered = row['date_entered']
        date_modified = row['date_modified']
        modified_user_id = row['modified_user_id']
        created_by = row['created_by']
        description = row['description']
        deleted = row['deleted']
        assigned_user_id = row['assigned_user_id']
        opportunity_type = row['opportunity_type']
        campaign_id = row['campaign_id']
        lead_source = row['lead_source']
        amount = row['amount']
        amount_usdollar = row['amount_usdollar']
        currency_id = row['currency_id']
        date_closed = row['date_closed']
        next_step = row['next_step']
        sales_stage = row['sales_stage']
        probability = row['probability']
        crm_regn_ctry_name =  'kenya'

        query = "INSERT INTO opportunities (id,name,date_entered,date_modified,modified_user_id, created_by,description,deleted,assigned_user_id,opportunity_type,campaign_id,lead_source,amount,amount_usdollar,currency_id,date_closed,next_step,sales_stage,probability,crm_regn_ctry_name) " \
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)" \
                "ON DUPLICATE KEY UPDATE name=VALUES(name);"

        mysqlcur.execute(query, (id,name,date_entered,date_modified,modified_user_id, created_by,description,deleted,assigned_user_id,opportunity_type,campaign_id,lead_source,amount,amount_usdollar,currency_id,date_closed,next_step,sales_stage,probability,crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print ("Inserting opportunities count  "+ str(opportunities_count))
        print ("ID=%s, Status=%s" % (name, date_closed))
        print ("ID=%s, Name=%s" % (row['name'], row['date_closed']))


    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()

def importOpportunitiesContacts():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print ('Deleting opportunities_contacts')
    #Prepare mysql for this import
    mysqlcur.execute("DELETE FROM opportunities_contacts")
    mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM opportunities_contacts")
    all_opportunities_contacts = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM opportunities_contacts')
    print ('Selecting opportunities_contacts')
    opportunities_contacts_count = 0
    for row in mssqlcur:
        opportunities_contacts_count +=1

        id = row['id']
        contact_id = row['contact_id']
        opportunity_id = row['opportunity_id']
        contact_role = row['contact_role']
        date_modified = row['date_modified']
        deleted = row['deleted']
        crm_regn_ctry_name =  'kenya'

        query = "INSERT INTO opportunities_contacts (id,contact_id,opportunity_id,contact_role,date_modified,deleted,crm_regn_ctry_name) " \
                "VALUES (%s,%s,%s,%s,%s,%s,%s)"

        mysqlcur.execute(query, (id,contact_id,opportunity_id,contact_role,date_modified,deleted,crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print ("Inserting opportunities_contacts count  "+ str(opportunities_contacts_count))


    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()

def importOpportunitiesContacts():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print ('Deleting opportunities_contacts')
    #Prepare mysql for this import
    mysqlcur.execute("DELETE FROM opportunities_contacts")
    mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM opportunities_contacts")
    all_opportunities_contacts = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM opportunities_contacts')
    print ('Selecting opportunities_contacts')
    opportunities_contacts_count = 0
    for row in mssqlcur:
        opportunities_contacts_count +=1

        id = row['id']
        contact_id = row['contact_id']
        opportunity_id = row['opportunity_id']
        contact_role = row['contact_role']
        date_modified = row['date_modified']
        deleted = row['deleted']
        crm_regn_ctry_name =  'kenya'

        query = "INSERT INTO opportunities_contacts (id,contact_id,opportunity_id,contact_role,date_modified,deleted,crm_regn_ctry_name) " \
                "VALUES (%s,%s,%s,%s,%s,%s,%s)"

        mysqlcur.execute(query, (id,contact_id,opportunity_id,contact_role,date_modified,deleted,crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print ("Inserting opportunities_contacts count  "+ str(opportunities_contacts_count))


    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()

def importOpportunitiesCSTM():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print ('Deleting opportunities_cstm')
    #Prepare mysql for this import
    mysqlcur.execute("DELETE FROM opportunities_cstm")
    mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM opportunities_cstm")
    all_opportunities = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM opportunities_cstm')
    print ('Selecting opportunities')
    opportunities_count = 0
    for row in mssqlcur:
        opportunities_count +=1
        id_c = row['id_c']
        opportunity_name_c = row['opportunity_name_c']
        crm_regn_ctry_name =  'kenya'

        query = "INSERT INTO opportunities_cstm (id_c,opportunity_name_c,crm_regn_ctry_name) " \
                "VALUES (%s, %s, %s)"

        mysqlcur.execute(query, (id_c,opportunity_name_c,crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print ("Inserting opportunities_cstm count  "+ str(opportunities_count))
        print ("ID=%s, opportunity_name_c=%s" % (id_c, opportunity_name_c))
        print ("ID=%s, opportunity_name_c=%s" % (row['id_c'], row['opportunity_name_c']))


    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()

def importLeads():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print ('Deleting leads')
    #Prepare mysql for this import
    mysqlcur.execute("DELETE FROM leads")
    mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM leads")
    all_leads = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM leads')
    print ('Selecting leads')
    leads_count = 0
    for row in mssqlcur:
        leads_count +=1

        id = row['id']
        date_entered = row['date_entered']
        date_modified = row['date_modified']
        modified_user_id = row['modified_user_id']
        created_by = row['created_by']
        description = row['description']
        deleted = row['deleted']
        assigned_user_id = row['assigned_user_id']
        salutation = row['salutation']
        first_name = row['first_name']
        last_name = row['last_name']
        title = row['title']
        department = row['department']
        do_not_call = row['do_not_call']
        phone_home = row['phone_home']
        phone_mobile = row['phone_mobile']
        phone_work = row['phone_work']
        phone_other = row['phone_other']
        phone_fax = row['phone_fax']
        primary_address_street = row['primary_address_street']
        primary_address_city = row['primary_address_city']
        primary_address_state = row['primary_address_state']
        primary_address_postalcode = row['primary_address_postalcode']
        primary_address_country=  row['primary_address_country']
        alt_address_street = row['alt_address_street']
        alt_address_city = row['alt_address_city']
        alt_address_state = row['alt_address_state']
        alt_address_postalcode = row['alt_address_postalcode']
        alt_address_country =  row['alt_address_country']
        assistant = row['assistant']
        assistant_phone = row['assistant_phone']
        converted = row['converted']
        refered_by = row['refered_by']
        lead_source = row['lead_source']
        lead_source_description = row['lead_source_description']
        status = row['status']
        status_description = row['status_description']
        reports_to_id = row['reports_to_id']
        account_name = row['account_name']
        account_description = row['account_description']
        contact_id = row['contact_id']
        account_id = row['account_id']
        opportunity_id = row['opportunity_id']
        opportunity_name = row['opportunity_name']
        opportunity_amount = row['opportunity_amount']
        campaign_id = row['campaign_id']
        birthdate = row['birthdate']
        portal_name = row['portal_name']
        portal_app = row['portal_app']
        website = row['website']
        crm_regn_ctry_name =  'kenya'

        query = "INSERT INTO leads (id, date_entered,date_modified,modified_user_id,created_by,description,deleted,assigned_user_id,salutation,first_name,last_name, title,department,do_not_call,phone_home,phone_mobile,phone_work,phone_other,phone_fax, primary_address_street,primary_address_city,primary_address_state,primary_address_postalcode,primary_address_country,alt_address_street,alt_address_city,alt_address_state,alt_address_postalcode,alt_address_country,assistant,assistant_phone,converted,refered_by,lead_source,lead_source_description,status,status_description,reports_to_id,account_name,account_description,contact_id,account_id,opportunity_id,opportunity_name,opportunity_amount,campaign_id,birthdate,portal_name,portal_app,website,crm_regn_ctry_name) " \
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, % s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s)"

        mysqlcur.execute(query, (id, date_entered, date_modified, modified_user_id, created_by, description, deleted, assigned_user_id,
         salutation, first_name, last_name, title, department, do_not_call, phone_home, phone_mobile, phone_work,
         phone_other, phone_fax, primary_address_street, primary_address_city, primary_address_state,
         primary_address_postalcode, primary_address_country, alt_address_street, alt_address_city, alt_address_state,
         alt_address_postalcode, alt_address_country, assistant, assistant_phone, converted, refered_by, lead_source,
         lead_source_description, status, status_description, reports_to_id, account_name, account_description,
         contact_id, account_id, opportunity_id, opportunity_name, opportunity_amount, campaign_id, birthdate,
         portal_name, portal_app, website, crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print ("Inserting leads count  "+ str(leads_count))
        print ("ID=%s, last_name=%s" % (first_name, last_name))
        print ("ID=%s, last_name=%s" % (row['first_name'], row['last_name']))


    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()


def importLeadsCSTM():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print ('Deleting leads_cstm')
    #Prepare mysql for this import
    mysqlcur.execute("DELETE FROM leads_cstm")
    mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM leads_cstm")
    all_opportunities = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM leads_cstm')
    print ('Selecting opportunities')
    opportunities_count = 0
    for row in mssqlcur:
        opportunities_count +=1

        id_c = row['id_c']
        opportunity_name_c = row['opportunity_name_c']
        crm_regn_ctry_name =  'kenya'

        query = "INSERT INTO leads_cstm (id_c,opportunity_name_c,crm_regn_ctry_name) " \
                "VALUES (%s, %s, %s)"

        mysqlcur.execute(query, (id_c,opportunity_name_c,crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print ("Inserting leads_cstm count  "+ str(opportunities_count))
        print ("ID=%s, opportunity_name_c=%s" % (id_c, opportunity_name_c))
        print ("ID=%s, opportunity_name_c=%s" % (row['id_c'], row['opportunity_name_c']))


    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()


def importAccountsOpportunities():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugar', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    mssqlcur.execute("SELECT * FROM accounts_opportunities")
    all_leads = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM accounts_opportunities')
    print ('Selecting accounts_contacts')
    leads_count = 0
    for row in mssqlcur:
        leads_count +=1
        id = row['id']
        opportunity_id = row['opportunity_id']
        account_id = row['account_id']
        date_modified = row['date_modified']
        deleted = row['deleted']
        crm_regn_ctry_name =  'kenya'

        query = "INSERT INTO accounts_opportunities (id,opportunity_id,account_id,date_modified,deleted,crm_regn_ctry_name) " \
                "VALUES (%s,%s,%s,%s,%s,%s)"

        mysqlcur.execute(query, (id,opportunity_id,account_id,date_modified,deleted,crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print ("Inserting accounts_opportunities count  "+ str(leads_count))
        print ("ID=%s, last_name=%s" % (opportunity_id, date_modified))
        print ("ID=%s, last_name=%s" % (row['account_id'], row['date_modified']))

    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()

def importAccountsContacts():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print ('Deleting accounts_contacts')
    #Prepare mysql for this import
    mysqlcur.execute("DELETE FROM accounts_contacts")
    mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM accounts_contacts")
    all_leads = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM accounts_contacts')
    print ('Selecting accounts_contacts')
    leads_count = 0
    for row in mssqlcur:
        leads_count +=1
        id = row['id']
        contact_id = row['contact_id']
        account_id = row['account_id']
        date_modified = row['date_modified']
        deleted = row['deleted']
        crm_regn_ctry_name =  'kenya'

        query = "INSERT INTO accounts_contacts (id,contact_id,account_id,date_modified,deleted,crm_regn_ctry_name) " \
                "VALUES (%s,%s,%s,%s,%s,%s)"

        mysqlcur.execute(query, (id,contact_id,account_id,date_modified,deleted,crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print ("Inserting accounts_contacts count  "+ str(leads_count))
        print ("ID=%s, last_name=%s" % (account_id, date_modified))
        print ("ID=%s, last_name=%s" % (row['account_id'], row['date_modified']))

    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()

def importSecurityGroups():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print('Deleting securitygroups')
    # Prepare mysql for this import
    mysqlcur.execute("DELETE FROM securitygroups")
    mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM securitygroups")
    #all_leads = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM securitygroups')
    print('Selecting calls')
    leads_count = 0
    for row in mssqlcur:
        leads_count += 1
        id = row['id']
        name = row['name']
        date_entered = row['date_entered']
        date_modified = row['date_modified']
        modified_user_id = row['modified_user_id']
        created_by = row['created_by']
        description = row['description']
        deleted = row['deleted']
        assigned_user_id = row['assigned_user_id']
        noninheritable = row['noninheritable']
        crm_regn_ctry_name =  'kenya'

        query = "INSERT INTO securitygroups (id, name, date_entered, date_modified, modified_user_id, created_by, description, deleted, assigned_user_id,noninheritable, crm_regn_ctry_name) " \
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s)"

        mysqlcur.execute(query, (id, name, date_entered, date_modified, modified_user_id, created_by, description, deleted, assigned_user_id,noninheritable, crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print("Inserting securitygroups count  " + str(leads_count))
        print("ID=%s, last_name=%s" % (name, name))

    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()

def importCalls():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print('Deleting calls')
    # Prepare mysql for this import
    mysqlcur.execute("DELETE FROM calls")
    mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM calls")
    #all_leads = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM calls')
    print('Selecting calls')
    leads_count = 0
    for row in mssqlcur:
        leads_count += 1

        id = row['id']
        name = row['name']
        date_entered = row['date_entered']
        date_modified = row['date_modified']
        modified_user_id = row['modified_user_id']
        created_by = row['created_by']
        description = row['description']
        deleted = row['deleted']
        assigned_user_id = row['assigned_user_id']
        duration_hours = row['duration_hours']
        duration_minutes = row['duration_minutes']
        date_start = row['date_start']
        date_end = row['date_end']
        parent_type = row['parent_type']
        status = row['status']
        direction = row['direction']
        parent_id = row['parent_id']
        reminder_time = row['reminder_time']
        email_reminder_time = row['email_reminder_time']
        email_reminder_sent = row['email_reminder_sent']
        outlook_id = row['outlook_id']
        repeat_type = row['repeat_type']
        repeat_interval = row['repeat_interval']
        repeat_dow = row['repeat_dow']
        repeat_until = row['repeat_until']
        repeat_count = row['repeat_count']
        repeat_parent_id = row['repeat_parent_id']
        recurring_source = row['recurring_source']
        crm_regn_ctry_name =  'kenya'

        query = "INSERT INTO calls (id , name , date_entered , date_modified,modified_user_id, created_by, description,deleted, assigned_user_id, duration_hours,duration_minutes, date_start, date_end,parent_type,status, direction,parent_id,reminder_time,email_reminder_time,email_reminder_sent,outlook_id,repeat_type, repeat_interval,repeat_dow,repeat_until,repeat_count, repeat_parent_id, recurring_source,crm_regn_ctry_name) " \
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s)"

        mysqlcur.execute(query, (id , name , date_entered , date_modified,modified_user_id, created_by, description,deleted, assigned_user_id, duration_hours,duration_minutes, date_start, date_end,parent_type,status, direction,parent_id,reminder_time,email_reminder_time,email_reminder_sent,outlook_id,repeat_type, repeat_interval,repeat_dow,repeat_until,repeat_count, repeat_parent_id, recurring_source,crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print("Inserting calls count  " + str(leads_count))
        print("ID=%s, last_name=%s" % (name, name))

    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()


def importSecurityGroupsUsers():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print('Deleting securitygroups_users')
    # Prepare mysql for this import
    mysqlcur.execute("DELETE FROM securitygroups_users")
    mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM securitygroups_users")
    #all_leads = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM securitygroups_users')
    print('Selecting securitygroups_users')
    leads_count = 0
    for row in mssqlcur:
        leads_count += 1
        id = row['id']
        date_modified = row['date_modified']
        deleted = row['deleted']
        securitygroup_id = row['securitygroup_id']
        user_id = row['user_id']
        primary_group = row['primary_group']
        noninheritable = row['noninheritable']
        crm_regn_ctry_name =  'kenya'

        query = "INSERT INTO securitygroups_users (id,date_modified,deleted,securitygroup_id,user_id,primary_group,noninheritable,crm_regn_ctry_name) " \
                "VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"

        mysqlcur.execute(query, (id,date_modified,deleted,securitygroup_id,user_id,primary_group,noninheritable,crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print("Inserting securitygroups_users count  " + str(leads_count))
        print("ID=%s, last_name=%s" % (securitygroup_id, securitygroup_id))

    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()



def importSecurityGroupsRecords():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print('Deleting securitygroups_records')
    # Prepare mysql for this import
    mysqlcur.execute("DELETE FROM securitygroups_records")
    mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM securitygroups_records")
    #all_leads = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM securitygroups_records')
    print('Selecting securitygroups_records')
    leads_count = 0
    for row in mssqlcur:
        leads_count += 1

        id = row['id']
        securitygroup_id = row['securitygroup_id']
        record_id = row['record_id']
        module = row['module']
        date_modified = row['date_modified']
        modified_user_id = row['modified_user_id']
        created_by = row['created_by']
        deleted = row['deleted']
        crm_regn_ctry_name =  'kenya'

        query = "INSERT INTO securitygroups_records (id,securitygroup_id,record_id,module,date_modified,modified_user_id,created_by,deleted,crm_regn_ctry_name) " \
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"

        mysqlcur.execute(query, (id,securitygroup_id,record_id,module,date_modified,modified_user_id,created_by,deleted,crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print("Inserting securitygroups_records count  " + str(leads_count))
        print("ID=%s, last_name=%s" % (securitygroup_id, securitygroup_id))

    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()




def importEmails():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print('Deleting Emails')
    # Prepare mysql for this import
    mysqlcur.execute("DELETE FROM emails")
    mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM emails")
    #all_leads = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM emails')
    print('Selecting Emails')
    leads_count = 0
    for row in mssqlcur:
        leads_count += 1
        id = row['id']
        date_entered = row['date_entered']
        date_modified = row['date_modified']
        assigned_user_id = row['assigned_user_id']
        modified_user_id = row['modified_user_id']
        created_by = row['created_by']
        deleted = row['deleted']
        date_sent = row['date_sent']
        message_id = row['message_id']
        name = row['name']
        type = row['type']
        status = row['status']
        flagged = row['flagged']
        reply_to_status = row['reply_to_status']
        intent = row['intent']
        mailbox_id = row['mailbox_id']
        parent_type = row['parent_type']
        parent_id = row['parent_id']
        crm_regn_ctry_name =  'kenya'


        query = "INSERT INTO emails (id,date_entered,date_modified,assigned_user_id, modified_user_id, created_by,  deleted,   date_sent,message_id,  name,  type,  status,  flagged, reply_to_status,  intent,  mailbox_id, parent_type, parent_id,   crm_regn_ctry_name) " \
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

        mysqlcur.execute(query, (id,date_entered,date_modified,assigned_user_id, modified_user_id, created_by,  deleted,   date_sent,message_id,  name,  type,  status,  flagged, reply_to_status,  intent,  mailbox_id, parent_type, parent_id,   crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print("Inserting emails count  " + str(leads_count))
        print("ID=%s, last_name=%s" % (date_modified, date_modified))

    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()

def importNotes():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print('Deleting notes')
    # Prepare mysql for this import
    mysqlcur.execute("DELETE FROM notes")
    mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM notes")
    #all_leads = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM notes')
    print('Selecting notes')
    leads_count = 0
    for row in mssqlcur:
        leads_count += 1

        assigned_user_id = row['assigned_user_id']
        id = row['id']
        date_entered = row['date_entered']
        date_modified = row['date_modified']
        modified_user_id = row['modified_user_id']
        created_by = row['created_by']
        name = row['name']
        file_mime_type = row['file_mime_type']
        filename = row['filename']
        parent_type = row['parent_type']
        parent_id = row['parent_id']
        contact_id = row['contact_id']
        portal_flag = row['portal_flag']
        embed_flag = row['embed_flag']
        description = row['description']
        deleted = row['deleted']
        crm_regn_ctry_name =  'kenya'

        query = "INSERT INTO notes (assigned_user_id, id,date_entered,date_modified,modified_user_id,created_by,name,file_mime_type,filename,parent_type,parent_id,contact_id,portal_flag,embed_flag,description,deleted,crm_regn_ctry_name) " \
                "VALUES (%s, %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

        mysqlcur.execute(query, (assigned_user_id, id,date_entered,date_modified,modified_user_id,created_by,name,file_mime_type,filename,parent_type,
        parent_id,contact_id,portal_flag,embed_flag,description,deleted,crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print("Inserting notes count  " + str(leads_count))
        print("ID=%s, last_name=%s" % (date_modified, date_modified))

    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()


def importACLActions():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print('Deleting acl_actions')
    # Prepare mysql for this import
    mysqlcur.execute("DELETE FROM acl_actions")
    mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM acl_actions")
    #all_leads = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM acl_actions')
    print('Selecting acl_actions')
    leads_count = 0
    for row in mssqlcur:
        leads_count += 1

        id = row['id']
        date_entered = row['date_entered']
        date_modified = row['date_modified']
        modified_user_id = row['modified_user_id']
        created_by = row['created_by']
        name = row['name']
        category = row['category']
        acltype = row['acltype']
        aclaccess = row['aclaccess']
        deleted = row['deleted']
        crm_regn_ctry_name =  'kenya'

        query = "INSERT INTO acl_actions (id,date_entered,date_modified,modified_user_id,created_by,name,category,acltype,aclaccess,deleted,crm_regn_ctry_name) " \
                "VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

        mysqlcur.execute(query, (id,date_entered,date_modified,modified_user_id,created_by,name,category,acltype,aclaccess,deleted,crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print("Inserting acl_actions count  " + str(leads_count))
        print("ID=%s, last_name=%s" % (date_modified, date_modified))

    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()


# def importAlerts():
#     mssqlcur = mssqlconn.cursor()
#     mysqlcur = mysqlconn.cursor()
#
#     print('Deleting alerts')
#     # Prepare mysql for this import
#     mysqlcur.execute("DELETE FROM alerts")
#     mysqlcur.connection.commit()
#
#     mssqlcur.execute("SELECT * FROM alerts")
#     all_leads = len(list(mssqlcur.fetchall()))
#
#     mssqlcur.execute('SELECT * FROM alerts')
#     print('Selecting alerts')
#     leads_count = 0
#     for row in mssqlcur:
#         leads_count += 1
#
#         assined_user_id = row['assined_user_id']
#         bean = row['bean']
#         bean_id = row['bean_id']
#         crm_regn_ctry_name =  'kenya'
#
#
#         query = "INSERT INTO alerts (assined_user_id,bean,bean_id,crm_regn_ctry_name) " \
#                 "VALUES (%s,%s,%s,%s)"
#
#         mysqlcur.execute(query, (assined_user_id,bean,bean_id,crm_regn_ctry_name))
#
#         mysqlcur.connection.commit()
#         print("Inserting alerts count  " + str(leads_count))
#         print("ID=%s, last_name=%s" % (date_modified, date_modified))
#
#     mssqlcur.close()
#     mssqlconn.close()
#     mysqlcur.close()
#     mysqlconn.close()


def importACLRoles():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print('Deleting acl_roles')
    # Prepare mysql for this import
    mysqlcur.execute("DELETE FROM acl_roles")
    mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM acl_roles")
    all_leads = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM acl_roles')
    print('Selecting acl_roles')
    leads_count = 0
    for row in mssqlcur:
        leads_count += 1

        id = row['id']
        date_entered = row['date_entered']
        date_modified = row['date_modified']
        modified_user_id = row['modified_user_id']
        created_by = row['created_by']
        name = row['name']
        description = row['description']
        deleted = row['deleted']
        crm_regn_ctry_name =  'kenya'


        query = "INSERT INTO acl_roles (id,date_entered, date_modified,modified_user_id, created_by, name,description,deleted,crm_regn_ctry_name) " \
                "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"

        mysqlcur.execute(query, (id, date_entered, date_modified, modified_user_id, created_by, name, description, deleted, crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print("Inserting acl_roles count  " + str(leads_count))
        print("ID=%s, last_name=%s" % (date_modified, date_modified))

    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()

def importRelationships():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print('Deleting relationships')
    # Prepare mysql for this import
    mysqlcur.execute("DELETE FROM relationships")
    mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM relationships")
    all_leads = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM relationships')
    print('Selecting relationships')
    leads_count = 0
    for row in mssqlcur:
        leads_count += 1

        id = row['id']
        relationship_name = row['relationship_name']
        lhs_module = row['lhs_module']
        lhs_table = row['lhs_table']
        lhs_key = row['lhs_key']
        rhs_module = row['rhs_module']
        rhs_table = row['rhs_table']
        rhs_key = row['rhs_key']
        join_table = row['join_table']
        join_key_lhs = row['join_key_lhs']
        join_key_rhs = row['join_key_rhs']
        relationship_type = row['relationship_type']
        relationship_role_column = row['relationship_role_column']
        relationship_role_column_value = row['relationship_role_column_value']
        reverse = row['reverse']
        deleted = row['deleted']
        crm_regn_ctry_name =  'kenya'

        query = "INSERT INTO relationships (id,relationship_name,lhs_module,lhs_table,lhs_key,rhs_module,rhs_table,rhs_key,join_table,join_key_lhs,join_key_rhs,relationship_type,relationship_role_column,relationship_role_column_value,reverse,deleted,crm_regn_ctry_name) " \
                "VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

        mysqlcur.execute(query, (id,relationship_name,lhs_module,lhs_table,lhs_key,rhs_module,rhs_table,rhs_key,join_table,join_key_lhs,join_key_rhs,relationship_type,relationship_role_column,relationship_role_column_value,reverse,deleted,crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print("Inserting relationships count  " + str(leads_count))
        print("ID=%s, last_name=%s" % (relationship_name, relationship_name))

    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()

def importEmailAddresses():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print('Deleting email_addresses')
    # Prepare mysql for this import
    mysqlcur.execute("DELETE FROM email_addresses")
    mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM email_addresses")
    all_leads = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM email_addresses')
    print('Selecting email_addresses')
    leads_count = 0
    for row in mssqlcur:
        leads_count += 1

        id = row['id']
        email_address = row['email_address']
        email_address_caps = row['email_address_caps']
        invalid_email = row['invalid_email']
        opt_out = row['opt_out']
        date_created = row['date_created']
        date_modified = row['date_modified']
        deleted = row['deleted']
        crm_regn_ctry_name =  'kenya'

        query = "INSERT INTO email_addresses (id,email_address,email_address_caps,invalid_email,opt_out,date_created,date_modified,deleted,crm_regn_ctry_name) " \
                "VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"

        mysqlcur.execute(query, (id,email_address,email_address_caps,invalid_email,opt_out,date_created,date_modified,deleted,crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print("Inserting email_addresses count  " + str(leads_count))
        print("ID=%s, last_name=%s" % (email_address, email_address))

    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()

def importEmailAddrBeanRel():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    print('Deleting email_addr_bean_rel')
    # Prepare mysql for this import
    mysqlcur.execute("DELETE FROM email_addr_bean_rel")
    mysqlcur.connection.commit()

    mssqlcur.execute("SELECT * FROM email_addr_bean_rel")
    all_leads = len(list(mssqlcur.fetchall()))

    mssqlcur.execute('SELECT * FROM email_addr_bean_rel')
    print('Selecting email_addr_bean_rel')
    leads_count = 0
    for row in mssqlcur:
        leads_count += 1

        id = row['id']
        email_address_id = row['email_address_id']
        bean_id = row['bean_id']
        bean_module = row['bean_module']
        primary_address = row['primary_address']
        reply_to_address = row['reply_to_address']
        date_created = row['date_created']
        date_modified = row['date_modified']
        deleted = row['deleted']
        crm_regn_ctry_name = 'kenya'

        query = "INSERT INTO email_addr_bean_rel (id,email_address_id,bean_id,bean_module,primary_address,reply_to_address,date_created,date_modified,deleted,crm_regn_ctry_name) " \
                "VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

        mysqlcur.execute(query, (id,email_address_id,bean_id,bean_module,primary_address,reply_to_address,date_created,date_modified,deleted,crm_regn_ctry_name))

        mysqlcur.connection.commit()
        print("Inserting email_addr_bean_rel count  " + str(leads_count))
        print("ID=%s, last_name=%s" % (email_address_id, email_address_id))

    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()


def importAlterDefaultMysqlTable():
    mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')

    mssqlcur = mssqlconn.cursor()
    mysqlcur = mysqlconn.cursor()

    #=========================================================================
    #It's a basic solution and need optimizing but the below example returns both column header and column value in a list.
    def return_mssql_columns_names_dict(table_name):
        try:
            mssqlconn = pymssql.connect(host='127.0.0.1', user='sa', password='yourStrong(!)Password', database='sugarcrm_db', as_dict=True)
            mssqlcur = mssqlconn.cursor()
            mssqlcur.execute("SELECT TOP 1 * FROM " + table_name)

            column_names = []
            query = ""
            for row in mssqlcur: #this will execute once coz its just on record on db
                for column_name, row in zip(mssqlcur.description, row):
                    column_names.append(column_name[0])
                column_names.append('crm_regn_ctry_name')
                print("column names "+ str(column_names))

            mssqlcur.close()
            mssqlconn.close()
            return column_names

        except Exception, e:
            print ('%s' % (e))

    #=========================================================================

    print('===================mssql tables=================')
    # Apparently this is MSSQL's equivalent of 'SHOW TABLES'
    mssqlcur.execute("SELECT name FROM SYSOBJECTS where TYPE = 'U' order by NAME")
    for (table_name) in mssqlcur:
        #table_name = table_name[0]
        table_name = str(table_name).replace("{u'name': u'","").replace("'}","")
        skipable_tables = ['config', 'securitygroups_message', 'versions']
        if table_name in skipable_tables or table_name.startswith('kreports' ) or table_name.startswith('twtnx' ):
           #Do nothing
           print("table is "+ table_name+ " skipping import")
        else:
           importDataIntoTableNotes(table_name, return_mssql_columns_names_dict(table_name))

    mssqlcur.close()
    mssqlconn.close()
    mysqlcur.close()
    mysqlconn.close()

#importUsers()
#importAccounts()
#importCases()
#importCasesCSTM()
#importOpportunities()
#importOpportunitiesContacts()
#importOpportunitiesCSTM()
#importLeads()
#importAccountsContacts()
importAccountsOpportunities()
#importCalls()
#importSecurityGroups()
#importSecurityGroupsUsers()
#importSecurityGroupsRecords()
#importNotes()
#importEmails()
#importACLRoles()
#importACLActions()
#importRelationships()
#importEmailAddresses()
#importEmailAddrBeanRel()

#importAlterDefaultMysqlTable()







































#==========================================================================================
#importAccountsAudit()
#"INSERT INTO accounts_audit (id,parent_id,date_created,created_by,field_name,data_type,before_value_string,after_value_string,before_value_text,after_value_text,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importAccountsContacts()
#"INSERT INTO accounts_contacts (id,contact_id,account_id,date_modified,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s)"

#importAccountsCSTM()
#"INSERT INTO accounts_cstm (id_c,twitter_c,crm_regn_ctry_name) VALUES (%s,%s,%s)"

#importAccountsOpportunities()
#"INSERT INTO accounts_opportunities (id,opportunity_id,account_id,date_modified,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s)"

#importAccountsActions()
#"INSERT INTO acl_actions (id,date_entered,date_modified,modified_user_id,created_by,name,category,acltype,aclaccess,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importAclRoles()
#"INSERT INTO acl_roles (id,date_entered,date_modified,modified_user_id,created_by,name,description,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importAclRolesActions()
#"INSERT INTO acl_roles_actions (id,role_id,action_id,access_override,date_modified,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s)"

#importAclRolesUsers()
#"INSERT INTO acl_roles_users (id,role_id,user_id,date_modified,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s)"

#importCalls()
#"INSERT INTO calls (id,name,date_entered,date_modified,modified_user_id,created_by,description,deleted,assigned_user_id,duration_hours,duration_minutes,date_start,date_end,parent_type,status,direction,parent_id,reminder_time,email_reminder_time,email_reminder_sent,outlook_id,repeat_type,repeat_interval,repeat_dow,repeat_until,repeat_count,repeat_parent_id,recurring_source,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importCallsContacts()
#"INSERT INTO calls_contacts (id,call_id,contact_id,required,accept_status,date_modified,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"

#importCallsLeads()
#"INSERT INTO calls_leads (id,call_id,lead_id,required,accept_status,date_modified,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"

#importCallsUsers()
#"INSERT INTO calls_users (id,call_id,user_id,required,accept_status,date_modified,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"

#importCases()
#"INSERT INTO cases (id,name,date_entered,date_modified,modified_user_id,created_by,description,deleted,assigned_user_id,case_number,type,status,priority,resolution,work_log,account_id,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importCasesAudit()
#"INSERT INTO cases_audit (id,parent_id,date_created,created_by,field_name,data_type,before_value_string,after_value_string,before_value_text,after_value_text,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importCasesCSTM()
#"INSERT INTO cases_cstm (id_c,business_type_c,channel_type_c,product_type_c,location_c,subject_type_c,hospitals_c,policy_no_c,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importContacts()
#"INSERT INTO contacts (id,date_entered,date_modified,modified_user_id,created_by,description,deleted,assigned_user_id,salutation,first_name,last_name,title,department,do_not_call,phone_home,phone_mobile,phone_work,phone_other,phone_fax,primary_address_street,primary_address_city,primary_address_state,primary_address_postalcode,primary_address_country,alt_address_street,alt_address_city,alt_address_state,alt_address_postalcode,alt_address_country,assistant,assistant_phone,lead_source,reports_to_id,birthdate,campaign_id,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importContactsAudit()
#"INSERT INTO contacts_audit (id,parent_id,date_created,created_by,field_name,data_type,before_value_string,after_value_string,before_value_text,after_value_text,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importContactsCSTM()
#"INSERT INTO contacts_cstm (id_c,twitter_c,crm_regn_ctry_name) VALUES (%s,%s,%s)"

#importContactsUsers()
#"INSERT INTO contacts_users (id,contact_id,user_id,date_modified,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s)"

#importEmailAddrBeanRel()
#"INSERT INTO email_addr_bean_rel (id,email_address_id,bean_id,bean_module,primary_address,reply_to_address,date_created,date_modified,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importEmailAddresses()
#"INSERT INTO email_addresses (id,email_address,email_address_caps,invalid_email,opt_out,date_created,date_modified,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importEmailTemplates()
#"INSERT INTO email_templates (id,date_entered,date_modified,modified_user_id,created_by,published,name,description,subject,body,body_html,deleted,assigned_user_id,text_only,type,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importEmails()
#"INSERT INTO emails (id,date_entered,date_modified,assigned_user_id,modified_user_id,created_by,deleted,date_sent,message_id,name,type,status,flagged,reply_to_status,intent,mailbox_id,parent_type,parent_id,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importEmailsBeans()
#"INSERT INTO emails_beans (id,email_id,bean_id,bean_module,campaign_data,date_modified,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"

#importEmailsEmailAddrRel()
#"INSERT INTO emails_email_addr_rel (id,email_id,address_type,email_address_id,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s)"

#importEmailsText()
#"INSERT INTO emails_text (email_id,from_addr,reply_to_addr,to_addrs,cc_addrs,bcc_addrs,description,description_html,raw_source,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importFieldsMetaData()
#"INSERT INTO fields_meta_data (id,name,vname,comments,help,custom_module,type,len,required,default_value,date_modified,deleted,audited,massupdate,duplicate_merge,reportable,importable,ext1,ext2,ext3,ext4,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importFolders()
#"INSERT INTO folders (id,name,folder_type,parent_folder,has_child,is_group,is_dynamic,dynamic_query,assign_to_id,created_by,modified_by,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importFoldersSubscriptions()
#"INSERT INTO folders_subscriptions (id,folder_id,assigned_user_id,crm_regn_ctry_name) VALUES (%s,%s,%s,%s)"

#importLeads()
#"INSERT INTO leads (id,date_entered,date_modified,modified_user_id,created_by,description,deleted,assigned_user_id,salutation,first_name,last_name,title,department,do_not_call,phone_home,phone_mobile,phone_work,phone_other,phone_fax,primary_address_street,primary_address_city,primary_address_state,primary_address_postalcode,primary_address_country,alt_address_street,alt_address_city,alt_address_state,alt_address_postalcode,alt_address_country,assistant,assistant_phone,converted,refered_by,lead_source,lead_source_description,status,status_description,reports_to_id,account_name,account_description,contact_id,account_id,opportunity_id,opportunity_name,opportunity_amount,campaign_id,birthdate,portal_name,portal_app,website,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importLeadsAudit()
#"INSERT INTO leads_audit (id,parent_id,date_created,created_by,field_name,data_type,before_value_string,after_value_string,before_value_text,after_value_text,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importLeadsCSTM()
#"INSERT INTO leads_cstm (id_c,facebook_userid_c,twitter_c,opportunity_name_drop_c,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s)"

#importMeetings()
#"INSERT INTO meetings (id,name,date_entered,date_modified,modified_user_id,created_by,description,deleted,assigned_user_id,location,password,join_url,host_url,displayed_url,creator,external_id,duration_hours,duration_minutes,date_start,date_end,parent_type,status,type,parent_id,reminder_time,email_reminder_time,email_reminder_sent,outlook_id,sequence,repeat_type,repeat_interval,repeat_dow,repeat_until,repeat_count,repeat_parent_id,recurring_source,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importMeetingsLeads()
#"INSERT INTO meetings_leads (id,meeting_id,lead_id,required,accept_status,date_modified,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"

#importMeetingsUsers()
#"INSERT INTO meetings_users (id,meeting_id,user_id,required,accept_status,date_modified,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"

#importNotes()
#"INSERT INTO notes (assigned_user_id,id,date_entered,date_modified,modified_user_id,created_by,name,file_mime_type,filename,parent_type,parent_id,contact_id,portal_flag,embed_flag,description,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importOpportunities()
#"INSERT INTO opportunities (id,name,date_entered,date_modified,modified_user_id,created_by,description,deleted,assigned_user_id,opportunity_type,campaign_id,lead_source,amount,amount_usdollar,currency_id,date_closed,next_step,sales_stage,probability,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importOpportunitiesAudit()
#"INSERT INTO opportunities_audit (id,parent_id,date_created,created_by,field_name,data_type,before_value_string,after_value_string,before_value_text,after_value_text,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importOpportunitiesContacts()
#"INSERT INTO opportunities_contacts (id,contact_id,opportunity_id,contact_role,date_modified,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s)"

#importOpportunitiesCstm()
#"INSERT INTO opportunities_cstm (id_c,opportunity_name_c,crm_regn_ctry_name) VALUES (%s,%s,%s)"

#importOutboundEmail()
#"INSERT INTO outbound_email (id,name,type,user_id,mail_sendtype,mail_smtptype,mail_smtpserver,mail_smtpport,mail_smtpuser,mail_smtppass,mail_smtpauth_req,mail_smtpssl,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importRelationships()
#"INSERT INTO relationships (id,relationship_name,lhs_module,lhs_table,lhs_key,rhs_module,rhs_table,rhs_key,join_table,join_key_lhs,join_key_rhs,relationship_type,relationship_role_column,relationship_role_column_value,reverse,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importSavedSearch()
#"INSERT INTO saved_search (id,name,search_module,deleted,date_entered,date_modified,assigned_user_id,contents,description,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importSchedulers()
#"INSERT INTO schedulers (id,deleted,date_entered,date_modified,created_by,modified_user_id,name,job,date_time_start,date_time_end,job_interval,time_from,time_to,last_run,status,catch_up,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importSecurityGroups()
#"INSERT INTO securitygroups (id,name,date_entered,date_modified,modified_user_id,created_by,description,deleted,assigned_user_id,noninheritable,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importSecurityGroupsACLRoles()
#"INSERT INTO securitygroups_acl_roles (id,securitygroup_id,role_id,date_modified,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s)"

#importSecurityGroupsRecords()
#"INSERT INTO securitygroups_records (id,securitygroup_id,record_id,module,date_modified,modified_user_id,created_by,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importSecurityGroupsUsers()
#"INSERT INTO securitygroups_users (id,date_modified,deleted,securitygroup_id,user_id,primary_group,noninheritable,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"

#importSugarFeed()
#"INSERT INTO sugarfeed (id,name,date_entered,date_modified,modified_user_id,created_by,description,deleted,assigned_user_id,related_module,related_id,link_url,link_type,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importTasks()
#"INSERT INTO tasks (id,name,date_entered,date_modified,modified_user_id,created_by,description,deleted,assigned_user_id,status,date_due_flag,date_due,date_start_flag,date_start,parent_type,parent_id,contact_id,priority,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importTracker()
#"INSERT INTO tracker (id,monitor_id,user_id,module_name,item_id,item_summary,date_modified,action,session_id,visible,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importUpgradeHistory()
#"INSERT INTO upgrade_history (id,filename,md5sum,type,status,version,name,description,id_name,manifest,date_entered,enabled,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importUserPreferences()
#"INSERT INTO user_preferences (id,category,deleted,date_entered,date_modified,assigned_user_id,contents,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"

#importUsers()a_list
#"INSERT INTO users (id,user_name,user_hash,system_generated_password,pwd_last_changed,authenticate_id,sugar_login,first_name,last_name,is_admin,external_auth_only,receive_notifications,description,date_entered,date_modified,modified_user_id,created_by,title,department,phone_home,phone_mobile,phone_work,phone_other,phone_fax,status,address_street,address_city,address_state,address_country,address_postalcode,deleted,portal_only,show_on_employees,employee_status,messenger_id,messenger_type,reports_to_id,is_group,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importUsersPasswordLink()
#"INSERT INTO users_password_link (id,username,date_generated,deleted,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s)"

#importUsersSignatures()
#"INSERT INTO users_signatures (id,date_entered,date_modified,deleted,user_id,name,signature,signature_html,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"

#importVcals()
#"INSERT INTO vcals (id,deleted,date_entered,date_modified,user_id,type,source,content,crm_regn_ctry_name) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"

def alterTablesAndAddCountryColumn():
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')
    mysqlcur = mysqlconn.cursor()
    mysqlcur.execute("SHOW TABLES")
    for (table_name) in mysqlcur:
        string_table_name = table_name[0]
        alter_mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')
        alter_mysqlcur = mysqlconn.cursor()
        #alter_mysqlcur.execute("ALTER TABLE "+string_table_name+" DROP COLUMN crm_regn_ctry_name")
        alter_mysqlcur.execute("ALTER TABLE "+string_table_name+" ADD COLUMN crm_regn_ctry_name VARCHAR(20)")
        print("table name is "+ string_table_name)
        alter_mysqlcur.connection.commit()

    mysqlcur.close()
    mysqlconn.close()

#alterTablesAndAddCountryColumn()