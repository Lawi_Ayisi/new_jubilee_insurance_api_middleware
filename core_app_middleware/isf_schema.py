import graphene
from graphene import resolve_only_args, relay
from graphene_sqlalchemy import SQLAlchemyConnectionField, SQLAlchemyObjectType
from models.isf_models import Agent as AgentModel, CustomerDetails as CustomerDetailsModel, LifePolicy as PolicyDetailsModel, PremiumStatement as PremiumStatementModel


def connection_for_type(_type):
    class Connection(graphene.Connection):
        total_count = graphene.Int()

        class Meta:
            name = _type._meta.name + 'Connection'
            node = _type

        def resolve_total_count(self, args, context, info):
            return self.length

    return Connection


class CustomerDetails(SQLAlchemyObjectType):

    class Meta:
        model = CustomerDetailsModel
        interfaces = (relay.Node, )


class Agents(SQLAlchemyObjectType):

    class Meta:
        model = AgentModel
        interfaces = (relay.Node, )


class PolicyDetails(SQLAlchemyObjectType):

    class Meta:
        model = PolicyDetailsModel
        interfaces = (relay.Node, )


class PremiumStatements(SQLAlchemyObjectType):

    class Meta:
        model = PremiumStatementModel
        interfaces = (relay.Node, )


class Query(graphene.ObjectType):
    customer_details = SQLAlchemyConnectionField(CustomerDetails,  policy_no=graphene.String())
    customer_detail = relay.Node.Field(CustomerDetails)

    agents = SQLAlchemyConnectionField(Agents,  code=graphene.String())
    agent = relay.Node.Field(Agents)

    policy_details = SQLAlchemyConnectionField(PolicyDetails,  policy_no=graphene.String())
    policy_detail = relay.Node.Field(PolicyDetails)

    premium_statements = SQLAlchemyConnectionField(PremiumStatements,  policy_no=graphene.String())
    premium_statement = relay.Node.Field(PremiumStatements)

    node = relay.Node.Field()
    get_customer_detail = relay.Node.Field(CustomerDetails, policy_no=graphene.String())
    get_policy_detail = relay.Node.Field(PolicyDetails, policy_no=graphene.String())
    get_premium_statement = relay.Node.Field(PremiumStatements, policy_no=graphene.String())

    def resolve_customer_details(self, args, context, info):
        query = CustomerDetails.get_query(context) # SQLAlchemy query
        policy_number = args.get('policy_no')
        if policy_number is None:
            return query.all()
        else:
            return query.filter_by(policy_no=policy_number).all()

    def resolve_policy_details(self, args, context, info):
        query = PolicyDetails.get_query(context) # SQLAlchemy query
        policy_number = args.get('policy_no')
        if policy_number is None:
            return query.all()
        else:
            return query.filter_by(policy_no=policy_number).all()

    def resolve_premium_statements(self, args, context, info):
        query = PremiumStatements.get_query(context)  # SQLAlchemy query
        policy_number = args.get('policy_no')
        if policy_number is None:
            return query.all()
        else:
	    #filters = {'policy_no':policy_number}
            #return query.filter_by(**filters).all()
	    return PremiumStatementModel.query.filter(PremiumStatementModel.policy_no==policy_number).all()
	    #return PremiumStatementModel.query.limit(20).all()

    def resolve_agents(self, args, context, info):
        query = Agents.get_query(context) # SQLAlchemy query
        print(str(args))
        code = args.get('code')
        if code is None:
            return query.distinct(AgentModel.code)
        else:
            return query.filter_by(code=code).distinct(AgentModel.code)


isf_schema = graphene.Schema(query=Query, types=[Agents, CustomerDetails, PolicyDetails, PremiumStatements])
