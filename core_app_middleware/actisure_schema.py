import graphene
from graphene import resolve_only_args, relay
from graphene_sqlalchemy import SQLAlchemyConnectionField, SQLAlchemyObjectType
from models.actisure_models import Agent as AgentModel, MedicalInformation as MedicalInformationModel


def connection_for_type(_type):
    class Connection(graphene.Connection):
        total_count = graphene.Int()

        class Meta:
            name = _type._meta.name + 'Connection'
            node = _type

        def resolve_total_count(self, args, context, info):
            return self.length

    return Connection


class MedicalInformations(SQLAlchemyObjectType):

    class Meta:
        model = MedicalInformationModel
        interfaces = (relay.Node, )


class Agents(SQLAlchemyObjectType):

    class Meta:
        model = AgentModel
        interfaces = (relay.Node, )


class Query(graphene.ObjectType):
    medical_informations = SQLAlchemyConnectionField(MedicalInformations,  member_no=graphene.String())
    medical_information = relay.Node.Field(MedicalInformations)

    agents = SQLAlchemyConnectionField(Agents,  code=graphene.String())
    agent = relay.Node.Field(Agents)

    node = relay.Node.Field()
    get_medical_information = relay.Node.Field(MedicalInformations, member_no=graphene.String())
    get_agent = relay.Node.Field(Agents, code=graphene.String())

    def resolve_medical_informations(self, args, context, info):
        query = MedicalInformations.get_query(context) # SQLAlchemy query
        member_number = args.get('member_no')
        if member_number is None:
            return query.all()
        else:
            return query.filter_by(member_no=member_number).all()

    def resolve_agents(self, args, context, info):
        query = Agents.get_query(context) # SQLAlchemy query
        print(str(args))
        code = args.get('code')
        if code is None:
            return query.distinct(AgentModel.code)
        else:
            return query.filter_by(code=code).distinct(AgentModel.code)

actisure_schema = graphene.Schema(query=Query, types=[Agents, MedicalInformations])
