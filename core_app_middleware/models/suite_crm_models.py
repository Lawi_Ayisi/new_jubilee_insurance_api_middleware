from sqlalchemy import Column, String as VARCHAR, Integer

from database import SuiteCRMBase


class INTEGER(Integer):
    def __init__(self, *args, **kwargs): super(Integer, self).__init__()


class Case(SuiteCRMBase):

    __tablename__ = 'cases'

    id = Column("id", VARCHAR(255), primary_key=True, nullable=False)
    name = Column("name", VARCHAR(255), nullable=True)
    date_entered = Column("date_entered", VARCHAR(255), nullable=True)
    date_modified = Column("date_modified", VARCHAR(255), nullable=True)
    modified_user_id = Column("modified_user_id", VARCHAR(255), nullable=True)
    created_by = Column("created_by", VARCHAR(255), nullable=True)
    description = Column("description", VARCHAR(255), nullable=True)
    assigned_user_id = Column("assigned_user_id", VARCHAR(255), nullable=True)
    case_number = Column("case_number", VARCHAR(255), nullable=True)
    type = Column("type", VARCHAR(255), nullable=True)
    status = Column("status", VARCHAR(255), nullable=True)
    priority = Column("priority", VARCHAR(255), nullable=True)
    resolution = Column("resolution", VARCHAR(255), nullable=True)
    work_log = Column("work_log", VARCHAR(255), nullable=True)
    account_id = Column("account_id", VARCHAR(255), nullable=True)
    state = Column("state", VARCHAR(255), nullable=True)
    contact_created_by_id = Column("contact_created_by_id", VARCHAR(255), nullable=True)
    crm_regn_ctry_name = Column("crm_regn_ctry_name", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<Case(%(case_number)s)>" % self.__dict__


class Account(SuiteCRMBase):

    __tablename__ = 'accounts'

    id = Column("id", VARCHAR(255), primary_key=True, nullable=False)
    name = Column("name", VARCHAR(255), nullable=True)
    national_id = Column("sic_code", VARCHAR(255), nullable=True)
    date_entered = Column("date_entered", VARCHAR(255), nullable=True)
    date_modified = Column("date_modified", VARCHAR(255), nullable=True)
    modified_user_id = Column("modified_user_id", VARCHAR(255), nullable=True)
    created_by = Column("created_by", VARCHAR(255), nullable=True)
    description = Column("description", VARCHAR(255), nullable=True)
    assigned_user_id = Column("assigned_user_id", VARCHAR(255), nullable=True)
    mobile_phone = Column("phone_office", VARCHAR(255), nullable=True)
    phone_alternate = Column("phone_alternate", VARCHAR(255), nullable=True)
    crm_regn_ctry_name = Column("crm_regn_ctry_name", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<Account(%(id)s)>" % self.__dict__


class AccountCSTM(SuiteCRMBase):

    __tablename__ = 'accounts_cstm'

    id = Column("id_c", VARCHAR(255), primary_key=True, nullable=False)
    income_tax_number = Column("income_tax_number_c", VARCHAR(255), nullable=True)
    passport_number = Column("passport_number_c", VARCHAR(255), nullable=True)
    date_of_birth = Column("date_of_birth_c", VARCHAR(255), nullable=True)
    gender = Column("gender_c", VARCHAR(255), nullable=True)
    category = Column("segmentation_list_c", VARCHAR(255), nullable=True)
    no_of_policies = Column("no_of_policies_c", VARCHAR(255), nullable=True)
    total_premium_value = Column("total_premium_value_c", VARCHAR(255), nullable=True)
    core_system_customer = Column("core_system_customer_c", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<AccountCSTM(%(id)s)>" % self.__dict__


class EmailAddress(SuiteCRMBase):

    __tablename__ = 'email_addresses'

    id = Column("id", VARCHAR(255), primary_key=True, nullable=False)
    email_address = Column("email_address", VARCHAR(255), nullable=True)
    email_address_caps = Column("email_address_caps", VARCHAR(255), nullable=True)
    date_created = Column("date_created", VARCHAR(255), nullable=True)
    date_modified = Column("date_modified", VARCHAR(255), nullable=True)
    crm_regn_ctry_name = Column("crm_regn_ctry_name", VARCHAR(255), nullable=True)


    def __repr__(self):return self.__str__()

    def __str__(self):return "<EmailAddress(%(id)s)>" % self.__dict__


class ACLRoles(SuiteCRMBase):

    __tablename__ = 'acl_roles'

    id = Column("id", VARCHAR(255), primary_key=True, nullable=False)
    name = Column("name", VARCHAR(255), nullable=True)

    def __repr__(self): return self.__str__()

    def __str__(self): return "<ACLRoles(%(name)s)>" % self.__dict__


class ACLRolesActions(SuiteCRMBase):
    __tablename__ = 'acl_roles_actions'

    id = Column("id", VARCHAR(255), primary_key=True, nullable=False)
    role_id = Column("role_id", VARCHAR(255), nullable=True)
    action_id = Column("action_id", VARCHAR(255), nullable=True)
    access_override = Column("access_override", VARCHAR(255), nullable=True)
    deleted = Column("deleted", VARCHAR(255), nullable=True)
    date_modified = Column("date_modified", VARCHAR(255), nullable=True)
    crm_regn_ctry_name = Column("crm_regn_ctry_name", VARCHAR(255), nullable=True)

    def __repr__(self): return self.__str__()

    def __str__(self): return "<ASOLCommonConfig(%(id)s)>" % self.__dict__


class ACLActions(SuiteCRMBase):
    __tablename__ = 'acl_actions'

    id = Column("id", VARCHAR(255), primary_key=True, nullable=False)
    name = Column("name", VARCHAR(255), nullable=True)
    category = Column("category", VARCHAR(255), nullable=True)
    deleted = Column("deleted", VARCHAR(255), nullable=True)
    date_modified = Column("date_modified", VARCHAR(255), nullable=True)
    crm_regn_ctry_name = Column("crm_regn_ctry_name", VARCHAR(255), nullable=True)

    def __repr__(self): return self.__str__()

    def __str__(self): return "<ACLActions(%(id)s)>" % self.__dict__


class ASOLCommonConfig(SuiteCRMBase):
    __tablename__ = 'asol_common_config'

    id = Column("id", VARCHAR(255), primary_key=True, nullable=False)
    name = Column("name", VARCHAR(255), nullable=True)
    category = Column("category", VARCHAR(255), nullable=True)
    modified_user_id = Column("modified_user_id", VARCHAR(255), nullable=True)
    created_by = Column("created_by", VARCHAR(255), nullable=True)
    date_entered = Column("date_entered", VARCHAR(255), nullable=True)
    deleted = Column("deleted", VARCHAR(255), nullable=True)
    date_modified = Column("date_modified", VARCHAR(255), nullable=True)
    config = Column("config", VARCHAR(255), nullable=True)

    def __repr__(self): return self.__str__()

    def __str__(self): return "<ASOLCommonConfig(%(id)s)>" % self.__dict__


class User(SuiteCRMBase):

    __tablename__ = 'users'

    id = Column("id", VARCHAR(255), primary_key=True, nullable=False)
    user_name = Column("user_name", VARCHAR(255), nullable=True)
    first_name = Column("first_name", VARCHAR(255), nullable=True)
    last_name = Column("last_name", VARCHAR(255), nullable=True)
    is_admin = Column("is_admin", VARCHAR(255), nullable=True)
    status = Column("status", VARCHAR(255), nullable=True)
    deleted = Column("deleted", VARCHAR(255), nullable=True)
    employee_status = Column("employee_status", VARCHAR(255), nullable=True)
    date_entered = Column("date_entered", VARCHAR(255), nullable=True)
    date_modified = Column("date_modified", VARCHAR(255), nullable=True)
    crm_regn_ctry_name = Column("crm_regn_ctry_name", VARCHAR(255), nullable=True)

    def __repr__(self): return self.__str__()

    def __str__(self): return "<User(%(id)s)>" % self.__dict__


class EmailAddressBeanRel(SuiteCRMBase):

    __tablename__ = 'email_addr_bean_rel'

    id = Column("id", VARCHAR(255), primary_key=True, nullable=False)
    email_address_id = Column("email_address_id", VARCHAR(255), nullable=True)
    bean_id = Column("bean_id", VARCHAR(255), nullable=True)
    bean_module = Column("bean_module", VARCHAR(255), nullable=True)
    primary_address = Column("primary_address", VARCHAR(255), nullable=True)
    crm_regn_ctry_name = Column("crm_regn_ctry_name", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<EmailAddressBeanRel(%(id)s)>" % self.__dict__


class AccountsOpportunities(SuiteCRMBase):

    __tablename__ = 'accounts_opportunities'

    id = Column("id", VARCHAR(255), primary_key=True, nullable=False)
    opportunity_id = Column("opportunity_id", VARCHAR(255), nullable=True)
    account_id = Column("account_id", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<AccountsOpportunities(%(id)s)>" % self.__dict__


class Lead(SuiteCRMBase):

    __tablename__ = 'leads'

    id = Column("id", VARCHAR(255), primary_key=True, nullable=False)
    first_name = Column("first_name", VARCHAR(255), nullable=True)
    last_name = Column("last_name", VARCHAR(255), nullable=True)
    phone_work = Column("phone_work", VARCHAR(255), nullable=True)
    description = Column("description", VARCHAR(255), nullable=True)
    account_id = Column("account_id", VARCHAR(255), nullable=True)
    opportunity_id = Column("opportunity_id", VARCHAR(255), nullable=True)
    opportunity_amount = Column("opportunity_amount", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<Lead(%(id)s)>" % self.__dict__


class Policy(SuiteCRMBase):

    __tablename__ = 'old_polic_policies'

    id = Column("id", VARCHAR(255), primary_key=True, nullable=False)
    policy_number = Column("policy_number", VARCHAR(255), nullable=True)
    #member_number = Column("member_number", VARCHAR(255), nullable=True)
    pol_class = Column("class", VARCHAR(255), nullable=True)
    pol_sub_class = Column("sub_class", VARCHAR(255), nullable=True)
    status = Column("status", VARCHAR(255), nullable=True)
    kyc_id = Column("kyc_id", VARCHAR(255), nullable=True)
    date_of_purchase = Column("date_of_purchase", VARCHAR(255), nullable=True)
    next_due_date = Column("next_due_date", VARCHAR(255), nullable=True)
    renewal_date = Column("renewal_date", VARCHAR(255), nullable=True)
    maturity_date = Column("maturity_date", VARCHAR(255), nullable=True)
    benefit_due_date = Column("benefit_due_date", VARCHAR(255), nullable=True)
    last_updated = Column("last_updated", VARCHAR(255), nullable=True)
    start_date = Column("start_date", VARCHAR(255), nullable=True)
    sum_assured = Column("sum_assured", VARCHAR(255), nullable=True)
    surrender_value = Column("surrender_value", VARCHAR(255), nullable=True)
    term_in_years = Column("term_in_years", VARCHAR(255), nullable=True)
    type = Column("type", VARCHAR(255), nullable=True)
    premium_amount = Column("premium_amount", VARCHAR(255), nullable=True)
    premium_frequency = Column("premium_frequency", VARCHAR(255), nullable=True)
    line_of_business = Column("line_of_business", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<Policy(%(id)s)>" % self.__dict__


class PolicyAccount(SuiteCRMBase):

    __tablename__ = 'polic_policies_accounts_c'

    id = Column("id", VARCHAR(255), primary_key=True, autoincrement=True)
    account_id = Column("polic_policies_accountsaccounts_ida", VARCHAR(255), nullable=False)
    policy_id = Column("polic_policies_accountspolic_policies_idb", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<PolicyAccount(%(account_id)s)>" % self.__dict__


class PaymentAccount(SuiteCRMBase):

    __tablename__ = 'payme_payments_accounts_c'

    id = Column("id", VARCHAR(255), primary_key=True, autoincrement=True)
    account_id = Column("payme_payments_accountsaccounts_ida", VARCHAR(255), nullable=False)
    payment_id = Column("payme_payments_accountspayme_payments_idb", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<PaymentAccount(%(account_id)s)>" % self.__dict__


class CaseCSTM(SuiteCRMBase):

    __tablename__ = 'cases_cstm'

    id = Column("id_c", VARCHAR(255), primary_key=True, autoincrement=True)
    business_type = Column("business_type_c", VARCHAR(255), nullable=False)
    policy_no = Column("policy_no_c", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<CaseCSTM(%(id)s)>" % self.__dict__


class TempPayments(SuiteCRMBase):

    __tablename__ = 'temp_payments'

    policy_number = Column("V_POLICY_NO", VARCHAR(255), primary_key=True)
    receipt_number = Column("V_RECEIPT_NO", VARCHAR(255), nullable=False)
    receipt_date = Column("D_RECEIPT_DATE", VARCHAR(255), nullable=False)
    amount = Column("N_RECEIPT_AMT", VARCHAR(255), nullable=False)
    ilanga_receipt_no = Column("V_OTHER_REF_NO", VARCHAR(255), nullable=False)
    ilanga_receipt_date = Column("D_OTHER_REF_DATE", VARCHAR(255), nullable=False)
    status = Column("V_RECEIPT_STATUS", VARCHAR(255), nullable=False)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<TempPayments(%(id)s)>" % self.__dict__


class Payments(SuiteCRMBase):

    __tablename__ = 'payme_payments'

    id = Column("id", VARCHAR(255), primary_key=True)
    deleted = Column("deleted", VARCHAR(255), nullable=False)
    status = Column("status", VARCHAR(255), nullable=False)
    date_of_last_payment = Column("date_of_last_payment", VARCHAR(255), nullable=False)
    mode_of_payment = Column("mode_of_payment", VARCHAR(255), nullable=False)
    next_pay_due_date = Column("next_pay_due_date", VARCHAR(255), nullable=False)
    last_updated = Column("last_updated", VARCHAR(255), nullable=False)
    receipt_number = Column("receipt_number", VARCHAR(255), nullable=False)
    receipt_date = Column("receipt_date", VARCHAR(255), nullable=False)
    ilanga_receipt_no = Column("ilanga_receipt_no", VARCHAR(255), nullable=False)
    ilanga_receipt_date = Column("ilanga_receipt_date", VARCHAR(255), nullable=False)
    amount = Column("amount", VARCHAR(255), nullable=False)
    policy_number = Column("policy_number", VARCHAR(255), nullable=False)
    date_of_payment = Column("date_of_payment", VARCHAR(255), nullable=False)
    kyc_id = Column("kyc_id", VARCHAR(255), nullable=False)
    line_of_business = Column("line_of_business", VARCHAR(255), nullable=False)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<Payments(%(id)s)>" % self.__dict__


class Communications(SuiteCRMBase):

    __tablename__ = 'comms_communications'

    id = Column("id", VARCHAR(255), primary_key=True)
    deleted = Column("deleted", VARCHAR(255), nullable=False)
    mean_of_communication = Column("mean_of_communication", VARCHAR(255), nullable=False)
    date_of_communication = Column("date_of_communication", VARCHAR(255), nullable=False)
    agent_serving_customer = Column("agent_serving_customer", VARCHAR(255), nullable=False)
    content_of_communication = Column("content_of_communication", VARCHAR(255), nullable=False)
    last_updated = Column("last_updated", VARCHAR(255), nullable=False)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<Communications(%(id)s)>" % self.__dict__


class CommunicationsAccount(SuiteCRMBase):

    __tablename__ = 'comms_communications_accounts_c'

    id = Column("id", VARCHAR(255), primary_key=True, autoincrement=True)
    account_id = Column("comms_communications_accountsaccounts_ida", VARCHAR(255), nullable=False)
    communication_id = Column("comms_communications_accountscomms_communications_idb", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<CommunicationsAccount(%(id)s)>" % self.__dict__


class PolicyCSTM(SuiteCRMBase):

    __tablename__ = 'polic_policies_cstm'

    id = Column("id_c", VARCHAR(255), primary_key=True, autoincrement=True)
    premium_amount = Column("premium_amount_c", VARCHAR(255), nullable=False)
    premium_frequency = Column("premium_frequency_c", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<PolicyCSTM(%(id)s)>" % self.__dict__
