from sqlalchemy import Column, String as VARCHAR, Integer

from database import JubileeInsureBase


class INTEGER(Integer):
    def __init__(self, *args, **kwargs): super(Integer, self).__init__()


class Product(JubileeInsureBase):

    __tablename__ = 'bf_products'

    id = Column("product_id", VARCHAR(255), primary_key=True, nullable=False)
    name = Column("product_name", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<Product(%(name)s)>" % self.__dict__


class Customer(JubileeInsureBase):

    __tablename__ = 'bf_customers'

    id = Column("cust_id", VARCHAR(255), primary_key=True, nullable=False)
    title = Column("title", VARCHAR(255), nullable=True)
    name = Column("surname", VARCHAR(255), nullable=True)
    gender = Column("gender", VARCHAR(255), nullable=True)
    date_of_birth = Column("dob", VARCHAR(255), nullable=True)
    marital_status = Column("marital_status", VARCHAR(255), nullable=True)
    id_no = Column("id_no", VARCHAR(255), nullable=True)
    passport_no = Column("passport_no", VARCHAR(255), nullable=True)
    income_tax_number = Column("kra_pin", VARCHAR(255), nullable=True)
    email_addr = Column("email_addr", VARCHAR(255), nullable=True)
    phone_number = Column("tel_no", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<Customer(%(name)s)>" % self.__dict__
