from sqlalchemy import Column, String as VARCHAR, Integer

from database import ISFUATBase


class INTEGER(Integer):
    def __init__(self, *args, **kwargs): super(Integer, self).__init__()


class PremiumStatement(ISFUATBase):

    __tablename__ = 'JHL_PREMIUM_STATEMENT'

    receipt_no = Column("RECEIPT_NO", VARCHAR(255), primary_key=True, nullable=False)
    policy_no = Column("POLICY_NO", VARCHAR(255), nullable=False)
    date = Column("RECEIPT_DATE", VARCHAR(255), nullable=True)
    type = Column("RECEIPT_TYPE", VARCHAR(255), nullable=True)
    amount = Column("AMOUNT", VARCHAR(255), nullable=True)
    policy_type = Column("POLICY_TYPE", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<PremiumStatement(%(receipt_no)s)>" % self.__dict__


class REMTReceipt(ISFUATBase):

    __tablename__ = 'REMT_RECEIPT'

    policy_no = Column("V_POLICY_NO", VARCHAR(255), primary_key=True, nullable=False)
    receipt_no = Column("V_RECEIPT_NO", VARCHAR(255), primary_key=True, nullable=False)
    amount = Column("N_RECEIPT_AMT", VARCHAR(255), nullable=True)
    remarks = Column("V_RECEIPT_REMARKS", VARCHAR(255), nullable=True)
    date = Column("D_RECEIPT_DATE", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<REMTReceipt(%(receipt_no)s)>" % self.__dict__


class CustomerDetails(ISFUATBase):

    __tablename__ = 'LIFE_POLICIES'

    customer_id = Column("N_CUST_REF_NO", VARCHAR(255), primary_key=True, nullable=False)
    customer_name = Column("CUST_NAME", VARCHAR(255), primary_key=True, nullable=False)
    policy_no = Column("POLICY_NO", VARCHAR(255), nullable=True)
    email = Column("EMAIL", VARCHAR(255), nullable=True)
    phone_number = Column("PHONE", VARCHAR(255), nullable=True)
    kra_pin = Column("KRA_PIN", VARCHAR(255), nullable=True)
    date_of_birth = Column("D_BIRTH_DATE", VARCHAR(255), nullable=True)
    national_id = Column("NIC", VARCHAR(255), nullable=True)
    agent_no = Column("N_AGENT_NO", VARCHAR(255), nullable=True)
    agent_code = Column("V_AGENT_CODE", VARCHAR(255), nullable=True)
    agent_name = Column("AGENT_NAME", VARCHAR(255), nullable=True)
    gender = Column("V_SEX", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<CustomerDetail(%(customer_id)s)>" % self.__dict__


class LifePolicy(CustomerDetails):

    """The main table relating to a client with its principal address."""

    customer_id_isf = CustomerDetails.customer_id
    email_address_isf = CustomerDetails.email
    phone_no_isf = CustomerDetails.phone_number
    income_tax_no = CustomerDetails.kra_pin
    date_of_birth = CustomerDetails.date_of_birth
    national_id_no = CustomerDetails.national_id
    policy_no = CustomerDetails.policy_no
    agent_no = CustomerDetails.agent_no
    agent_code = CustomerDetails.agent_code
    agent_name = CustomerDetails.agent_name

    policy_type = Column("POLICY_TYPE", VARCHAR(255), nullable=True)
    start_date = Column("START_DATE", VARCHAR(255), nullable=True)
    issue_date = Column("ISSUE_DATE", VARCHAR(255), nullable=True)
    premium_due_date = Column("PREM_DUE_DATE", VARCHAR(255), nullable=True)
    maturity_date = Column("MATURITY_DATE", VARCHAR(255), nullable=True)
    premium_amount = Column("PREMIUM_AMT", VARCHAR(255), nullable=True)
    sum_assured = Column("SUM_ASSURED", VARCHAR(255), nullable=True)
    bonus = Column("BONUS", VARCHAR(255), nullable=True)
    premium_frequency = Column("PREM_FREQUENCY", VARCHAR(255), nullable=True)
    total_premium_paid = Column("TOTAL_PREM_PAID", VARCHAR(255), nullable=True)
    surrender_value = Column("SURRENDER_VALUE", VARCHAR(255), nullable=True)
    status = Column("POLICY_STATUS", VARCHAR(255), nullable=True)
    passport_no = Column("PASSPORT_NO", VARCHAR(255), nullable=True)
    religion = Column("V_RELIGION_DESC", VARCHAR(255), nullable=True)
    term_in_years = Column("TERM", VARCHAR(255), nullable=True)
    next_out_date = Column("D_NEXT_OUT_DATE", VARCHAR(255), nullable=True)
    loan_limit = Column("LOAN_LIMIT", VARCHAR(255), nullable=True)
    loan_eligible = Column("LOAN_ELIGIBLE", VARCHAR(255), nullable=True)
    os_premium = Column("OS_PREMIUM", VARCHAR(255), nullable=True)


    def __init__(self, customer_id_isf):
        self.source_application = "ISF"
        self.line_of_business = "LIFE"
        self.country = "KENYA"
        self.customer_kyc_id = None
        self.customer_id_apex = None
        self.customer_id_actisure = None
        self.customer_id_premia = None
        self.customer_id_ofa = None
        self.customer_id_jubilee_insure = None
        self.email_address_premia = None
        self.phone_no_premia = None
        self.email_address_actisure = None
        self.phone_no_actisure = None
        self.email_address_apex = None
        self.phone_no_apex = None
        self.email_address_jubilee_insure = None
        self.phone_no_jubilee_insure = None
        self.email_address_ofa = None
        self.phone_no_jubilee_ofa = None

    def __repr__(self): return self.__str__()

    def __str__(self): return "<LifePolicy(%(customer_id)s)>" % self.__dict__


class CustomerContact(ISFUATBase):

    """The main table relating to a client with its principal address."""

    '''
    [
        {
            "N_CUST_REF_NO": 3549200,
            "V_CONTACT_CODE": "CONT010",
            "V_CONTACT_NUMBER": "0711340128",
            "V_STATUS": "A",
            "V_LASTUPD_USER": "134",
            "V_LASTUPD_PROG": "PENTABANC",
            "V_LASTUPD_INFTIM": "2017-02-28 00:00:00"
        }


        [
  {
    "N_CUST_REF_NO": 3549200,
    "V_CONTACT_CODE": "CONT001",
    "V_CONTACT_NUMBER": "karani.erick@gmail.com",
    "V_STATUS": "A",
    "V_LASTUPD_USER": "134",
    "V_LASTUPD_PROG": "PENTABANC",
    "V_LASTUPD_INFTIM": "2017-02-28 00:00:00"
  }
]

    ]
    '''

    __tablename__ = 'GNDT_CUSTMOBILE_CONTACTS'

    customer_reference_no = Column("N_CUST_REF_NO", VARCHAR(255), primary_key=True, nullable=False)
    #This is what determines whether its an email or just a phone number
    contact_code = Column("V_CONTACT_CODE", VARCHAR(255), nullable=True)
    contact_no = Column("V_CONTACT_NUMBER", VARCHAR(255), nullable=True)
    status = Column("V_STATUS", VARCHAR(255), nullable=True)
    last_updated_user = Column("V_LASTUPD_USER", VARCHAR(255), nullable=True)
    last_updated_prog = Column("V_LASTUPD_PROG", VARCHAR(255), nullable=True)
    last_updated_date = Column("V_LASTUPD_INFTIM", VARCHAR(255), nullable=True)

    def __repr__(self): return self.__str__()

    def __str__(self): return "<CustomerContact(%(id)s)>" % self.__dict__


class BulkSMS(ISFUATBase):

    __tablename__ = 'JHL_BULK_SMS'

    id = Column("N_UNIQUE_ID", VARCHAR(255), primary_key=True, nullable=False)
    policy_no = Column("V_POLICY_NO", VARCHAR(255), nullable=True)
    text_content = Column("V_SMS_TEXT", VARCHAR(255), nullable=True)
    phone_number = Column("V_TEL_NO", VARCHAR(255), nullable=True)
    delivery_timestamp = Column("D_ENTRY", VARCHAR(255), nullable=True)

    def __repr__(self): return self.__str__()

    def __str__(self): return "<BulkSMS(%(id)s)>" % self.__dict__


class Agent(ISFUATBase):

    """The main table relating to a client with its principal address."""

    __tablename__ = 'V_AGENT_MASTER'

    code = Column("V_AGENT_CODE", VARCHAR(255), primary_key=True, nullable=False)
    name = Column("V_AGENT_NAME", VARCHAR(255), nullable=True)

    def __init__(self, policy_no):
        self.code = None
        self.name = None

    def __repr__(self): return self.__str__()

    def __str__(self): return "<Agent(%(name)s)>" % self.__dict__
