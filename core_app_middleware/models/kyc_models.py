from sqlalchemy import Column, String as VARCHAR, Integer

from database import KYCBase


class INTEGER(Integer):
    def __init__(self, *args, **kwargs): super(Integer, self).__init__()


class Customer(KYCBase):

    __tablename__ = 'customers'

    id = Column("id", VARCHAR(255), primary_key=True, nullable=False)
    kyc_id = Column("kyc_id", VARCHAR(255), nullable=True)
    actisure_id = Column("actisure_id", VARCHAR(255), nullable=True)
    apex_id = Column("apex_id", VARCHAR(255), nullable=True)
    isf_id = Column("isf_id", VARCHAR(255), nullable=True)
    jubilee_insure_id = Column("jubilee_insure_id", VARCHAR(255), nullable=True)
    ofa_id = Column("ofa_id", VARCHAR(255), nullable=True)
    premia_id = Column("premia_id", VARCHAR(255), nullable=True)
    actisure_email = Column("actisure_email", VARCHAR(255), nullable=True)
    apex_email = Column("apex_email", VARCHAR(255), nullable=True)
    isf_email = Column("isf_email", VARCHAR(255), nullable=True)
    jubilee_insure_email = Column("jubilee_insure_email", VARCHAR(255), nullable=True)
    ofa_email = Column("ofa_email", VARCHAR(255), nullable=True)
    premia_email = Column("premia_email", VARCHAR(255), nullable=True)
    income_tax_number = Column("income_tax_number", VARCHAR(255), nullable=True)
    national_id_no = Column("national_id_no", VARCHAR(255), nullable=True)
    passport_no = Column("passport_no", VARCHAR(255), nullable=True)
    actisure_phone_no = Column("actisure_phone_no", VARCHAR(255), nullable=True)
    apex_phone_no = Column("apex_phone_no", VARCHAR(255), nullable=True)
    isf_phone_no = Column("isf_phone_no", VARCHAR(255), nullable=True)
    jubilee_insure_phone_no = Column("jubilee_insure_phone_no", VARCHAR(255), nullable=True)
    ofa_phone_no = Column("ofa_phone_no", VARCHAR(255), nullable=True)
    premia_phone_no = Column("premia_phone_no", VARCHAR(255), nullable=True)
    premia_cust_code = Column("premia_cust_code", VARCHAR(255), nullable=True)
    created_at = Column("created_at", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<Customer(%(kyc_id)s)>" % self.__dict__


class CustomerDetails(KYCBase):

    __tablename__ = 'customer_details'

    id = Column("id", VARCHAR(255), primary_key=True, nullable=False)
    name = Column("name", VARCHAR(255), nullable=True)
    date_of_birth = Column("date_of_birth", VARCHAR(255), nullable=True)
    gender = Column("gender", VARCHAR(255), nullable=True)

    def __init__(self, id, name, date_of_birth=None, gender=None):
        self.id = id
        self.name = name
        self.date_of_birth = date_of_birth
        self.gender = gender

    def __repr__(self):return self.__str__()

    def __str__(self):return "<CustomerDetails(%(name)s)>" % self.__dict__


class Policy(KYCBase):

    __tablename__ = 'policies'

    id = Column("id", VARCHAR(255), primary_key=True, autoincrement=True)
    kyc_id = Column("kyc_id", VARCHAR(255), nullable=False)
    policy_number = Column("pol_number", VARCHAR(255), nullable=True)
    product = Column("product", VARCHAR(255), nullable=True)
    pol_class = Column("class", VARCHAR(255), nullable=True)
    pol_sub_class = Column("sub_class", VARCHAR(255), nullable=True)
    date_of_purchase = Column("date_of_purchase", VARCHAR(255), nullable=True)
    status = Column("status", VARCHAR(255), nullable=True)
    start_date = Column("start_date", VARCHAR(255), nullable=True)
    sum_assured = Column("sum_assured", VARCHAR(255), nullable=True)
    surrender_value = Column("surrender_value", VARCHAR(255), nullable=True)
    term_in_years = Column("term_in_years", VARCHAR(255), nullable=True)
    type = Column("type", VARCHAR(255), nullable=True)
    renewal_date = Column("renewal_date", VARCHAR(255), nullable=True)
    maturity_date = Column("maturity_date", VARCHAR(255), nullable=True)
    benefit_due_date = Column("benefit_due_date", VARCHAR(255), nullable=True)
    line_of_business = Column("line_of_business", VARCHAR(255), nullable=True)
    premium_amount = Column("premium_amount", VARCHAR(255), nullable=True)
    premium_frequency = Column("premium_frequency", VARCHAR(255), nullable=True)
    last_updated = Column("last_updated", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<KYCPolicy(%(policy_number)s)>" % self.__dict__


class PaymentDetails(KYCBase):

    __tablename__ = 'payment_details'

    id = Column("id", VARCHAR(255), primary_key=True, autoincrement=True)
    kyc_id = Column("kyc_id", VARCHAR(255), nullable=False)
    receipt_number = Column("receipt_number", VARCHAR(255), nullable=True)
    amount = Column("amount", VARCHAR(255), nullable=True)
    status = Column("status", VARCHAR(255), nullable=True)
    channel_of_payment = Column("channel_of_payment", VARCHAR(255), nullable=True)
    policy_number = Column("policy_number", VARCHAR(255), nullable=True)
    date_of_payment = Column("date_of_payment", VARCHAR(255), nullable=True)
    date_of_last_payment = Column("date_of_last_payment", VARCHAR(255), nullable=True)
    next_pay_due_date = Column("next_pay_due_date", VARCHAR(255), nullable=True)
    line_of_business = Column("line_of_business", VARCHAR(255), nullable=True)
    last_updated = Column("last_updated", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<PaymentDetails(%(receipt_number)s)>" % self.__dict__
