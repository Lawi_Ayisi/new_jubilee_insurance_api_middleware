from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.engine import url

# isf_engine = create_engine("oracle://JHLISFADMIN:ISFJHL321@192.168.50.8:1521/JHLKPROD.jubileekenya.com", coerce_to_decimal=False)
# isf_db_session = scoped_session(sessionmaker(autocommit=False,autoflush=False,bind=isf_engine))
# ISFBase = declarative_base()
# ISFBase.query = isf_db_session.query_property()

isf_uat_engine = create_engine("oracle://JHLISFRPT:JHLISFRPT@192.168.50.8:1521/JHLKPROD.jubileekenya.com", coerce_to_decimal=False)
#isf_uat_engine = create_engine("oracle://JHLISFADMIN:JHL1SF999@192.168.52.15:1521/ISFUPGKET", coerce_to_decimal=False)
isf_uat_db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=isf_uat_engine))
ISFUATBase = declarative_base()
ISFUATBase.query = isf_uat_db_session.query_property()


# connect_url = url.URL('oracle+cx_oracle', username='PAY_COMMUNICATION_USR', password='d5tlaw1Pay17', host='92.168.52.40', port='1521', query=dict(service_name='premkeuat'))
#
# premia_engine = create_engine(connect_url, coerce_to_decimal=False)  premkeuat JICKTST
#premia_engine = create_engine("oracle://PAY_COMMUNICATION_USR:d5tlaw1Pay17@keprpredbt.jubileekenya.com:1521/JICKTST", coerce_to_decimal=False)
premia_engine = create_engine("oracle://digital_apps:digital_apps@192.168.50.39:1521/PREMIADB", coerce_to_decimal=False)
premia_db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False,bind=premia_engine))
PremiaBase = declarative_base()
PremiaBase.query = premia_db_session.query_property()

actisure_engine = create_engine("oracle://jubexp:jubexp123@192.168.52.25:1521/ACTTESTDB", coerce_to_decimal=False)
actisure_db_session = scoped_session(sessionmaker(autocommit=False,autoflush=False,bind=actisure_engine))
ACTISUREBase = declarative_base()
ACTISUREBase.query = actisure_db_session.query_property()

actisure_production_engine = create_engine("oracle://digital_apps:digital_apps@192.168.0.193:1521/ACTISURE1", coerce_to_decimal=False)
actisure_production_db_session = scoped_session(sessionmaker(autocommit=False,autoflush=False,bind=actisure_production_engine))
ACTISUREPRODBase = declarative_base()
ACTISUREPRODBase.query = actisure_production_db_session.query_property()

#apex_engine = create_engine("oracle://C##ONSULTIT:CONSULTIT@192.168.52.13:1521/JICDAT", coerce_to_decimal=False)
apex_engine = create_engine("oracle://digital_apps:digital_apps@192.168.50.13:1521/pensions", coerce_to_decimal=False)
apex_db_session = scoped_session(sessionmaker(autocommit=False,autoflush=False,bind=apex_engine))
APEXBase = declarative_base()
APEXBase.query = apex_db_session.query_property()

online_bima_engine = create_engine("oracle://ONLBIMA:BIMA321@192.168.50.8:1521/JHLKPROD.jubileekenya.com", coerce_to_decimal=False)
online_bima_session = scoped_session(sessionmaker(autocommit=False,autoflush=False,bind=online_bima_engine))
OnlineBimaBase = declarative_base()
OnlineBimaBase.query = online_bima_session.query_property()

jhl_online_engine = create_engine("oracle://JHLONLINE:JHLONLINE2@192.168.50.8:1521/JHLKPROD.jubileekenya.com", coerce_to_decimal=False)
jhl_online_session = scoped_session(sessionmaker(autocommit=False,autoflush=False,bind=jhl_online_engine))
JHLOnlineBase = declarative_base()
JHLOnlineBase.query = jhl_online_session.query_property()

suite_crm_engine = create_engine(
    'mysql://wasambo:hGGmr<7Ekdez@192.168.50.26:3306/jubilee_suitecrm_db',
    echo=False,
    pool_size=100,
    pool_recycle=280)
suite_crm_db_session = scoped_session(sessionmaker(autocommit=False,autoflush=False,bind=suite_crm_engine))
SuiteCRMBase = declarative_base()
SuiteCRMBase.query = suite_crm_db_session.query_property()


kyc_engine = create_engine('postgresql://admin:admin@192.168.50.27:5432/kyc_webservice_db', echo=False)
kyc_db_session = scoped_session(sessionmaker(autocommit=False,autoflush=False,bind=kyc_engine))
KYCBase = declarative_base()
KYCBase.query = kyc_db_session.query_property()


actisure_fail_over_engine = create_engine('postgresql://root:Root123!@192.168.52.47:5433/actisure_fail_over', echo=False)
actisure_fail_over_db_session = scoped_session(sessionmaker(autocommit=False,autoflush=False,bind=actisure_fail_over_engine))
ACTISUREFailOverBase = declarative_base()
ACTISUREFailOverBase.query = actisure_fail_over_db_session.query_property()


jubilee_insure_staging_engine = create_engine('mysql://root:!23qweASD@192.168.52.48:3306/jubilee_insure_staging', echo=False)
jubilee_insure_staging_session = scoped_session(sessionmaker(autocommit=False,autoflush=False,bind=jubilee_insure_staging_engine))
JubileeInsureBase = declarative_base()
JubileeInsureBase.query = jubilee_insure_staging_session.query_property()


ilanga_engine = create_engine('mysql://erick:Jubilee123@192.168.0.195:3306/IFMSFinacials', echo=False)
ilanga_session = scoped_session(sessionmaker(autocommit=False,autoflush=False,bind=ilanga_engine))
IlangaBase = declarative_base()
IlangaBase.query = ilanga_session.query_property()
