from sqlalchemy import Column, String as VARCHAR, Integer

from database import ACTISUREBase


class INTEGER(Integer):
    def __init__(self, *args, **kwargs): super(Integer, self).__init__()


class MedicalPolicy(ACTISUREBase):

    """The main table relating to a client with its principal address."""

    __tablename__ = 'CUSTOMERDETAILS'

    customer_id_actisure = Column("ENTITYID", VARCHAR(255), primary_key=True, nullable=False)
    customer_name = Column("BENEFICIARYNAME", VARCHAR(255), nullable=True)
    policy_no = Column("POLICYNUMBER", VARCHAR(255), nullable=True)
    policy_status = Column("POLICYSTATUS", VARCHAR(255), nullable=True)
    #policy_amount = Column("POLICYAMOUNT", VARCHAR(255), nullable=True)
    policy_effective_date = Column("EFFECTIVEDATE", VARCHAR(255), nullable=True)
    policy_end_date = Column("ENDDATE", VARCHAR(255), nullable=True)
    policy_product = Column("PRODUCT", VARCHAR(255), nullable=True)
    email_address_actisure = Column("EMAIL", VARCHAR(255), nullable=True)
    phone_no_actisure = Column("PHONENUMBER", VARCHAR(25), nullable=True)
    #income_tax_no = Column("PIN", VARCHAR(255), nullable=True)
    date_of_birth = Column("DOB", VARCHAR(255), nullable=True)
    national_id_no = Column("IDNO", VARCHAR(255), nullable=True)
    agent_code = Column("AGENTCODE", VARCHAR(25), nullable=True)
    agent_name = Column("AGENTNAME", VARCHAR(255), nullable=True)
    gender = Column("GENDER", VARCHAR(255), nullable=True)

    def __init__(self, customer_id_actisure):
        self.source_application = "ACTISURE"
        self.line_of_business = "MEDICAL"
        self.country = "KENYA"
        self.customer_kyc_id = None
        self.customer_id_apex = None
        self.customer_id_isf = None
        self.customer_id_premia = None
        self.customer_id_ofa = None
        self.customer_id_jubilee_insure = None
        self.email_address_premia = None
        self.phone_no_premia = None
        self.email_address_isf = None
        self.phone_no_actisure = None
        self.email_address_apex = None
        self.phone_no_apex = None
        self.email_address_jubilee_insure = None
        self.phone_no_jubilee_insure = None
        self.email_address_ofa = None
        self.phone_no_jubilee_ofa = None

    def __repr__(self): return self.__str__()

    def __str__(self): return "<MedicalPolicy(%(customer_id)s)>" % self.__dict__


class JCAREMedicalPolicy(ACTISUREBase):

    """The main table relating to a client with its principal address."""

    __tablename__ = 'CUSTOMERDETAILS_JCARE'

    customer_id_actisure = Column("ENTITYID", VARCHAR(255), primary_key=True, nullable=False)
    customer_name = Column("BENEFICIARYNAME", VARCHAR(255), nullable=True)
    policy_no = Column("POLICYNUMBER", VARCHAR(255), nullable=True)
    policy_status = Column("POLICYSTATUS", VARCHAR(255), nullable=True)
    #policy_amount = Column("POLICYAMOUNT", VARCHAR(255), nullable=True)
    policy_effective_date = Column("EFFECTIVEDATE", VARCHAR(255), nullable=True)
    policy_end_date = Column("ENDDATE", VARCHAR(255), nullable=True)
    policy_product = Column("PRODUCT", VARCHAR(255), nullable=True)
    email_address_actisure = Column("EMAIL", VARCHAR(255), nullable=True)
    phone_no_actisure = Column("PHONENUMBER", VARCHAR(25), nullable=True)
    #income_tax_no = Column("PIN", VARCHAR(255), nullable=True)
    date_of_birth = Column("DOB", VARCHAR(255), nullable=True)
    national_id_no = Column("IDNO", VARCHAR(255), nullable=True)
    agent_code = Column("AGENTCODE", VARCHAR(25), nullable=True)
    agent_name = Column("AGENTNAME", VARCHAR(255), nullable=True)
    gender = Column("GENDER", VARCHAR(255), nullable=True)

    def __init__(self, customer_id_actisure):
        self.source_application = "ACTISURE"
        self.line_of_business = "MEDICAL"
        self.country = "KENYA"
        self.customer_kyc_id = None
        self.customer_id_apex = None
        self.customer_id_isf = None
        self.customer_id_premia = None
        self.customer_id_ofa = None
        self.customer_id_jubilee_insure = None
        self.email_address_premia = None
        self.phone_no_premia = None
        self.email_address_isf = None
        self.phone_no_actisure = None
        self.email_address_apex = None
        self.phone_no_apex = None
        self.email_address_jubilee_insure = None
        self.phone_no_jubilee_insure = None
        self.email_address_ofa = None
        self.phone_no_jubilee_ofa = None

    def __repr__(self): return self.__str__()

    def __str__(self): return "<MedicalPolicy(%(customer_id)s)>" % self.__dict__


class MedicalInformation(ACTISUREBase):

    """The main table relating to a client with its principal address."""

    __tablename__ = 'MEMBERINFO'

    #policy_no = Column("POLICYNUMBER", VARCHAR(255), primary_key=True, nullable=False)
    member_no = Column("ENTITYID", VARCHAR(255), primary_key=True, nullable=True)
    policy_status = Column("POLICYSTATUS", VARCHAR(255), nullable=True)
    policy_start_date = Column("POLICYSTARTDATE", VARCHAR(255), nullable=True)
    policy_class = Column("PRODUCT", VARCHAR(255), nullable=True)
    renewal_date = Column("RENEWALDATE", VARCHAR(255), nullable=True)
    cover_limit = Column("BENEFITLIMIT", VARCHAR(255), nullable=True)
    scope_of_benefit = Column("INVOICEBENEFIT", VARCHAR(255), nullable=True)
    #utilized_amount = Column("UTILIZEDAMOUNT", VARCHAR(255), nullable=True)

    def __init__(self, member_no):
        #self.policy_no = None
        self.member_no = None
        self.policy_status = None
        self.policy_start_date = None
        self.policy_class = None
        self.renewal_date = None
        self.scope_of_benefit = None
        self.cover_limit = None

    def __repr__(self): return self.__str__()

    def __str__(self): return "<MedicalInformation(%(member_no)s)>" % self.__dict__


class Agent(ACTISUREBase):

    """The main table relating to a client with its principal address."""

    __tablename__ = 'AGENTDETAILS'

    code = Column("AGENTCODE", VARCHAR(255), primary_key=True, nullable=False)
    name = Column("AGENTNAME", VARCHAR(255), nullable=True)
    type = Column("RELATIONSHIPTYPE", VARCHAR(255), nullable=True)
    phone_number = Column("PHONENUMBER", VARCHAR(255), nullable=True)

    def __init__(self, policy_no):
        self.code = None
        self.name = None
        self.type = None
        self.phone_number = None

    def __repr__(self): return self.__str__()

    def __str__(self): return "<Agent(%(name)s)>" % self.__dict__

