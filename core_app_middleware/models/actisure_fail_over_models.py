from sqlalchemy import Column, String as VARCHAR, Integer

from database import ACTISUREFailOverBase


class INTEGER(Integer):
    def __init__(self, *args, **kwargs): super(Integer, self).__init__()

class CustomerDetails(ACTISUREFailOverBase):
    """The main table relating to a client with its principal address."""

    __tablename__ = 'production_customerdetails'

    id = Column("id", VARCHAR(255), primary_key=True, nullable=False)
    customer_id_actisure = Column("entityid", VARCHAR(255), nullable=False)
    customer_name = Column("beneficiaryname", VARCHAR(255), nullable=True)
    policy_no = Column("policynumber", VARCHAR(255), nullable=True)
    policy_status = Column("policystatus", VARCHAR(255), nullable=True)
    policy_effective_date = Column("effectivedate", VARCHAR(255), nullable=True)
    policy_end_date = Column("enddate", VARCHAR(255), nullable=True)
    policy_product = Column("product", VARCHAR(255), nullable=True)
    email_address_actisure = Column("email", VARCHAR(255), nullable=True)
    phone_no_actisure = Column("phonenumber", VARCHAR(25), nullable=True)
    income_tax_no = Column("pinno", VARCHAR(255), nullable=True)
    date_of_birth = Column("dob", VARCHAR(255), nullable=True)
    national_id_no = Column("idno", VARCHAR(255), nullable=True)
    agent_code = Column("agentcode", VARCHAR(25), nullable=True)
    agent_name = Column("agentname", VARCHAR(255), nullable=True)
    gender = Column("gender", VARCHAR(255), nullable=True)
    relationship = Column("relationship", VARCHAR(255), nullable=True)
    relationshipid = Column("relationshipid", VARCHAR(255), nullable=True)
    policyholdername = Column("policyholdername", VARCHAR(255), nullable=True)
    policyholderid = Column("policyholderid", VARCHAR(255), nullable=True)
    policyid = Column("policyid", VARCHAR(255), nullable=True)

    def __init__(self):
        self.country = "KENYA"

    # def __init__(self, customer_id_actisure):
    #     self.source_application = "ACTISURE"
    #     self.line_of_business = "MEDICAL"
    #     self.country = "KENYA"
    #     self.customer_kyc_id = None
    #     self.customer_id_apex = None
    #     self.customer_id_isf = None
    #     self.customer_id_premia = None
    #     self.customer_id_ofa = None
    #     self.customer_id_jubilee_insure = None
    #     self.email_address_premia = None
    #     self.phone_no_premia = None
    #     self.email_address_isf = None
    #     self.phone_no_actisure = None
    #     self.email_address_apex = None
    #     self.phone_no_apex = None
    #     self.email_address_jubilee_insure = None
    #     self.phone_no_jubilee_insure = None
    #     self.email_address_ofa = None
    #     self.phone_no_jubilee_ofa = None

    def __repr__(self): return self.__str__()

    def __str__(self): return "<CustomerDetails(%(customer_id_actisure)s)>" % self.__dict__


class MedicalInformation(ACTISUREFailOverBase):

    """The main table relating to a client with its principal address."""

    __tablename__ = 'production_memberinfo'

    #policy_no = Column("POLICYNUMBER", VARCHAR(255), primary_key=True, nullable=False)
    memberinfoid = Column("production_memberinfo_id", VARCHAR(255), primary_key=True, nullable=False)
    member_no = Column("entityid", VARCHAR(255), nullable=True)
    policy_status = Column("policystatus", VARCHAR(255), nullable=True)
    policy_start_date = Column("policystartdate", VARCHAR(255), nullable=True)
    policy_class = Column("product", VARCHAR(255), nullable=True)
    renewal_date = Column("renewaldate", VARCHAR(255), nullable=True)
    cover_limit = Column("benefitlimit", VARCHAR(255), nullable=True)
    benefit_distribution = Column("benefit_distribution", VARCHAR(255), nullable=True)
    scope_of_benefit = Column("invoicebenefit", VARCHAR(255), nullable=True)
    #utilized_amount = Column("UTILIZEDAMOUNT", VARCHAR(255), nullable=True)

    def __init__(self):
        #self.memberinfoid = None
        self.member_no = None
        # self.benefit_distribution = None
        # self.policy_status = None
        # self.policy_start_date = None
        # self.policy_class = None
        # self.renewal_date = None
        # self.scope_of_benefit = None
        # self.cover_limit = None

    def __repr__(self): return self.__str__()

    def __str__(self): return "<MedicalInformation(%(member_no)s)>" % self.__dict__


class Agent(ACTISUREFailOverBase):

    """The main table relating to a client with its principal address."""

    __tablename__ = 'agentdetails'

    code = Column("agentcode", VARCHAR(255), primary_key=True, nullable=False)
    name = Column("agentname", VARCHAR(255), nullable=True)
    type = Column("relationshiptype", VARCHAR(255), nullable=True)
    phone_number = Column("phonenumber", VARCHAR(255), nullable=True)

    def __init__(self):
        self.code = None
        self.name = None
        self.type = None
        self.phone_number = None

    def __repr__(self): return self.__str__()

    def __str__(self): return "<Agent(%(name)s)>" % self.__dict__

