from sqlalchemy import Column, String as VARCHAR, Integer

from database import OnlineBimaBase


class INTEGER(Integer):
    def __init__(self, *args, **kwargs): super(Integer, self).__init__()


class SMSProxy(OnlineBimaBase):

    __tablename__ = 'JHL_SMS_PROXY'

    id = Column("ID", VARCHAR(255), primary_key=True, nullable=False)
    phone_number = Column("PHONE", VARCHAR(255), nullable=True)
    sender = Column("SENDER", VARCHAR(255), nullable=True)
    text_content = Column("TEXT", VARCHAR(255), nullable=True)
    delivery_timestamp = Column("DLR_TIMESTAMP", VARCHAR(255), nullable=True)
    delivery_status = Column("DLR_DESCRIPTION", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<SMSProxy(%(id)s)>" % self.__dict__


class CampaignADHOC(OnlineBimaBase):

    __tablename__ = 'JHL_CAMPAIGN_ADHOC'

    phone_number = Column("PHONE_NO", VARCHAR(255), primary_key=True, nullable=False)
    name = Column("CLIENT_NAME", VARCHAR(255), nullable=True)
    campaign_name = Column("CAMPAIGN_NAME", VARCHAR(255), nullable=True)
    policy_no = Column("COLUMN2", VARCHAR(255), nullable=True)
    kra_pin = Column("COLUMN4", VARCHAR(255), nullable=True)
    national_id = Column("COLUMN5", VARCHAR(255), nullable=True)
    address_one = Column("COLUMN6", VARCHAR(255), nullable=True)
    address_two = Column("COLUMN7", VARCHAR(255), nullable=True)
    beneficiary_name = Column("COLUMN8", VARCHAR(255), nullable=True)
    sum_covered = Column("COLUMN9", VARCHAR(255), nullable=True)
    n_term = Column("COLUMN10", VARCHAR(255), nullable=True)
    v_pymt_desc = Column("COLUMN11", VARCHAR(255), nullable=True)
    method_name = Column("COLUMN12", VARCHAR(255), nullable=True)
    plan_name = Column("COLUMN13", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<CampaignADHOC(%(phone_number)s)>" % self.__dict__


class ClientFeedback(OnlineBimaBase):

    __tablename__ = 'JHL_CLIENT_FEEDBACK'

    phone_no = Column("PHONE_NO", VARCHAR(255), primary_key=True, nullable=False)
    campaign_type = Column("CAMPAIGN_TYPE", VARCHAR(255), nullable=True)
    client_name = Column("CLIENT_NAME", VARCHAR(255), nullable=True)
    id_number = Column("ID_NUMBER", VARCHAR(255), nullable=True)
    aware_unpaid_dda = Column("AWARE_UNPAID_DDA", VARCHAR(255), nullable=True)
    reason_not_paying = Column("REASON_NOT_PAYING", VARCHAR(255), nullable=True)
    feedback_pay = Column("FEEDBACK_PAY", VARCHAR(255), nullable=True)
    general_comment = Column("GEN_COMMENT", VARCHAR(255), nullable=True)
    created_date = Column("CREATED_DATE", VARCHAR(255), nullable=True)
    policy_number = Column("POLICY_NUMBER", VARCHAR(255), nullable=True)
    campaign_name = Column("CAMPAIGN_NAME", VARCHAR(255), nullable=True)
    doc_received = Column("DOC_RECEIVED", VARCHAR(255), nullable=True)
    cust_understands = Column("CUST_UNDERSTANDS", VARCHAR(255), nullable=True)
    cust_satisfaction = Column("CUST_SATISFACTION", VARCHAR(255), nullable=True)
    dissat_reason = Column("DISSAT_REASON", VARCHAR(255), nullable=True)
    payment_date = Column("PAYMENT_DATE", VARCHAR(255), nullable=True)
    recommend = Column("RECOMMEND", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<ClientFeedback(%(phone_no)s)>" % self.__dict__

