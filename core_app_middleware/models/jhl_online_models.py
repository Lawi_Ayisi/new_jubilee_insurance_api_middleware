from sqlalchemy import Column, String as VARCHAR, Integer

from database import JHLOnlineBase


class INTEGER(Integer):
    def __init__(self, *args, **kwargs): super(Integer, self).__init__()


class GPNTStatement(JHLOnlineBase):

    __tablename__ = 'GPNT_STATEMENTS'

    member_no = Column("V_MEMBERNO", VARCHAR(255), primary_key=True, nullable=False)
    email = Column("E_MAIL", VARCHAR(255), nullable=True)
    n_member_op_reg = Column("N_MEMBEROPREG", VARCHAR(255), nullable=True)
    n_member_op_reg_vol = Column("N_MEMBEROPREGVOL", VARCHAR(255), nullable=True)
    n_member_total_reg = Column("N_MEMBERTOTALREG", VARCHAR(255), nullable=True)
    n_member_reg_transfer = Column("N_MEMBERREGTRANSFER", VARCHAR(255), nullable=True)
    n_member_reg_expense = Column("N_MEMBERREGEXPENSE", VARCHAR(255), nullable=True)
    n_member_op_reg_interest = Column("N_MEMBEROPREGINTEREST", VARCHAR(255), nullable=True)
    n_member_reg_rba_levy = Column("N_MEMBERREGRBALEVY", VARCHAR(255), nullable=True)
    n_member_reg_interest = Column("N_MEMBERREGINTEREST", VARCHAR(255), nullable=True)

    n_member_op_unreg = Column("N_MEMBEROPUNREG", VARCHAR(255), nullable=True)
    n_member_op_unreg_vol = Column("N_MEMBEROPUNREGVOL", VARCHAR(255), nullable=True)
    n_member_total_unreg = Column("N_MEMBERTOTALUNREG", VARCHAR(255), nullable=True)
    n_member_unreg_transfer = Column("N_MEMBERUNREGTRANSFER", VARCHAR(255), nullable=True)
    n_member_unreg_expense = Column("N_MEMBERUNREGEXPENSE", VARCHAR(255), nullable=True)
    n_member_op_unreg_interest = Column("N_MEMBEROPUNREGINTEREST", VARCHAR(255), nullable=True)
    n_member_unreg_rba_levy = Column("N_MEMBERUNREGRBALEVY", VARCHAR(255), nullable=True)
    n_member_unreg_interest = Column("N_MEMBERUNREGINTEREST", VARCHAR(255), nullable=True)
    n_member_unreg_tax = Column("N_MEMBERUNREGTAX", VARCHAR(255), nullable=True)

    n_employer_op_reg = Column("N_EMPLOYEROPREG", VARCHAR(255), nullable=True)
    n_employer_op_reg_sp = Column("N_EMPLOYEROPREGSP", VARCHAR(255), nullable=True)
    n_employer_total_reg = Column("N_EMPLOYERTOTALREG", VARCHAR(255), nullable=True)
    n_employer_reg_transfer = Column("N_EMPLOYERREGTRANSFER", VARCHAR(255), nullable=True)
    n_employer_reg_expense = Column("N_EMPLOYERREGEXPENSE", VARCHAR(255), nullable=True)
    n_employer_op_reg_interest = Column("N_EMPLOYEROPREGINTEREST", VARCHAR(255), nullable=True)
    n_employer_reg_rba_levy = Column("N_EMPLOYERREGRBALEVY", VARCHAR(255), nullable=True)
    n_employer_reg_interest = Column("N_EMPLOYERREGINTEREST", VARCHAR(255), nullable=True)

    n_employer_op_unreg = Column("N_EMPLOYEROPUNREG", VARCHAR(255), nullable=True)
    n_employer_op_unreg_sp = Column("N_EMPLOYEROPUNREGSP", VARCHAR(255), nullable=True)
    n_employer_total_unreg = Column("N_EMPLOYERTOTALUNREG", VARCHAR(255), nullable=True)
    n_employer_unreg_transfer = Column("N_EMPLOYERUNREGTRANSFER", VARCHAR(255), nullable=True)
    n_employer_un_reg_expense = Column("N_EMPLOYERUNREGEXPENSE", VARCHAR(255), nullable=True)
    n_employer_op_unreg_interest = Column("N_EMPLOYEROPUNREGINTEREST", VARCHAR(255), nullable=True)
    n_employer_unreg_rba_levy = Column("N_EMPLOYERUNREGRBALEVY", VARCHAR(255), nullable=True)
    n_employer_unreg_interest = Column("N_EMPLOYERUNREGINTEREST", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<GPNTStatement(%(member_no)s)>" % self.__dict__

