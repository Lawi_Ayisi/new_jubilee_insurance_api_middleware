from sqlalchemy import Column, String as VARCHAR, Integer

from database import PremiaBase


class INTEGER(Integer):
    def __init__(self, *args, **kwargs): super(Integer, self).__init__()


class GeneralPolicy(PremiaBase):

    """The main table relating to a client with its principal address."""

    '''
    [
  {
    "ASSR_DOB": null,
    "ASSR_ID_NO": "0879619",
    "ASSR_PIN_NO": "A001357410G",
    "ASSR_PHONE": "0721221587",
    "ASSR_NAME": "ANNE JERONO JOSE",
    "ASSR_CODE": "1288330",
    "POL_NO": "P/ELD/2010/2015/212"
  }
]
    '''
    __tablename__ = 'CUSTOMERDETAILS'

    customer_id_premia = Column("ASSR_CODE", VARCHAR(255), primary_key=True, nullable=False)
    cust_id_premia = Column("CUST_CODE", VARCHAR(255), nullable=True)
    customer_name = Column("ASSR_NAME", VARCHAR(255), nullable=True)
    policy_no = Column("POL_NO", VARCHAR(255), nullable=True)
    phone_no_premia = Column("ASSR_PHONE", VARCHAR(25), nullable=True)
    income_tax_no = Column("ASSR_PIN_NO", VARCHAR(255), nullable=True)
    date_of_birth = Column("ASSR_DOB", VARCHAR(255), nullable=True)
    national_id_no = Column("ASSR_ID_NO", VARCHAR(255), nullable=True)

    def __init__(self, customer_id_isf):
        self.source_application = "PREMIA"
        self.line_of_business = "GENERAL"
        self.country = "KENYA"
        self.customer_kyc_id = None
        self.customer_id_apex = None
        self.customer_id_actisure = None
        self.customer_id_isf = None
        self.customer_id_ofa = None
        self.customer_id_jubilee_insure = None
        self.email_address_premia = None
        self.phone_no_isf = None
        self.email_address_actisure = None
        self.phone_no_actisure = None
        self.email_address_apex = None
        self.phone_no_apex = None
        self.email_address_jubilee_insure = None
        self.phone_no_jubilee_insure = None
        self.email_address_ofa = None
        self.phone_no_jubilee_ofa = None
        self.cust_id_premia = None

    def __repr__(self): return self.__str__()

    def __str__(self): return "<GeneralPolicy(%(customer_id_premia)s)>" % self.__dict__


class CustomerDetails(PremiaBase):

    '''
    [
      {
        "VEH_VEHICLE_ID": "zb 061",
        "CUSTOMER_ID": "7023196",
        "CUSTOMER_NAME": "MOMBASA SALT WORKS LTD                                      ",
        "CUSTOMER_NATIONAL_ID": null,
        "POL_NO": "P/MSA/2040/2004/456",
        "CUSTOMER_EMAIL": null,
        "CUSTOMER_PHONE": null,
        "CUSTOMER_KRA_PIN": null,
        "CUSTOMER_DOB": null,
        "CUSTOMER_CREATED_DATE": null,
        "AGENT_CODE": "200056",
        "AGENT_ID": "200056",
        "AGENT_NAME": "NOMURA INSURANCE BROKERS LIMITED",
        "AGENT_KRA_PIN": "P000620694X",
        "AGENT_DOB": null,
        "POL_SYS_ID": 35783,
        "POL_END_NO": "E/MSA/2040/05/6971",
        "POL_END_NO_IDX": 2,
        "RISK_SYS_ID": 104284
      }
    ]
    '''

    __tablename__ = 'VEHICLE_QUERY'

    vehicle_reg_no = Column("VEH_VEHICLE_ID", VARCHAR(255), primary_key=True, nullable=False)
    customer_name = Column("CUSTOMER_NAME", VARCHAR(255), primary_key=True, nullable=False)
    policy_no = Column("POL_NO", VARCHAR(255), nullable=True)
    email = Column("CUSTOMER_EMAIL", VARCHAR(255), nullable=True)
    phone_number = Column("CUSTOMER_PHONE", VARCHAR(255), nullable=True)
    agent_code = Column("AGENT_CODE", VARCHAR(255), nullable=True)
    agent_name = Column("AGENT_NAME", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<CustomerDetail(%(customer_name)s)>" % self.__dict__


class PolicyDetails(PremiaBase):

    __tablename__ = 'POLICYDETAILS'

    policy_no = Column("POLICY_NUMBER", VARCHAR(255), primary_key=True, nullable=False)
    assured_id = Column("ASSR_CODE", VARCHAR(255), nullable=True)
    status = Column("STATUS", VARCHAR(255), nullable=True)
    pol_subclass = Column("SC_NAME", VARCHAR(255), nullable=True)
    pol_class = Column("CLASS_NAME", VARCHAR(255), nullable=True)
    from_date = Column("PR_FM_DT", VARCHAR(255), nullable=True)
    to_date = Column("PR_TO_DT", VARCHAR(255), nullable=True)
    net_premium = Column("NET_PREMIUM", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<PolicyDetails(%(policy_no)s)>" % self.__dict__


class RiskDetails(PremiaBase):

    __tablename__ = 'RISKDETAILS'

    vehicle_doc_no = Column("VEH_DOC_NO", VARCHAR(255), nullable=True)
    vehicle_reg_no = Column("VEH_REGN_NO", VARCHAR(255), primary_key=True, nullable=False)
    policy_no = Column("POL_NO", VARCHAR(255), nullable=True)
    assured_name = Column("ASSR_NAME", VARCHAR(255), nullable=True)
    policy_end_no = Column("POL_END_NO", VARCHAR(255), nullable=True)
    pol_end_no_idx = Column("POL_END_NO_IDX", VARCHAR(255), nullable=True)
    veh_cert_fm_dt = Column("VEH_CERT_FM_DT", VARCHAR(255), nullable=True)
    veh_cert_to_dt = Column("VEH_CERT_TO_DT", VARCHAR(255), nullable=True)
    veh_regn_date = Column("VEH_REGN_DATE", VARCHAR(255), nullable=True)
    veh_cr_uid = Column("VEH_CR_UID", VARCHAR(255), nullable=True)
    veh_cert_type = Column("VEH_CERT_TYPE", VARCHAR(255), nullable=True)
    pol_sys_id = Column("POL_SYS_ID", VARCHAR(255), nullable=True)
    veh_sys_id = Column("VEH_SYS_ID", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<RiskDetails(%(vehicle_reg_no)s)>" % self.__dict__

class Agent(PremiaBase):

    """The main table relating to a client with its principal address."""

    __tablename__ = 'VW_AGENT_INFO'

    code = Column("AGENT CODE", VARCHAR(255), primary_key=True, nullable=False)
    name = Column("AGENT NAME", VARCHAR(255), nullable=True)

    def __init__(self, customer_id_apex, policy_no):
        self.code = None
        self.name = None

    def __repr__(self): return self.__str__()

    def __str__(self): return "<Agent(%(code)s)>" % self.__dict__