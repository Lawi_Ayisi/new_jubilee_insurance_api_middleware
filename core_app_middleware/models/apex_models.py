from sqlalchemy import Column, String as VARCHAR, Integer

from database import APEXBase


class INTEGER(Integer):
    def __init__(self, *args, **kwargs): super(Integer, self).__init__()


class PensionPolicy(APEXBase):

    """The main table relating to a client with its principal address."""

    '''
[
  {
    "POLICY_NO": "56977",
    "CUSTOMER_ID": "56977",
    "CUSTOMER_NAME": "Phyllis Kihika",
    "INCOME_TAX_NO": null,
    "IDTYPE": null,
    "NATIONAL_ID_NO": null,
    "EMAIL_ADDRESS_APEX": null,
    "GENDER": null,
    "DATE_OF_BIRTH": "1971-04-18 00:00:00",
    "AGENT_CODE": "1526",
    "AGENT_NAME": "Noble Strides Insurance Agency",
    "TELEPHONE": "702000001",
    "CREATED_ON": "2016-11-16 12:06:31.000000"
  }
]
    '''
    __tablename__ = 'V_PENSIONS_POLICIES'

    policy_no = Column("POLICY_NO", VARCHAR(255), nullable=True)
    customer_id_apex = Column("CUSTOMER_ID", VARCHAR(255), primary_key=True, nullable=False)
    customer_name = Column("CUSTOMER_NAME", VARCHAR(255), nullable=True)
    income_tax_no = Column("INCOME_TAX_NO", VARCHAR(255), nullable=True)
    id_type = Column("IDTYPE", VARCHAR(255), nullable=True)
    national_id_no = Column("NATIONAL_ID_NO", VARCHAR(255), nullable=True)
    email_address_apex = Column("EMAIL_ADDRESS_APEX", VARCHAR(255), nullable=True)
    gender = Column("GENDER", VARCHAR(255), nullable=True)
    date_of_birth = Column("DATE_OF_BIRTH", VARCHAR(255), nullable=True)
    phone_no_apex = Column("TELEPHONE", VARCHAR(25), nullable=True)
    agent_code = Column("AGENT_CODE", VARCHAR(25), nullable=True)
    agent_name = Column("AGENT_NAME", VARCHAR(255), nullable=True)

    def __init__(self, customer_id_apex):
        self.source_application = "APEX"
        self.line_of_business = "PENSION"
        self.country = "KENYA"
        self.customer_kyc_id = None
        self.customer_id_apex = None
        self.customer_id_actisure = None
        self.customer_id_premia = None
        self.customer_id_ofa = None
        self.customer_id_jubilee_insure = None
        self.email_address_premia = None
        self.phone_no_premia = None
        self.email_address_actisure = None
        self.phone_no_actisure = None
        self.email_address_apex = None
        self.phone_no_apex = None
        self.email_address_jubilee_insure = None
        self.phone_no_jubilee_insure = None
        self.email_address_ofa = None
        self.phone_no_jubilee_ofa = None

    def __repr__(self): return self.__str__()

    def __str__(self): return "<PensionPolicy(%(customer_id_apex)s)>" % self.__dict__


class PensionAccountInfo(APEXBase):

    """The main table relating to a client with its principal address."""

    __tablename__ = 'VW_ACCOUNT_INFO'

    policy_no = Column("POLICY_NUMBER", VARCHAR(255), nullable=True)
    old_policy_no = Column("LEG_MEM_NUM", VARCHAR(255), primary_key=True, nullable=True)
    product = Column("FULL_SCHEMENAME", VARCHAR(255), nullable=False)
    policy_start_date = Column("SCHEME JOIN DATE", VARCHAR(255), nullable=True)
    national_id = Column("IDNUMBER", VARCHAR(255), nullable=True)
    member_name = Column("MEMBERNAME", VARCHAR(255), nullable=True)
    policy_status = Column("MEMBERSTATUS", VARCHAR(255), nullable=True)

    def __init__(self, customer_id_apex, policy_no):
        self.policy_no = None
        self.old_policy_no = None
        self.product = None
        self.policy_start_date = None
        self.national_id = None
        self.member_name = None
        self.policy_status = None

    def __repr__(self): return self.__str__()

    def __str__(self): return "<PensionAccountInfo(%(policy_no)s)>" % self.__dict__


class Agent(APEXBase):

    """The main table relating to a client with its principal address."""

    __tablename__ = 'VW_AGENT_INFO'

    code = Column("AGENT CODE", VARCHAR(255), primary_key=True, nullable=False)
    name = Column("AGENT NAME", VARCHAR(255), nullable=True)

    def __init__(self, customer_id_apex, policy_no):
        self.code = None
        self.name = None

    def __repr__(self): return self.__str__()

    def __str__(self): return "<Agent(%(code)s)>" % self.__dict__


class GPNTStatement(APEXBase):

    __tablename__ = 'GPNT_STATEMENTS'

    '''
    "V_SCHEMENAME" : "The Personal Pension Plan",
		"MEMBERNO" : "PP0125872007",
		"V_MEMBERNAME" : "JANET KAVILU MACKENZIE",
		"D_VALUATIONDATE" : "2017-06-27T07:38:40Z",
		"N_REGINTERESTRATE" : null,
		"N_UNREGINTERESTRATE" : null,
		"N_VESTING" : null,
		"N_MEMBEROPREG" : 3137672.22,
		"N_MEMBEROPREGVOL" : 0,
		"N_MEMBEROPUNREG" : 52731.86,
		"N_MEMBEROPUNREGVOL" : 0,
		"N_MEMBERREGRBALEVY" : 6275.34,
		"N_MEMBERVOLREGRBALEVY" : 0,
		"N_MEMBERUNREGRBALEVY" : 105.46,
		"N_MEMBERVOLUNREGRBALEVY" : 0,
		"N_MEMBERREGTRANSFER" : 0,
		"N_MEMBERVOLREGTRANSFER" : 0,
		"N_MEMBERUNREGTRANSFER" : 0,
		"N_MEMBERVOLUNREGTRANSFER" : 0,
		"N_MEMBERREGEXPENSE" : null,
		"N_MEMBERVOLREGEXPENSE" : null,
		"N_MEMBERVOLUNREGEXPENSE" : null,
		"N_MEMBERUNREGEXPENSE" : null,
		"N_MEMBERREGPOLICYFEE" : 0,
		"N_MEMBERVOLREGPOLICYFEE" : 0,
		"N_MEMBERVOLUNREGPOLICYFEE" : 0,
		"N_MEMBERUNREGPOLICYFEE" : 0,
		"N_MEMBERREGADMEXPENSE" : null,
		"N_MEMBERVOLREGADMEXPENSE" : null,
		"N_MEMBERVOLUNREGADMEXPENSE" : null,
		"N_MEMBERUNREGADMEXPENSE" : null,
		"N_MEMBERTOTALREG" : 79200,
		"N_MEMBERTOTALREGVOL" : 0,
		"N_MEMBERTOTALUNREG" : 0,
		"N_MEMBERTOTALUNREGVOL" : 0,
		"N_MEMBERREGINTEREST" : 1066.93,
		"N_MEMBERVOLREGINTEREST" : 0,
		"N_MEMBERVOLUNREGINTEREST" : 0,
		"N_MEMBERUNREGINTEREST" : -3.02,
		"N_MEMBERREGTRANSFEROUT" : null,
		"N_MEMBERVOLREGTRANSFEROUT" : null,
		"N_MEMBERUNREGTRANSFEROUT" : null,
		"N_MEMBERVOLUNREGTRANSFEROUT" : null,
		"N_MEMBERREGTAX" : null,
		"N_MEMBERVOLREGTAX" : null,
		"N_MEMBERUNREGTAX" : -455.07,
		"N_MEMBERVOLUNREGTAX" : 0,
		"N_MEMBERWREGTAX" : 3.02,
		"N_MEMBERVOLWREGTAX" : 0,
		"N_MEMBERNORMALBALREG" : 3302103.01,
		"N_MEMBERGROSSBENREG" : null,
		"N_MEMBERWITHDRAWALREG" : null,
		"N_MEMBERGROSSSURPLUSREG" : null,
		"N_MEMBERNORMALBALUNREG" : 53688.24,
		"N_MEMBERGROSSBENUNREG" : null,
		"N_MEMBERWITHDRAWALUNREG" : null,
		"N_MEMBERGROSSSURPLUSUNREG" : null,
		"N_MEMBERVOLREGBAL" : 0,
		"N_MEMBERVOLUNREGBAL" : 0,
		"N_TOTALMEMBERBAL" : null,
		"N_EMPLOYEROPREG" : 0,
		"N_EMPLOYEROPREGSP" : 0,
		"N_EMPLOYEROPUNREG" : 0,
		"N_EMPLOYEROPUNREGSP" : 0,
		"N_EMPLOYERREGRBALEVY" : 0,
		"N_EMPLOYERSPREGRBALEVY" : 0,
		"N_EMPLOYERUNREGRBALEVY" : 0,
		"N_EMPLOYERSPUNREGRBALEVY" : 0,
		"N_EMPLOYERREGEXPENSE" : null,
		"N_EMPLOYERSPREGEXPENSE" : null,
		"N_EMPLOYERUNREGEXPENSE" : null,
		"N_EMPLOYERSPUNREGEXPENSE" : null,
		"N_EMPLOYERREGADMEXPENSE" : null,
		"N_EMPLOYERSPREGADMEXPENSE" : null,
		"N_EMPLOYERUNREGADMEXPENSE" : null,
		"N_EMPLOYERSPUNREGADMEXPENSE" : null,
		"N_EMPLOYERREGPOLICYFEE" : 0,
		"N_EMPLOYERSPREGPOLICYFEE" : 0,
		"N_EMPLOYERUNREGPOLICYFEE" : 0,
		"N_EMPLOYERSPUNREGPOLICYFEE" : 0,
		"N_EMPLOYERREGTRANSFER" : 0,
		"N_EMPLOYERSPREGTRANSFER" : 0,
		"N_EMPLOYERUNREGTRANSFER" : 0,
		"N_EMPLOYERSPUNREGTRANSFER" : 0,
		"N_EMPLOYERREGTRANSFEROUT" : null,
		"N_EMPLOYERSPREGTRANSFEROUT" : null,
		"N_EMPLOYERUNREGTRANSFEROUT" : null,
		"N_EMPLOYERSPUNREGTRANSFEROUT" : null,
		"N_EMPLOYERTOTALREG" : 0,
		"N_EMPLOYERTOTALUNREG" : 0,
		"N_EMPLOYERTOTALREGSP" : 0,
		"N_EMPLOYERTOTALUNREGSP" : 0,
		"N_EMPLOYERREGINTEREST" : 0,
		"N_EMPLOYERSPREGINTEREST" : 0,
		"N_EMPLOYERUNREGINTEREST" : 0,
		"N_EMPLOYERSPUNREGINTEREST" : 0,
		"N_EMPLOYERREGTAX" : null,
		"N_EMPLOYERSPREGTAX" : null,
		"N_EMPLOYERUNREGTAX" : 0,
		"N_EMPLOYERSPUNREGTAX" : 0,
		"N_EMPLOYERWREGTAX" : 0,
		"N_EMPLOYERSPWREGTAX" : 0,
		"N_TOTALEMPLOYERBAL" : null,
		"N_EMPLOYERBAL" : null,
		"N_EMPLOYERUNVESTED" : null,
		"N_TOTALBEFORETAX" : null,
		"N_TOTALAFTERTAX" : null,
		"N_EMPLOYERNORMALBALREG" : 0,
		"N_EMPLOYERNORMALBALUNREG" : 0,
		"N_EMPLOYERSPREGBAL" : 0,
		"N_EMPLOYERSPUNREGBAL" : 0,
		"N_EMPLOYERGROSSBENREG" : null,
		"N_EMPLOYERWITHDRAWALREG" : null,
		"N_EMPLOYERGROSSSURPLUSREG" : null,
		"N_EMPLOYERGROSSBENUNREG" : null,
		"N_EMPLOYERWITHDRAWALUNREG" : null,
		"N_EMPLOYERGROSSSURPLUSUNREG" : null,
		"N_MEMBERREGDEDUCT" : null,
		"N_MEMBERVOLREGDEDUCT" : null,
		"N_MEMBERVOLUNREGDEDUCT" : null,
		"N_MEMBERUNREGDEDUCT" : null,
		"N_EMPLOYERREGDEDUCT" : null,
		"N_EMPLOYERSPREGDEDUCT" : null,
		"N_EMPLOYERUNREGDEDUCT" : null,
		"N_EMPLOYERSPUNREGDEDUCT" : null,
		"N_EMPLOYERBALREG" : null,
		"N_EMPLOYERBALUNREG" : null,
		"N_MEMBERBALREG" : null,
		"N_MEMBERBALUNREG" : null,
		"N_GROSSREGPAID" : null,
		"N_GROSSUNREGPAID" : null,
		"N_GROSSANNUITYAMOUNTREG" : null,
		"N_GROSSANNUITYAMOUNTUNREG" : null,
		"V_UPDATECOUNTER" : null,
		"N_EMPLOYERUNVESTEDREG" : null,
		"V_USER" : null,
		"N_MEMBERREGNSSFTRANSFER" : null,
		"N_EMPLOYERREGNSSFTRANSFER" : null,
		"N_MEMBERREGNSSFINTEREST" : null,
		"N_EMPLOYERREGNSSFINTEREST" : null,
		"N_MEMBERNORMALBALREG_NSSF" : null,
		"N_EMPLOYERNORMALBALREG_NSSF" : null,
		"N_MEMBEROPREG_NSSF" : null,
		"N_EMPLOYEROPREG_NSSF" : null,
		"N_MEMBERREGTRANSOUT_NSSF" : null,
		"N_EMPLOYERREGTRANSOUT_NSSF" : null,
		"N_MEMBERTOTALREG_NSSF" : null,
		"N_EMPLOYERTOTALREG_NSSF" : null,
		"N_REGNSSFINTERESTRATE" : null,
		"N_GROSSREGPAID_NSSF" : null,
		"N_MEMBERREGNSSFRBALEVY" : null,
		"N_EMPLOYERREGNSSFRBA_LEVY" : null,
		"N_MEMBERREGEXPENSE_NSSF" : null,
		"N_EMPLOYERREGEXPENSE_NSSF" : null,
		"N_MEMBERREGPOLICYFEE_NSSF" : null,
		"N_EMPLOYERREGPOLICYFEE_NSSF" : null,
		"N_MEMBERBALREG_NSSF" : null,
		"N_EMPLOYERBALREG_NSSF" : null,
		"N_MEMBERWITHDRAWALREG_NSSF" : null,
		"N_MEMBERGROSSBENREG_NSSF" : null,
		"N_MEMBERGROSSSURPLUSREG_NSSF" : null,
		"N_EMPLOYERWITHDRAWALREG_NSSF" : null,
		"N_EMPLOYERGROSSBENREG_NSSF" : null,
		"N_EMPLOYERGROSSSURPLUSREG_NSSF" : null,
		"N_EMPLOYERUNVESTED_NSSF" : null,
		"N_EMPLOYERBAL_NSSF" : null,
		"N_MEMBERBAL_NSSF" : null,
		"N_MEMBERVOLREGNSSFRBALEVY" : null,
		"N_MEMBERTOTALREGNSSFVOL" : null,
		"N_MEMBERVOLREGNSSFTRANSFER" : null,
		"N_MEMBERVOLREGNSSFEXPENSE" : null,
		"N_MEMBERVOLREGNSSFINTEREST" : null,
		"N_MEMBERVOLREGNSSFTAX" : null,
		"N_MEMBERVOLREGNSSFTRANSFEROUT" : null,
		"N_MEMBERVOLREGNSSFBAL" : null,
		"N_MEMBEROPREGNSSFVOL" : null,
		"N_EMPLOYERSPREGNSSFTAX" : null,
		"PV_SCHEMECODE" : "100999",
		"PV_COMPANYCODE" : "1001",
		"PV_BRANCHCODE" : "    ",
		"N_MEMBEROPREGINTEREST" : 90439.2,
		"N_EMPLOYEROPREGINTEREST" : 0,
		"N_EMPLOYEROPUNREGINTEREST" : 0,
		"N_MEMBEROPUNREGINTEREST" : 1519.93,
		"PV_CATEGORYCODE" : "    ",
		"DBLPARTIALWITH" : null,
		"DBLPARTIALDIFF" : null,
		"DBLPARTIALWITHUNREG" : null,
		"DBLPARTIALDIFFUNREG" : null,
		"USER_ID" : null,
		"STATUS" : 0.0,
		"V_MEMBERIDNO" : "A1000198",
		"D_ENDDATE" : "2017-12-30T21:00:00Z",
		"D_STARTDATE" : "2016-12-31T21:00:00Z",
		"E_MAIL" : "jttmackenzie@yahoo.com"
	},

    
    '''

    member_no = Column("MEMBERNO", VARCHAR(255), primary_key=True, nullable=False)
    ##email = Column("E_MAIL", VARCHAR(255), nullable=True)
    n_member_op_reg = Column("N_MEMBEROPREG", VARCHAR(255), nullable=True)
    n_member_op_reg_vol = Column("N_MEMBEROPREGVOL", VARCHAR(255), nullable=True)
    n_member_total_reg = Column("N_MEMBERTOTALREG", VARCHAR(255), nullable=True)
    n_member_reg_transfer = Column("N_MEMBERREGTRANSFER", VARCHAR(255), nullable=True)
    n_member_reg_expense = Column("N_MEMBERREGEXPENSE", VARCHAR(255), nullable=True)
    n_member_op_reg_interest = Column("N_MEMBEROPREGINTEREST", VARCHAR(255), nullable=True)
    n_member_reg_rba_levy = Column("N_MEMBERREGRBALEVY", VARCHAR(255), nullable=True)
    n_member_reg_interest = Column("N_MEMBERREGINTEREST", VARCHAR(255), nullable=True)

    n_member_op_unreg = Column("N_MEMBEROPUNREG", VARCHAR(255), nullable=True)
    n_member_op_unreg_vol = Column("N_MEMBEROPUNREGVOL", VARCHAR(255), nullable=True)
    n_member_total_unreg = Column("N_MEMBERTOTALUNREG", VARCHAR(255), nullable=True)
    n_member_unreg_transfer = Column("N_MEMBERUNREGTRANSFER", VARCHAR(255), nullable=True)
    n_member_unreg_expense = Column("N_MEMBERUNREGEXPENSE", VARCHAR(255), nullable=True)
    n_member_op_unreg_interest = Column("N_MEMBEROPUNREGINTEREST", VARCHAR(255), nullable=True)
    n_member_unreg_rba_levy = Column("N_MEMBERUNREGRBALEVY", VARCHAR(255), nullable=True)
    n_member_unreg_interest = Column("N_MEMBERUNREGINTEREST", VARCHAR(255), nullable=True)
    n_member_unreg_tax = Column("N_MEMBERUNREGTAX", VARCHAR(255), nullable=True)

    n_employer_op_reg = Column("N_EMPLOYEROPREG", VARCHAR(255), nullable=True)
    n_employer_op_reg_sp = Column("N_EMPLOYEROPREGSP", VARCHAR(255), nullable=True)
    n_employer_total_reg = Column("N_EMPLOYERTOTALREG", VARCHAR(255), nullable=True)
    n_employer_reg_transfer = Column("N_EMPLOYERREGTRANSFER", VARCHAR(255), nullable=True)
    n_employer_reg_expense = Column("N_EMPLOYERREGEXPENSE", VARCHAR(255), nullable=True)
    n_employer_op_reg_interest = Column("N_EMPLOYEROPREGINTEREST", VARCHAR(255), nullable=True)
    n_employer_reg_rba_levy = Column("N_EMPLOYERREGRBALEVY", VARCHAR(255), nullable=True)
    n_employer_reg_interest = Column("N_EMPLOYERREGINTEREST", VARCHAR(255), nullable=True)

    n_employer_op_unreg = Column("N_EMPLOYEROPUNREG", VARCHAR(255), nullable=True)
    n_employer_op_unreg_sp = Column("N_EMPLOYEROPUNREGSP", VARCHAR(255), nullable=True)
    n_employer_total_unreg = Column("N_EMPLOYERTOTALUNREG", VARCHAR(255), nullable=True)
    n_employer_unreg_transfer = Column("N_EMPLOYERUNREGTRANSFER", VARCHAR(255), nullable=True)
    n_employer_un_reg_expense = Column("N_EMPLOYERUNREGEXPENSE", VARCHAR(255), nullable=True)
    n_employer_op_unreg_interest = Column("N_EMPLOYEROPUNREGINTEREST", VARCHAR(255), nullable=True)
    n_employer_unreg_rba_levy = Column("N_EMPLOYERUNREGRBALEVY", VARCHAR(255), nullable=True)
    n_employer_unreg_interest = Column("N_EMPLOYERUNREGINTEREST", VARCHAR(255), nullable=True)

    def __repr__(self):return self.__str__()

    def __str__(self):return "<GPNTStatement(%(member_no)s)>" % self.__dict__

