import graphene
from graphene import resolve_only_args, relay
from graphene_sqlalchemy import SQLAlchemyConnectionField, SQLAlchemyObjectType
from models.apex_models import Agent as AgentModel, PensionPolicy as PensionPolicyModel, PensionAccountInfo as PensionAccountInfoModel


def connection_for_type(_type):
    class Connection(graphene.Connection):
        total_count = graphene.Int()

        class Meta:
            name = _type._meta.name + 'Connection'
            node = _type

        def resolve_total_count(self, args, context, info):
            return self.length

    return Connection


class PensionPolicies(SQLAlchemyObjectType):

    class Meta:
        model = PensionPolicyModel
        interfaces = (relay.Node, )


class Agents(SQLAlchemyObjectType):

    class Meta:
        model = AgentModel
        interfaces = (relay.Node, )


class PensionAccountInfos(SQLAlchemyObjectType):

    class Meta:
        model = PensionAccountInfoModel
        interfaces = (relay.Node, )


class Query(graphene.ObjectType):

    agents = SQLAlchemyConnectionField(Agents,  code=graphene.String())
    agent = relay.Node.Field(Agents)

    pension_policies = SQLAlchemyConnectionField(PensionPolicies,  policy_no=graphene.String())
    pension_policy = relay.Node.Field(PensionPolicies)

    pension_account_infos = SQLAlchemyConnectionField(PensionAccountInfos, policy_no=graphene.String())
    pension_account_info = relay.Node.Field(PensionAccountInfos)

    node = relay.Node.Field()
    get_pension_policy = relay.Node.Field(PensionPolicies, policy_no=graphene.String())
    get_pension_account_info = relay.Node.Field(PensionAccountInfos, policy_no=graphene.String())

    def resolve_agents(self, args, context, info):
        query = Agents.get_query(context) # SQLAlchemy query
        print(str(args))
        code = args.get('code')
        if code is None:
            return query.distinct(AgentModel.code)
        else:
            return query.filter_by(code=code).distinct(AgentModel.code)

    def resolve_pension_policies(self, args, context, info):
        query = PensionPolicies.get_query(context) # SQLAlchemy query
        print(str(args))
        policy_number = args.get('policy_no')
        if policy_number is None:
            return query.all()
        else:
            return query.filter_by(policy_no=policy_number).all()


    def resolve_pension_account_infos(self, args, context, info):
        query = PensionAccountInfos.get_query(context) # SQLAlchemy query
        print(str(args))
        policy_number = args.get('policy_no')
        if policy_number is None:
            return query.all()
        else:
            return query.filter_by(policy_no=policy_number).all()

apex_schema = graphene.Schema(query=Query, types=[Agents, PensionPolicies, PensionAccountInfos])
