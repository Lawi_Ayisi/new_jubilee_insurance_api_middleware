# -*- coding: utf-8 -*-
from flask import render_template, abort, Flask
from flask_graphql import GraphQLView
from flask_restful import Api, abort, Resource
from flask_sqlalchemy import SQLAlchemy
from webargs import fields, validate
from webargs.flaskparser import use_args, use_kwargs, parser, abort

from models.database import isf_uat_db_session, actisure_db_session, premia_db_session, apex_db_session, jhl_online_session, actisure_fail_over_db_session
from isf_schema import isf_schema
from premia_schema import premia_schema
from actisure_schema import actisure_schema
from apex_schema import apex_schema
from jhl_online_schema import jhl_online_schema
from actisure_fail_over_schema import actisure_fail_over_schema
from models.jhl_online_models import GPNTStatement

app = Flask(__name__)
app.debug = True

api = Api(app)


class GpntStatement(Resource):
    """Customer Details endpoint."""

    customer_get_args = {
        'member_no': fields.Str(required=False),
    }

    @use_kwargs(customer_get_args)
    def get(self, **kwargs):
        # Remove null values
        filtered_criterion_dict = {k: v for k, v in kwargs.items() if str(v) != '<marshmallow.missing>'}
        member_no = filtered_criterion_dict.values()[0]
        gpnt_statement = jhl_online_session.query(GPNTStatement).filter(GPNTStatement.member_no == member_no).first()
        if gpnt_statement is not None:
            registered_member_value = (gpnt_statement.n_member_op_reg or 0) + (gpnt_statement.n_member_op_reg_vol or 0) \
                                      + (gpnt_statement.n_member_total_reg or 0) + (
                                          gpnt_statement.n_member_reg_transfer or 0) \
                                      - (gpnt_statement.n_member_reg_expense or 0) + (
                                          gpnt_statement.n_member_op_reg_interest or 0) \
                                      - (gpnt_statement.n_member_reg_rba_levy or 0) + (
                                          gpnt_statement.n_member_reg_interest or 0)

            unregistered_member_value = (gpnt_statement.n_member_op_unreg or 0) + (
                gpnt_statement.n_member_op_unreg_vol or 0) + (gpnt_statement.n_member_total_unreg or 0) + (
                                            gpnt_statement.n_member_unreg_transfer or 0) - (
                                            gpnt_statement.n_member_unreg_expense or 0) + (
                                            gpnt_statement.n_member_op_unreg_interest or 0) - (
                                            gpnt_statement.n_member_unreg_rba_levy or 0) + (
                                            gpnt_statement.n_member_unreg_interest or 0) + (
                                            gpnt_statement.n_member_unreg_tax or 0)

            total_member_value = registered_member_value + unregistered_member_value

            registered_employer_value = (gpnt_statement.n_employer_op_reg or 0) + (
                gpnt_statement.n_employer_op_reg_sp or 0) + (gpnt_statement.n_employer_total_reg or 0) + (
                                            gpnt_statement.n_employer_reg_transfer or 0) - (
                                            gpnt_statement.n_employer_reg_expense or 0) + (
                                            gpnt_statement.n_employer_op_reg_interest or 0) - (
                                            gpnt_statement.n_employer_reg_rba_levy or 0) + (
                                            gpnt_statement.n_employer_reg_interest or 0)
            unregistered_employer_value = (gpnt_statement.n_employer_op_unreg or 0) + (
                gpnt_statement.n_employer_op_unreg_sp or 0) + (gpnt_statement.n_employer_total_unreg or 0) + (
                                              gpnt_statement.n_employer_unreg_transfer or 0) - (
                                              gpnt_statement.n_employer_un_reg_expense or 0) + (
                                              gpnt_statement.n_employer_op_unreg_interest or 0) - (
                                              gpnt_statement.n_employer_unreg_rba_levy or 0) + (
                                              gpnt_statement.n_employer_unreg_interest or 0)

            total_employer_value = registered_employer_value + unregistered_employer_value
            return {
                'registered_member_value': registered_member_value,
                'unregistered_member_value': unregistered_member_value,
                'registered_employer_value': registered_employer_value,
                'unregistered_employer_value': unregistered_employer_value,
                'total_employer_value': total_employer_value,
                'total_member_value': total_member_value,
            }
        else:
            return {'error': ('member  with member number %s not found', member_no)}

db = SQLAlchemy(app)
app.add_url_rule('/isf_graphql', view_func=GraphQLView.as_view('isf_graphql', schema=isf_schema, graphiql=True))
app.add_url_rule('/premia_graphql', view_func=GraphQLView.as_view('premia_graphql', schema=premia_schema, graphiql=True))
app.add_url_rule('/actisure_graphql', view_func=GraphQLView.as_view('actisure_graphql', schema=actisure_schema, graphiql=True))
app.add_url_rule('/actisure_fail_over_graphql', view_func=GraphQLView.as_view('actisure_fail_over_graphql', schema=actisure_fail_over_schema, graphiql=True))
app.add_url_rule('/apex_graphql', view_func=GraphQLView.as_view('apex_graphql', schema=apex_schema, graphiql=True))
app.add_url_rule('/gpnt_statements_graphql', view_func=GraphQLView.as_view('gpnt_statements_graphql', schema=jhl_online_schema, graphiql=True))


default_query = '''
{
  allEmployees {
    edges {
      node {
        id,
        name,
        department {
          id,
          name
        },
        role {
          id,
          name
        }
      }
    }
  }
}'''.strip()


customer_details_query = '''{
  customerDetails(policyNo: "IL201300328596") {
    edges {
      node {
        policyNo
        phoneNumber
        customerName
        email
        kraPin
        nationalId
        dateOfBirth
        customerId
      }
    }
  }
}
'''.strip()


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_server_error(e):
    return render_template('500.html'), 500


# This error handler is necessary for usage with Flask-RESTful
@parser.error_handler
def handle_request_parsing_error(err):
    """webargs error handler that uses Flask-RESTful's abort function to return
    a JSON error response to the client.
    """
    abort(422, errors=err.messages)


@app.teardown_appcontext
def shutdown_session(exception=None):
    actisure_db_session.remove()
    actisure_fail_over_db_session.remove()
    isf_uat_db_session.remove()
    premia_db_session.remove()
    apex_db_session.remove()
    jhl_online_session.remove()


if __name__ == "__main__":
    api.add_resource(GpntStatement, '/gpnt_statements')
    app.run(host='0.0.0.0', port=5000, threaded=True)
