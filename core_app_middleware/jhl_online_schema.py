import graphene
from graphene import resolve_only_args, relay
from graphene_sqlalchemy import SQLAlchemyConnectionField, SQLAlchemyObjectType
from models.jhl_online_models import GPNTStatement as GPNTStatementModel


def connection_for_type(_type):
    class Connection(graphene.Connection):
        total_count = graphene.Int()

        class Meta:
            name = _type._meta.name + 'Connection'
            node = _type

        def resolve_total_count(self, args, context, info):
            return self.length

    return Connection


class GPNTStatements(SQLAlchemyObjectType):

    class Meta:
        model = GPNTStatementModel
        interfaces = (relay.Node, )


class Query(graphene.ObjectType):
    gpnt_statements = SQLAlchemyConnectionField(GPNTStatements,  member_no=graphene.String())
    gpnt_statement = relay.Node.Field(GPNTStatements)

    node = relay.Node.Field()
    get_gpnt_statement = relay.Node.Field(GPNTStatements, member_no=graphene.String())

    def resolve_gpnt_statements(self, args, context, info):
        query = GPNTStatements.get_query(context) # SQLAlchemy query
        member_number = args.get('member_no')
        if member_number is None:
            return query.all()
        else:
            gpnt_statement = query.filter_by(member_no=member_number).first()

            if gpnt_statement is not None:
                registered_member_value = (gpnt_statement.n_member_op_reg or 0) + (gpnt_statement.n_member_op_reg_vol or 0) \
                                          + (gpnt_statement.n_member_total_reg or 0) + (
                                          gpnt_statement.n_member_reg_transfer or 0) \
                                          - (gpnt_statement.n_member_reg_expense or 0) + (
                                          gpnt_statement.n_member_op_reg_interest or 0) \
                                          - (gpnt_statement.n_member_reg_rba_levy or 0) + (
                                          gpnt_statement.n_member_reg_interest or 0)

                unregistered_member_value = (gpnt_statement.n_member_op_unreg or 0) + (
                gpnt_statement.n_member_op_unreg_vol or 0) + (gpnt_statement.n_member_total_unreg or 0) + (
                                            gpnt_statement.n_member_unreg_transfer or 0) - (
                                            gpnt_statement.n_member_unreg_expense or 0) + (
                                            gpnt_statement.n_member_op_unreg_interest or 0) - (
                                            gpnt_statement.n_member_unreg_rba_levy or 0) + (
                                            gpnt_statement.n_member_unreg_interest or 0) + (
                                            gpnt_statement.n_member_unreg_tax or 0)

                total_member_value = registered_member_value + unregistered_member_value

                registered_employer_value = (gpnt_statement.n_employer_op_reg or 0) + (
                gpnt_statement.n_employer_op_reg_sp or 0) + (gpnt_statement.n_employer_total_reg or 0) + (
                                            gpnt_statement.n_employer_reg_transfer or 0) - (
                                            gpnt_statement.n_employer_reg_expense or 0) + (
                                            gpnt_statement.n_employer_op_reg_interest or 0) - (
                                            gpnt_statement.n_employer_reg_rba_levy or 0) + (
                                            gpnt_statement.n_employer_reg_interest or 0)
                unregistered_employer_value = (gpnt_statement.n_employer_op_unreg or 0) + (
                gpnt_statement.n_employer_op_unreg_sp or 0) + (gpnt_statement.n_employer_total_unreg or 0) + (
                                              gpnt_statement.n_employer_unreg_transfer or 0) - (
                                              gpnt_statement.n_employer_un_reg_expense or 0) + (
                                              gpnt_statement.n_employer_op_unreg_interest or 0) - (
                                              gpnt_statement.n_employer_unreg_rba_levy or 0) + (
                                              gpnt_statement.n_employer_unreg_interest or 0)

                total_employer_value = registered_employer_value + unregistered_employer_value
                return {
                    'registered_member_value': registered_member_value,
                    'unregistered_member_value': unregistered_member_value,
                    'registered_employer_value': registered_employer_value,
                    'unregistered_employer_value': unregistered_employer_value,
                    'total_employer_value': total_employer_value,
                    'total_member_value': total_member_value,
                }
            else:
                return {'error': ('member  with member number %s not found', member_number)}
jhl_online_schema = graphene.Schema(query=Query, types=[GPNTStatements])
