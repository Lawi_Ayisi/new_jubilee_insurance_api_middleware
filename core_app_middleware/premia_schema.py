import graphene
from graphene import resolve_only_args, relay
from graphene_sqlalchemy import SQLAlchemyConnectionField, SQLAlchemyObjectType
from models.premia_models import CustomerDetails as CustomerDetailsModel, RiskDetails as RiskDetailsModel, PolicyDetails as PolicyDetailsModel


def connection_for_type(_type):
    class Connection(graphene.Connection):
        total_count = graphene.Int()

        class Meta:
            name = _type._meta.name + 'Connection'
            node = _type

        def resolve_total_count(self, args, context, info):
            return self.length

    return Connection


class CustomerDetails(SQLAlchemyObjectType):

    class Meta:
        model = CustomerDetailsModel
        interfaces = (relay.Node, )


class RiskDetails(SQLAlchemyObjectType):

    class Meta:
        model = RiskDetailsModel
        interfaces = (relay.Node, )


class PolicyDetails(SQLAlchemyObjectType):
    class Meta:
        model = PolicyDetailsModel
        interfaces = (relay.Node,)


class Query(graphene.ObjectType):
    customer_details = SQLAlchemyConnectionField(CustomerDetails,  vehicle_reg_no=graphene.String())
    customer_detail = relay.Node.Field(CustomerDetails)

    risk_details = SQLAlchemyConnectionField(RiskDetails,
                                             vehicle_reg_no=graphene.String(),
                                             policy_no=graphene.String(),
                                             pol_sys_id=graphene.String(),
                                             veh_sys_id=graphene.String())

    risk_detail = relay.Node.Field(RiskDetails)

    policy_details = SQLAlchemyConnectionField(PolicyDetails, policy_no=graphene.String())
    policy_detail = relay.Node.Field(PolicyDetails)

    node = relay.Node.Field()
    get_customer_detail = relay.Node.Field(CustomerDetails, vehicle_reg_no=graphene.String())
    get_risk_detail = relay.Node.Field(RiskDetails, vehicle_reg_no=graphene.String())
    get_policy_detail = relay.Node.Field(PolicyDetails, policy_no=graphene.String())

    def resolve_customer_details(self, args, context, info):
        query = CustomerDetails.get_query(context) # SQLAlchemy query
        print(str(args))
        vehicle_number_plate = args.get('vehicle_reg_no')
        if vehicle_number_plate is None:
            return query.all()
        else:
            return query.filter_by(vehicle_reg_no=vehicle_number_plate).all()

    def resolve_policy_details(self, args, context, info):
        query = PolicyDetails.get_query(context) # SQLAlchemy query
        print(str(args))
        policy_number = args.get('policy_no')
        if policy_number is None:
            return query.all()
        else:
            return query.filter_by(policy_no=policy_number).all()

    def resolve_risk_details(self, args, context, info):
        query = RiskDetails.get_query(context) # SQLAlchemy query
        print(str(args))

        vehicle_reg_no = args.get('vehicle_reg_no')
        policy_no = args.get('policy_no')
        pol_sys_id = args.get('pol_sys_id')
        veh_sys_id = args.get('veh_sys_id')

        if vehicle_reg_no is not None:
            return query.filter_by(vehicle_reg_no=vehicle_reg_no).all()
        elif policy_no is not None:
            return query.filter_by(policy_no=policy_no).all()
        elif pol_sys_id is not None:
            return query.filter_by(pol_sys_id=pol_sys_id).all()
        elif veh_sys_id is not None:
            return query.filter_by(veh_sys_id=veh_sys_id).all()
        else:
            return query.all()

premia_schema = graphene.Schema(query=Query, types=[CustomerDetails, RiskDetails, PolicyDetails])
