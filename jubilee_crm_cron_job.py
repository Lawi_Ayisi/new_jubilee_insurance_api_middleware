#!/usr/bin/env python
from __future__ import print_function
import pymysql
from lxml import html
import requests

#always append  'crm_regn_ctry_name' to columns_names

#
# def convertAllAccountNamesToUpperCase():
#     mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')
#     mysqlcur = mysqlconn.cursor()
#     update_mysqlcur = mysqlconn.cursor()
#     mysqlcur.execute("SELECT id, name FROM accounts ")
#     print("Selecting names from accounts")
#     accounts_count = 0
#     for row in mysqlcur:
#         id = row[0]
#         name = row[1]
#
#             upper_name = str(name).upper()
#             print("id is " + id + " " +name + " is now " + upper_name)
#
#             #update_mysqlcur.execute("""UPDATE accounts SET name=%s WHERE id=%s """, (upper_name, id))
#             #update_mysqlcur.connection.commit()
#             accounts_count += 1
#     print("All accounts is " + str(accounts_count))
#     update_mysqlcur.close()
#     mysqlcur.close()
#     mysqlconn.close()


def convertHTMLDescriptionToPlainText():
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')
    mysqlcur = mysqlconn.cursor()
    update_mysqlcur = mysqlconn.cursor()
    mysqlcur.execute("SELECT id, description FROM cases WHERE description like '<p%' or description like '<tab%' or description like '<ul%'")
    print("Selecting description from cases")
    cases_count = 0
    for row in mysqlcur:
        id = row[0]
        html_description_body = row[1]
        doc = doc = html.fromstring(html_description_body)
        description_without_html_tags = doc.text_content()
        print("id is " + id + " " +html_description_body + " is now " + description_without_html_tags)

        update_mysqlcur.execute("""UPDATE cases SET description=%s WHERE id=%s """, (description_without_html_tags, id))
        update_mysqlcur.connection.commit()
        # cases_count += 1
        # a_list = []
        # for columns_name in columns_names[:-1] :
        #     a_list.append(row[columns_name])
        # a_list.append('kenya')
        # a_list = tuple(a_list)
        #
        # mysqlcur.execute(insert_query, a_list)
        # mysqlcur.connection.commit()
        # print("Inserting "+table_name+ " count  " + str(cases_count))
    update_mysqlcur.close()
    mysqlcur.close()
    mysqlconn.close()


def lookForPolicyAgentDetailsFromISFAndUpdate():
    mysqlconn = pymysql.connect(host='192.168.52.42', port=3306, user='wasambo', passwd='scrmdbpass', db='jubilee_suitecrm_db', charset='utf8')
    mysqlcur = mysqlconn.cursor()
    update_mysqlcur = mysqlconn.cursor()
    mysqlcur.execute("SELECT id_c, policy_no_c FROM cases_cstm WHERE policy_no_c like 'IL%' and agent_code_c IS NULL ")
    print("Selecting life cases without agent numbers from cases_cstm")
    cases_count = 0
    for row in mysqlcur:
        id_c = row[0]
        policy_no_c = row[1]

        try:
            #make a POST request to ISF Micro Service and fetch data
            dictToSend = {"query":"query ($policyNo: String!){customerDetails (policyNo: $policyNo) { edges { node   {  policyNo agentCode  agentNo agentName } }}}","variables":{"policyNo":policy_no_c}}
            print(str(dictToSend))
            res = requests.post('http://192.168.50.27:5000/graphql', json=dictToSend)
            dictFromServer = res.json()['data']['customerDetails']['edges'][0]['node']
            agent_code = dictFromServer['agentCode']
            agent_no = dictFromServer['agentNo']
            agent_name = dictFromServer['agentName']

            print("id is " + id_c + "  Policy Number is " + policy_no_c +  "  Agent Code is now " + agent_code +"  agent Number is now " + agent_no + "  agent name is  now " + agent_name)

            update_mysqlcur.execute("""UPDATE cases_cstm SET agent_code_c=%s, agent_number_c=%s, agent_name_c=%s  WHERE id_c=%s """, (agent_code,agent_no, agent_name, id_c))
            update_mysqlcur.connection.commit()
        except Exception:
            pass

    update_mysqlcur.close()
    mysqlcur.close()
    mysqlconn.close()


convertHTMLDescriptionToPlainText()
#convertAllAccountNamesToUpperCase()
#lookForPolicyAgentDetailsFromISFAndUpdate()