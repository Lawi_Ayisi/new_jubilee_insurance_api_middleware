FROM python:2.7.10

ENV ORACLE_HOME /opt/oracle/instantclient_12_1
ENV LD_RUN_PATH=$ORACLE_HOME

RUN apt-get update
RUN apt-get install -y g++ make software-properties-common --force-yes
# Install unzip
RUN apt-get install -y unzip
RUN apt-get install -y libaio1

COPY instantclient/* /tmp/

RUN \
	mkdir -p /opt/oracle && \
	unzip "/tmp/instantclient*.zip" -d /opt/oracle && \
	ln -s $ORACLE_HOME/libclntsh.so.12.1 $ORACLE_HOME/libclntsh.so

COPY start.sh /
COPY requirements.txt /

COPY . /jubilee_core_apps_api_middleware/
WORKDIR /jubilee_core_apps_api_middleware/

RUN pip install --upgrade pip
RUN pip install cx_Oracle==5.3.0
RUN  pip install -r requirements.txt

ENTRYPOINT ./start.sh
