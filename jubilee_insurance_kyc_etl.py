#!/usr/bin/env python
import json, random, uuid, requests, csv, re, sys
from datetime import datetime
import dateutil.parser
from dateutil.relativedelta import relativedelta
from sqlalchemy import or_, not_, func, exc, distinct

from core_app_middleware.models.actisure_prod_models import MedicalPolicy as ProdMedicalPolicy, JCAREMedicalPolicy as ProdJCAREMedicalPolicy,  MedicalInformation as ProdMedicalInformation, Agent as ProdMedicalAgent

from core_app_middleware.models.actisure_models import MedicalPolicy, JCAREMedicalPolicy, MedicalInformation, Agent as MedicalAgent
from core_app_middleware.models.actisure_fail_over_models import CustomerDetails as FailOverCustomerDetails, MedicalInformation as FailOverMedicalInformation ,Agent as FailOverMedicalAgent
from core_app_middleware.models.apex_models import PensionPolicy, PensionAccountInfo
from core_app_middleware.models.database import kyc_db_session, suite_crm_db_session, apex_db_session, premia_db_session, online_bima_session, isf_uat_db_session, ilanga_session, jhl_online_session, actisure_db_session, actisure_fail_over_db_session, actisure_production_db_session
from core_app_middleware.models.isf_models import LifePolicy, REMTReceipt, BulkSMS, PremiumStatement
from core_app_middleware.models.jubilee_insure_models import Product, Customer as JubileeInsureCustomer
from core_app_middleware.models.kyc_models import Customer as KYCCustomer, CustomerDetails as KYCCustomerDetails, Policy as KYCPolicy, PaymentDetails as KYCPaymentDetails
from core_app_middleware.models.premia_models import GeneralPolicy, PolicyDetails, RiskDetails, CustomerDetails
from core_app_middleware.models.suite_crm_models import Account, AccountCSTM, Communications, CommunicationsAccount, Payments, PaymentAccount, TempPayments
from core_app_middleware.models.online_bima_models import ClientFeedback, CampaignADHOC, SMSProxy
from core_app_middleware.models.jhl_online_models import GPNTStatement


def load_jubilee_insure_customers():
    customers = JubileeInsureCustomer.query.all()
    url = 'http://192.168.50.27:2300/customer'
    headers = {'content-type': 'application/json'}

    '''
        id = Column("cust_id", VARCHAR(255), primary_key=True, nullable=False)
        title = Column("title", VARCHAR(255), nullable=True)
        name = Column("surname", VARCHAR(255), nullable=True)
        gender = Column("gender", VARCHAR(255), nullable=True)
        date_of_birth = Column("dob", VARCHAR(255), nullable=True)
        marital_status = Column("marital_status", VARCHAR(255), nullable=True)
        id_no = Column("id_no", VARCHAR(255), nullable=True)
        passport_no = Column("passport_no", VARCHAR(255), nullable=True)
        income_tax_number = Column("kra_pin", VARCHAR(255), nullable=True)
        email_addr = Column("email_addr", VARCHAR(255), nullable=True)
        phone_number = Column("tel_no", VARCHAR(255), nullable=True)
    '''

    for customer in customers:
        payload = {}

        print (str(customer.name).upper())
        # if life_policy.email_address_isf is not None: payload['isf_email'] = str(life_policy.email_address_isf).lower()
        # if life_policy.customer_id_isf is not None: payload['isf_id'] = str(life_policy.customer_id_isf)
        # if life_policy.income_tax_no is not None: payload['income_tax_number'] = str(life_policy.income_tax_no)
        # if life_policy.national_id_no is not None: payload['national_id_no'] = str(life_policy.national_id_no)
        # if life_policy.passport_no is not None: payload['passport_no'] = str(life_policy.passport_no)
        # if life_policy.phone_no_isf is not None: payload['isf_phone_no'] = str(life_policy.phone_no_isf)
        #
        #
        # print ("payload is "+ str(payload))
        # response = requests.post(url, data=json.dumps(payload), headers=headers)
        # if response.status_code == 200:
        #     print ('record posted success fully ')
        # else:
        #     print ('error while posting record ' + response.text)


def load_jubilee_insure_products():
    products = Product.query.all()
    url = 'http://192.168.50.27:2300/customer'
    headers = {'content-type': 'application/json'}
    for product in products:
        payload = {}
        print (str(product))
        # if life_policy.email_address_isf is not None: payload['isf_email'] = str(life_policy.email_address_isf).lower()
        # if life_policy.customer_id_isf is not None: payload['isf_id'] = str(life_policy.customer_id_isf)
        # if life_policy.income_tax_no is not None: payload['income_tax_number'] = str(life_policy.income_tax_no)
        # if life_policy.national_id_no is not None: payload['national_id_no'] = str(life_policy.national_id_no)
        # if life_policy.passport_no is not None: payload['passport_no'] = str(life_policy.passport_no)
        # if life_policy.phone_no_isf is not None: payload['isf_phone_no'] = str(life_policy.phone_no_isf)
        #
        #
        # print ("payload is "+ str(payload))
        # response = requests.post(url, data=json.dumps(payload), headers=headers)
        # if response.status_code == 200:
        #     print ('record posted success fully ')
        # else:
        #     print ('error while posting record ' + response.text)



def load_isf_customer_data_into_kyc_db():
    life_policies = LifePolicy.query.all()
    url = 'http://192.168.50.27:2300/customer'
    headers = {'content-type': 'application/json'}
    for life_policy in life_policies:
        payload = {}

        if life_policy.email_address_isf is not None: payload['isf_email'] = str(life_policy.email_address_isf).lower()
        if life_policy.customer_id_isf is not None: payload['isf_id'] = str(life_policy.customer_id_isf)
        if life_policy.income_tax_no is not None: payload['income_tax_number'] = str(life_policy.income_tax_no)
        if life_policy.national_id_no is not None: payload['national_id_no'] = str(life_policy.national_id_no)
        if life_policy.passport_no is not None: payload['passport_no'] = str(life_policy.passport_no)
        if life_policy.phone_no_isf is not None: payload['isf_phone_no'] = str(life_policy.phone_no_isf)


        print ("payload is "+ str(payload))
        # response = requests.post(url, data=json.dumps(payload), headers=headers)
        # if response.status_code == 200:
        #     print ('record posted success fully ')
        # else:
        #     print ('error while posting record ' + response.text)


def load_premia_customer_kra_pin_into_kyc_db():
    general_policies = GeneralPolicy.query.filter(GeneralPolicy.income_tax_no.like("A00%")).distinct(GeneralPolicy.income_tax_no)
    url = 'http://192.168.50.27:2300/customer'
    headers = {'content-type': 'application/json'}
    count = 0
    for general_policy in general_policies:
        payload = {}

        pin_number = str(general_policy.income_tax_no).replace(" ", "")[:11]
        if pin_number.__len__() == 11:
            if str(pin_number[10]).isalpha():
                if general_policy.customer_id_premia is not None: payload['premia_id'] = str(general_policy.customer_id_premia)
                if general_policy.income_tax_no is not None: payload['income_tax_number'] = str(general_policy.income_tax_no).replace(" ", "")[:11]
                count +=1
                print ("payload is " + str(payload))
                response = requests.post(url, data=json.dumps(payload), headers=headers)
                if response.status_code == 200:
                    print ('record posted success fully ')
                else:
                    print ('error while posting record ' + response.text)
    print ("count is " + str(count))


def load_premia_customer_landline_phone_number_into_kyc_db():

    general_policies = GeneralPolicy.query.filter(or_(GeneralPolicy.phone_no_premia.like("01%"),
                                            GeneralPolicy.phone_no_premia.like("02%"),
                                            GeneralPolicy.phone_no_premia.like("03%"),
                                            GeneralPolicy.phone_no_premia.like("04%"),
                                            GeneralPolicy.phone_no_premia.like("05%"),
                                            GeneralPolicy.phone_no_premia.like("06%"),
                                            GeneralPolicy.phone_no_premia.like("08%"))).distinct(GeneralPolicy.phone_no_premia)


    url = 'http://192.168.50.27:2300/customer'
    headers = {'content-type': 'application/json'}
    count = 0
    for general_policy in general_policies:
        payload = {}

        phone_no_premia = str(general_policy.phone_no_premia).replace(" ", "").replace("-", "")
        #     if str(pin_number[10]).isalpha():
        if general_policy.customer_id_premia is not None: payload['premia_id'] = str(general_policy.customer_id_premia)
        if general_policy.phone_no_premia is not None: payload['premia_phone_no'] = phone_no_premia
        count +=1
        print ("payload is " + str(payload))
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        if response.status_code == 200:
            print ('record posted success fully ')
        else:
            print ('error while posting record ' + response.text)
    print ("count is " + str(count))


def load_premia_customer_id_numbers_into_kyc_db():

    general_policies = GeneralPolicy.query.filter(GeneralPolicy.national_id_no.isnot(None)) \
                                            .filter(func.char_length(GeneralPolicy.national_id_no) == 8) \
                                            .filter(not_(GeneralPolicy.national_id_no.like("%A%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%B%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%C%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%D%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%E%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%F%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%G%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%H%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%I%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%J%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%K%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%L%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%M%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%N%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%O%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%P%"))) \
                                            .filter(not_(GeneralPolicy.national_id_no.like("%Q%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%R%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%S%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%T%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%U%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%V%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%W%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%X%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%Y%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%Z%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%.%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%-%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%R%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%G%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("%K%")))\
                                            .filter(not_(GeneralPolicy.national_id_no.like("07%")))\
                                            .distinct(GeneralPolicy.national_id_no)

    url = 'http://192.168.50.27:2300/customer'
    headers = {'content-type': 'application/json'}
    count = 0
    for general_policy in general_policies:
        payload = {}
        #     if str(pin_number[10]).isalpha():
        if general_policy.customer_id_premia is not None: payload['premia_id'] = str(general_policy.customer_id_premia)
        if general_policy.national_id_no is not None: payload['national_id_no'] = str(general_policy.national_id_no)
        count += 1
        print ("payload is " + str(payload))
        print ("count is " + str(count))
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        if response.status_code == 200:
            print ('record posted success fully ')
        else:
            print ('error while posting record ' + response.text)
    print ("total count is " + str(count))


#
# def load_premia_customer_phone_number_into_kyc_db():
#     #general_policies = GeneralPolicy.query.filter(GeneralPolicy.phone_no_premia.like("A00%")).distinct(GeneralPolicy.phone_no_premia)
#     general_policies = GeneralPolicy.query.filter(GeneralPolicy.phone_no_premia.like("07%")).distinct(GeneralPolicy.phone_no_premia)
#     url = 'http://192.168.50.27:2300/customer'
#     headers = {'content-type': 'application/json'}
#     count = 0
#     for general_policy in general_policies:
#         payload = {}
#
#         phone_no_premia = str(general_policy.phone_no_premia).replace(" ", "").replace("-", "")[:11]
#         if phone_no_premia.__len__() == 10:
#             #     if str(pin_number[10]).isalpha():
#             if general_policy.customer_id_premia is not None: payload['premia_id'] = str(general_policy.customer_id_premia)
#             if general_policy.phone_no_premia is not None: payload['premia_phone_no'] = phone_no_premia
#             count +=1
#             print ("payload is " + str(payload))
#             response = requests.post(url, data=json.dumps(payload), headers=headers)
#             if response.status_code == 200:
#                 print ('record posted success fully ')
#             else:
#                 print ('error while posting record ' + response.text)
#     print ("count is " + str(count))
#

# def load_premia_customer_data_into_kyc_db():
#     #general_policies = GeneralPolicy.query.filter(GeneralPolicy.message.like("%somestr%")).all()
#     general_policies = GeneralPolicy.query.filter(GeneralPolicy.income_tax_no.like("A00%")).distinct(GeneralPolicy.income_tax_no)
#     url = 'http://192.168.50.27:2300/customer'
#     headers = {'content-type': 'application/json'}
#     count = 0
#     for general_policy in general_policies:
#         payload = {}
#
#         #LENGTH(ASSR_PHONE) = 10
#         pin_number = str(general_policy.income_tax_no).replace(" ", "")[:11]
#         if pin_number.__len__() == 11:
#             if general_policy.customer_id_premia is not None: payload['premia_id'] = str(general_policy.customer_id_premia)
#             if general_policy.income_tax_no is not None: payload['income_tax_number'] = str(general_policy.income_tax_no).replace(" ", "")[:11]
#             #if general_policy.national_id_no is not None: payload['national_id_no'] = str(general_policy.national_id_no)
#             #if general_policy.phone_no_premia is not None: payload['premia_phone_no'] = str(general_policy.phone_no_premia)
#             count+=1
#             print ("payload is " + str(payload))
#             # response = requests.post(url, data=json.dumps(payload), headers=headers)
#             # if response.status_code == 200:
#             #     print ('record posted success fully ')
#             # else:
#             #     print ('error while posting record ' + response.text)
#     print ("count is " + str(count))


def load_actisure_customer_data_into_kyc_db():
    medical_policies = MedicalPolicy.query.filter(MedicalPolicy.phone_no_actisure.like("25407%")).all()
    url = 'http://192.168.50.27:2300/customer'
    headers = {'content-type': 'application/json'}
    for medical_policy in medical_policies:
        payload = {}
        print medical_policy.policy_effective_date
        if medical_policy.email_address_actisure is not None: payload['actisure_email'] = str(medical_policy.email_address_actisure).lower()
        if medical_policy.customer_id_actisure is not None: payload['actisure_id'] = str(medical_policy.customer_id_actisure)
        #if medical_policy.income_tax_no is not None: payload['income_tax_number'] = str(medical_policy.income_tax_no)
        #if medical_policy.national_id_no is not None: payload['national_id_no'] = str(medical_policy.national_id_no)
        #if medical_policy.passport_no is not None: payload['passport_no'] = str(medical_policy.passport_no)
        phone_no_actisure = str(medical_policy.phone_no_actisure)
        if medical_policy.phone_no_actisure is not None: payload['actisure_phone_no'] = phone_no_actisure[3:]

        print ("payload is "+ str(payload))
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        if response.status_code == 200:
            print ('record posted success fully ')
        else:
            print ('error while posting record ' + response.text)


def load_actisure_failover_customer_data_into_kyc_db():
    medical_policies = FailOverCustomerDetails.query.filter(FailOverCustomerDetails.phone_no_actisure.like("25407%")).all()
    url = 'http://192.168.50.27:2300/customer'
    headers = {'content-type': 'application/json'}
    for medical_policy in medical_policies:
        payload = {}
        #print medical_policy.email_address_actisure
        if medical_policy.email_address_actisure is not None and bool(re.search(r"^[\w\.\+\-]+\@[\w]+\.[a-z]{2,3}$", medical_policy.email_address_actisure)): payload['actisure_email'] = str(medical_policy.email_address_actisure).lower()
        if medical_policy.customer_id_actisure is not None: payload['actisure_id'] = str(medical_policy.customer_id_actisure)
        if medical_policy.income_tax_no is not None and str(medical_policy.income_tax_no).__len__() == 11 and str(medical_policy.income_tax_no)[:3]=='A00': payload['income_tax_number'] = str(medical_policy.income_tax_no)
        if medical_policy.national_id_no is not None and str(medical_policy.national_id_no).__len__() == 8 and not re.search('[a-zA-Z]', str(medical_policy.national_id_no)): payload['national_id_no'] = str(medical_policy.national_id_no)
        #
        #     if not re.search('[a-zA-Z]', str(medical_policy.national_id_no)):
        #         payload['national_id_no'] = str(medical_policy.national_id_no)
        #         #if medical_policy.passport_no is not None: payload['passport_no'] = str(medical_policy.passport_no)
        #         phone_no_actisure = str(medical_policy.phone_no_actisure)
        #         if medical_policy.phone_no_actisure is not None: payload['actisure_phone_no'] = phone_no_actisure[7:]
        #
        #         print ("payload is " + str(payload))
                # response = requests.post(url, data=json.dumps(payload), headers=headers)
                # if response.status_code == 200:
                #     print ('record posted success fully ')
                # else:
                #     print ('error while posting record ' + response.text)

            # if medical_policy.passport_no is not None: payload[
        # 'passport_no'] = str(medical_policy.passport_no)
        phone_no_actisure = str(medical_policy.phone_no_actisure)
        if medical_policy.phone_no_actisure is not None: payload['actisure_phone_no'] = '0'+ phone_no_actisure[4:]
        #
        print ("payload is " + str(payload))
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        if response.status_code == 200:
            print ('record posted success fully ')
        else:
            print ('error while posting record ' + response.text)

def load_jcare_actisure_customer_data_into_kyc_db():
    medical_policies = JCAREMedicalPolicy.query.filter(JCAREMedicalPolicy.phone_no_actisure.like("254010007%")).all()
    url = 'http://192.168.50.27:2300/customer'
    headers = {'content-type': 'application/json'}
    for medical_policy in medical_policies:
        payload = {}
        print medical_policy.policy_effective_date
        if medical_policy.email_address_actisure is not None: payload['actisure_email'] = str(
            medical_policy.email_address_actisure).lower()
        if medical_policy.customer_id_actisure is not None: payload['actisure_id'] = str(
            medical_policy.customer_id_actisure)
        # if medical_policy.income_tax_no is not None: payload['income_tax_number'] = str(medical_policy.income_tax_no)
        # if medical_policy.national_id_no is not None: payload['national_id_no'] = str(medical_policy.national_id_no)
        # if medical_policy.passport_no is not None: payload['passport_no'] = str(medical_policy.passport_no)
        phone_no_actisure = str(medical_policy.phone_no_actisure)
        if medical_policy.phone_no_actisure is not None: payload['actisure_phone_no'] = '0' + str(phone_no_actisure)

        print ("payload is " + str(payload))
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        if response.status_code == 200:
            print ('record posted success fully ')
        else:
            print ('error while posting record ' + response.text)


#41544 of 41545 is 100.000000 % complete
#40545 of 50000 is 52.730000 % complete

# def load_apex_customer_data_into_kyc_db():
#     apex_policies = PensionPolicy.query.all()
#     url = 'http://192.168.50.27:2300/customer'
#     headers = {'content-type': 'application/json'}
#     for apex_policy in apex_policies:
#         payload = {}
#
#         if apex_policy.email_address_apex is not None: payload['apex_email'] = str(apex_policy.email_address_apex).lower()
#         if apex_policy.customer_id_apex is not None: payload['apex_id'] = str(apex_policy.customer_id_apex)
#         if apex_policy.income_tax_no is not None: payload['income_tax_number'] = str(apex_policy.income_tax_no)
#         # if apex_policy.national_id_no is not None: payload['national_id_no'] = str(apex_policy.national_id_no)
#         # if apex_policy.passport_no is not None: payload['passport_no'] = str(apex_policy.passport_no)
#         if apex_policy.phone_no_apex is not None: payload['apex_phone_no'] = str(apex_policy.phone_no_apex)
#
#         print ("payload is "+ str(payload))
#         # response = requests.post(url, data=json.dumps(payload), headers=headers)
#         # if response.status_code == 200:
#         #     print ('record posted success fully ')
#         # else:
#         #     print ('error while posting record ' + response.text)
#


def load_apex_customer_phone_data_into_kyc_db():
    apex_policies = PensionPolicy.query.all()
    url = 'http://192.168.50.27:2300/customer'
    headers = {'content-type': 'application/json'}
    count = 0
    for apex_policy in apex_policies:
        payload = {}

        phone_no_apex = str(apex_policy.phone_no_apex).replace(" ", "").replace("-", "")[:11]
        if phone_no_apex.__len__() == 10:
            #     if str(pin_number[10]).isalpha():
            count += 1

            #if apex_policy.email_address_apex is not None: payload['apex_email'] = str(apex_policy.email_address_apex).lower()
            if apex_policy.customer_id_apex is not None: payload['apex_id'] = str(apex_policy.customer_id_apex)
            #if apex_policy.income_tax_no is not None: payload['income_tax_number'] = str(apex_policy.income_tax_no)
            # if apex_policy.national_id_no is not None: payload['national_id_no'] = str(apex_policy.national_id_no)
            # if apex_policy.passport_no is not None: payload['passport_no'] = str(apex_policy.passport_no)
            if apex_policy.phone_no_apex is not None: payload['apex_phone_no'] = str(phone_no_apex)

            print ("payload is " + str(payload))
            response = requests.post(url, data=json.dumps(payload), headers=headers)
            if response.status_code == 200:
                print ('record posted success fully ')
            else:
                print ('error while posting record ' + response.text)

    print ("Total count is " + str(count))


def load_apex_customer_email_data_into_kyc_db():
    apex_policies = PensionPolicy.query.filter(PensionPolicy.email_address_apex.like("%@%")).all()
    url = 'http://192.168.50.27:2300/customer'
    headers = {'content-type': 'application/json'}
    count = 0
    for apex_policy in apex_policies:
        payload = {}

        apex_email = str(apex_policy.email_address_apex).replace(" ", "").lower()
        #     if str(pin_number[10]).isalpha():
        count += 1

        #if apex_policy.email_address_apex is not None: payload['apex_email'] = str(apex_policy.email_address_apex).lower()
        if apex_policy.customer_id_apex is not None: payload['apex_id'] = str(apex_policy.customer_id_apex)
        #if apex_policy.income_tax_no is not None: payload['income_tax_number'] = str(apex_policy.income_tax_no)
        # if apex_policy.national_id_no is not None: payload['national_id_no'] = str(apex_policy.national_id_no)
        # if apex_policy.passport_no is not None: payload['passport_no'] = str(apex_policy.passport_no)
        if apex_policy.email_address_apex is not None: payload['apex_email'] = str(apex_email)

        print ("payload is " + str(payload))
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        if response.status_code == 200:
            print ('record posted success fully ')
        else:
            print ('error while posting record ' + response.text)

    print ("Total email count is " + str(count))


def load_apex_customer_income_tax_number_into_kyc_db():
    apex_policies = PensionPolicy.query.filter(PensionPolicy.income_tax_no.like("A00%")).all()
    url = 'http://192.168.50.27:2300/customer'
    headers = {'content-type': 'application/json'}
    count = 0
    for apex_policy in apex_policies:
        payload = {}

        income_tax_no = str(apex_policy.income_tax_no).replace(" ", "")
        #     if str(pin_number[10]).isalpha():
        count += 1

        #if apex_policy.email_address_apex is not None: payload['apex_email'] = str(apex_policy.email_address_apex).lower()
        if apex_policy.customer_id_apex is not None: payload['apex_id'] = str(apex_policy.customer_id_apex)
        #if apex_policy.income_tax_no is not None: payload['income_tax_number'] = str(apex_policy.income_tax_no)
        # if apex_policy.national_id_no is not None: payload['national_id_no'] = str(apex_policy.national_id_no)
        # if apex_policy.passport_no is not None: payload['passport_no'] = str(apex_policy.passport_no)
        if apex_policy.income_tax_no is not None: payload['income_tax_number'] = str(income_tax_no)

        print ("payload is " + str(payload))
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        if response.status_code == 200:
            print ('record posted success fully ')
        else:
            print ('error while posting record ' + response.text)

    print ("Total email count is " + str(count))


def load_apex_customer_id_numbers_into_kyc_db():

    apex_policies = PensionPolicy.query.filter(PensionPolicy.national_id_no.isnot(None)) \
                                            .filter(func.char_length(PensionPolicy.national_id_no) == 8) \
                                            .filter(not_(PensionPolicy.national_id_no.like("%A%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%B%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%C%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%D%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%E%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%F%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%G%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%H%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%I%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%J%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%K%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%L%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%M%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%N%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%O%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%P%"))) \
                                            .filter(not_(PensionPolicy.national_id_no.like("%Q%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%R%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%S%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%T%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%U%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%V%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%W%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%X%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%Y%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%Z%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%.%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%-%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%R%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%G%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("%K%")))\
                                            .filter(not_(PensionPolicy.national_id_no.like("07%")))\
                                            .distinct(PensionPolicy.national_id_no)

    url = 'http://192.168.50.27:2300/customer'
    headers = {'content-type': 'application/json'}
    count = 0
    for apex_policy in apex_policies:
        payload = {}
        #     if str(pin_number[10]).isalpha():
        if apex_policy.customer_id_apex is not None: payload['apex_id'] = str(apex_policy.customer_id_apex)
        if apex_policy.national_id_no is not None: payload['national_id_no'] = str(apex_policy.national_id_no)
        count += 1
        print ("payload is " + str(payload))
        print ("count is " + str(count))
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        if response.status_code == 200:
            print ('record posted success fully ')
        else:
            print ('error while posting record ' + response.text)
    print ("total count is " + str(count))


def get_number_of_actisure_numbers():
    return MedicalPolicy.query.filter(MedicalPolicy.national_id_no.isnot(None)) \
                                            .filter(func.char_length(MedicalPolicy.national_id_no) == 8) \
                                            .filter(not_(MedicalPolicy.national_id_no.like("%A%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%B%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%C%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%D%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%E%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%F%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%G%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%H%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%I%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%J%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%K%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%L%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%M%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%N%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%O%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%P%"))) \
                                            .filter(not_(MedicalPolicy.national_id_no.like("%Q%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%R%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%S%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%T%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%U%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%V%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%W%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%X%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%Y%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%Z%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%.%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%-%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%R%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%G%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%K%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("07%")))\
                                            .distinct(MedicalPolicy.national_id_no).count()

def load_actisure_customer_id_numbers_into_kyc_db():

    actisure_policies = MedicalPolicy.query.filter(MedicalPolicy.national_id_no.isnot(None)) \
                                            .filter(func.char_length(MedicalPolicy.national_id_no) == 8) \
                                            .filter(not_(MedicalPolicy.national_id_no.like("%A%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%B%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%C%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%D%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%E%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%F%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%G%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%H%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%I%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%J%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%K%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%L%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%M%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%N%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%O%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%P%"))) \
                                            .filter(not_(MedicalPolicy.national_id_no.like("%Q%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%R%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%S%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%T%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%U%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%V%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%W%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%X%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%Y%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%Z%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%.%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%-%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%R%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%G%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%K%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("07%")))\
                                            .distinct(MedicalPolicy.national_id_no)

    url = 'http://192.168.50.27:2300/customer'
    headers = {'content-type': 'application/json'}
    count = 0
    for actisure_policy in actisure_policies:
        payload = {}
        #     if str(pin_number[10]).isalpha():
        if actisure_policy.customer_id_actisure is not None: payload['actisure_id'] = str(actisure_policy.customer_id_actisure)
        if actisure_policy.national_id_no is not None: payload['national_id_no'] = str(actisure_policy.national_id_no)
        # if actisure_policy.email_address_actisure is not None:
        #     is_valid_email = validate_email(actisure_policy.email_address_actisure)
        #     if is_valid_email:
        #         payload['actisure_email'] = str(actisure_policy.email_address_actisure).lower()
        count += 1
        print ("payload is " + str(payload))
        print ("count is " + str(count))
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        if response.status_code == 200:
            print ('record posted success fully ')
        else:
            print ('error while posting record ' + response.text)
    print ("total count is " + str(count))



def load_actisure_customer_id_numbers_and_phone_numbers_into_kyc_db():

    actisure_policies = MedicalPolicy.query.filter(MedicalPolicy.national_id_no.isnot(None)) \
                                            .filter(MedicalPolicy.phone_no_actisure.isnot(None)) \
                                            .filter(func.char_length(MedicalPolicy.national_id_no) == 8) \
                                            .filter(not_(MedicalPolicy.national_id_no.like("%A%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%B%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%C%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%D%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%E%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%F%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%G%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%H%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%I%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%J%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%K%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%L%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%M%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%N%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%O%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%P%"))) \
                                            .filter(not_(MedicalPolicy.national_id_no.like("%Q%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%R%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%S%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%T%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%U%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%V%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%W%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%X%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%Y%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%Z%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%.%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%-%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%R%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%G%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%K%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("07%")))\
                                            .distinct(MedicalPolicy.national_id_no)

    url = 'http://192.168.50.27:2300/customer'
    headers = {'content-type': 'application/json'}
    count = 0
    for actisure_policy in actisure_policies:
        payload = {}
        #     if str(pin_number[10]).isalpha():
        if actisure_policy.customer_id_actisure is not None: payload['actisure_id'] = str(actisure_policy.customer_id_actisure)
        if actisure_policy.national_id_no is not None: payload['national_id_no'] = str(actisure_policy.national_id_no)
        if actisure_policy.phone_no_actisure is not None: payload['actisure_phone_no'] = str(actisure_policy.phone_no_actisure)
        # if actisure_policy.email_address_actisure is not None:
        #     is_valid_email = validate_email(actisure_policy.email_address_actisure)
        #     if is_valid_email:
        #         payload['actisure_email'] = str(actisure_policy.email_address_actisure).lower()
        count += 1
        print ("payload is " + str(payload))
        print ("count is " + str(count))
        # response = requests.post(url, data=json.dumps(payload), headers=headers)
        # if response.status_code == 200:
        #     print ('record posted success fully ')
        # else:
        #     print ('error while posting record ' + response.text)
    print ("total count is " + str(count))


def load_actisure_customer_passport_numbers_into_kyc_db():

    actisure_policies = MedicalPolicy.query.filter(MedicalPolicy.national_id_no.isnot(None)) \
                                            .filter(func.char_length(MedicalPolicy.national_id_no) >= 5) \
                                            .filter(not_(MedicalPolicy.national_id_no.like("%/%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%-%")))\
                                            .filter(not_(MedicalPolicy.national_id_no.like("%TEMP%")))\
                                            .filter(or_(MedicalPolicy.national_id_no.like("A%"),
                                                        (MedicalPolicy.national_id_no.like("B%")),
                                                        (MedicalPolicy.national_id_no.like("C%")),
                                                        (MedicalPolicy.national_id_no.like("D%")),
                                                        (MedicalPolicy.national_id_no.like("E%")),
                                                        (MedicalPolicy.national_id_no.like("F%")),
                                                        (MedicalPolicy.national_id_no.like("G%")),
                                                        (MedicalPolicy.national_id_no.like("H%")),
                                                        (MedicalPolicy.national_id_no.like("I%")),
                                                        (MedicalPolicy.national_id_no.like("J%")),
                                                        (MedicalPolicy.national_id_no.like("K%")),
                                                        (MedicalPolicy.national_id_no.like("L%")),
                                                        (MedicalPolicy.national_id_no.like("M%")),
                                                        (MedicalPolicy.national_id_no.like("N%")),
                                                        (MedicalPolicy.national_id_no.like("O%")),
                                                        (MedicalPolicy.national_id_no.like("P%")),
                                                        (MedicalPolicy.national_id_no.like("Q%")),
                                                        (MedicalPolicy.national_id_no.like("R%")),
                                                        (MedicalPolicy.national_id_no.like("S%")),
                                                        (MedicalPolicy.national_id_no.like("T%")),
                                                        (MedicalPolicy.national_id_no.like("U%")),
                                                        (MedicalPolicy.national_id_no.like("V%")),
                                                        (MedicalPolicy.national_id_no.like("W%")),
                                                        (MedicalPolicy.national_id_no.like("X%")),
                                                        (MedicalPolicy.national_id_no.like("Y%")),
                                                        (MedicalPolicy.national_id_no.like("Z%")),
                                                        )) \
                                            .distinct(MedicalPolicy.national_id_no)

    url = 'http://192.168.50.27:2300/customer'
    headers = {'content-type': 'application/json'}
    count = 0
    for actisure_policy in actisure_policies:
        payload = {}
        #     if str(pin_number[10]).isalpha():
        if actisure_policy.customer_id_actisure is not None: payload['actisure_id'] = str(actisure_policy.customer_id_actisure)
        if actisure_policy.national_id_no is not None: payload['passport_no'] = str(actisure_policy.national_id_no).replace(" ", "")
        # if actisure_policy.email_address_actisure is not None:
        #     is_valid_email = validate_email(actisure_policy.email_address_actisure)
        #     if is_valid_email:
        #         payload['actisure_email'] = str(actisure_policy.email_address_actisure).lower()
        count += 1
        print ("payload is " + str(payload))
        print ("count is " + str(count))
        response = requests.post(url, data=json.dumps(payload), headers=headers)
        if response.status_code == 200:
            print ('record posted success fully ')
        else:
            print ('error while posting record ' + response.text)
    print ("total count is " + str(count))


def get_numnber_of_people_sharing_national_id(id_no):
    return KYCCustomer.query.filter(KYCCustomer.national_id_no == id_no).count()


def create_generator():
    customers = KYCCustomer.query.filter(KYCCustomer.national_id_no.isnot(None)).all()
    for customer in customers:
        count = get_numnber_of_people_sharing_national_id(customer.national_id_no)
        if count > 1:
            yield get_numnber_of_people_sharing_national_id(customer.national_id_no)


def detect_duplicate_passport_numbers_in_kyc_db_and_remove_them():
    grand_total_duplicates = 0
    all_duplicate_passports = []
    t = kyc_db_session.query(KYCCustomer.passport_no,
                             func.count(KYCCustomer.passport_no).label('no_of_duplicates')).filter(
        KYCCustomer.passport_no.isnot(None)).filter(KYCCustomer.premia_id.isnot(None)).group_by(
        KYCCustomer.passport_no)
    total_duplicates = 0
    for passport_no, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_passports.append(passport_no)
            print ("passport_no is " + str(passport_no) + " two is " + str(number_of_times_duplicated))
    print ("total premia duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates

    t = kyc_db_session.query(KYCCustomer.passport_no,
                             func.count(KYCCustomer.passport_no).label('no_of_duplicates')).filter(
        KYCCustomer.passport_no.isnot(None)).filter(KYCCustomer.isf_id.isnot(None)).group_by(
        KYCCustomer.passport_no)
    total_duplicates = 0
    for passport_no, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_passports.append(passport_no)
            print ("passport_no is " + str(passport_no) + " two is " + str(number_of_times_duplicated))
    print ("total isf duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates

    t = kyc_db_session.query(KYCCustomer.passport_no,
                             func.count(KYCCustomer.passport_no).label('no_of_duplicates')).filter(
        KYCCustomer.passport_no.isnot(None)).filter(KYCCustomer.apex_id.isnot(None)).group_by(
        KYCCustomer.passport_no)
    total_duplicates = 0
    for passport_no, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_passports.append(passport_no)
            print ("passport_no is " + str(passport_no) + " two is " + str(number_of_times_duplicated))
    print ("total apex duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates

    t = kyc_db_session.query(KYCCustomer.passport_no,
                             func.count(KYCCustomer.passport_no).label('no_of_duplicates')).filter(
        KYCCustomer.passport_no.isnot(None)).filter(KYCCustomer.actisure_id.isnot(None)).group_by(
        KYCCustomer.passport_no)
    total_duplicates = 0
    for passport_no, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_passports.append(passport_no)
            print ("passport_no is " + str(passport_no) + " two is " + str(number_of_times_duplicated))
    print ("total actisure duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates
    print (all_duplicate_passports)

    # Now loop and delete
    # for passport_no in all_duplicate_passports:
    #     print ("deleting " + str(passport_no))
    #     dupliciate_customers = KYCCustomer.query.filter(KYCCustomer.passport_no == passport_no).all()
    #     for dupliciate_customer in dupliciate_customers:
    #         kyc_db_session.delete(dupliciate_customer)
    #         kyc_db_session.commit()
    print ("Grand duplicates total are : " + str(all_duplicate_passports.__len__()))


def detect_duplicate_national_ids_in_kyc_db_and_remove_them():

    grand_total_duplicates = 0
    all_duplicate_ids = []
    t = kyc_db_session.query(KYCCustomer.national_id_no, func.count(KYCCustomer.national_id_no).label('no_of_duplicates')).filter(KYCCustomer.national_id_no.isnot(None)).filter(KYCCustomer.premia_id.isnot(None)).group_by(KYCCustomer.national_id_no)
    total_duplicates = 0
    for id_number, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_ids.append(id_number)
            print ("id_number is " + str(id_number) + " two is " + str(number_of_times_duplicated))
    print ("total premia duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates

    t = kyc_db_session.query(KYCCustomer.national_id_no, func.count(KYCCustomer.national_id_no).label('no_of_duplicates')).filter(KYCCustomer.national_id_no.isnot(None)).filter(KYCCustomer.isf_id.isnot(None)).group_by(KYCCustomer.national_id_no)
    total_duplicates = 0
    for id_number, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_ids.append(id_number)
            print ("id_number is " + str(id_number) + " two is " + str(number_of_times_duplicated))
    print ("total isf duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates

    t = kyc_db_session.query(KYCCustomer.national_id_no, func.count(KYCCustomer.national_id_no).label('no_of_duplicates')).filter(KYCCustomer.national_id_no.isnot(None)).filter(KYCCustomer.apex_id.isnot(None)).group_by(KYCCustomer.national_id_no)
    total_duplicates = 0
    for id_number, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_ids.append(id_number)
            print ("id_number is " + str(id_number) + " two is " + str(number_of_times_duplicated))
    print ("total apex duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates

    t = kyc_db_session.query(KYCCustomer.national_id_no, func.count(KYCCustomer.national_id_no).label('no_of_duplicates')).filter(KYCCustomer.national_id_no.isnot(None)).filter(KYCCustomer.actisure_id.isnot(None)).group_by(KYCCustomer.national_id_no)
    total_duplicates = 0
    for id_number, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_ids.append(id_number)
            print ("id_number is " + str(id_number) + " two is " + str(number_of_times_duplicated))
    print ("total actisure duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates
    print (all_duplicate_ids)
    
    # Now loop and delete
    for national_id in all_duplicate_ids:
        print ("deleting " + str(national_id))
        dupliciate_customers = KYCCustomer.query.filter(KYCCustomer.national_id_no == national_id).all()
        for dupliciate_customer in dupliciate_customers:
            kyc_db_session.delete(dupliciate_customer)
            kyc_db_session.commit()
    print ("Grand duplicates total are : " + str(all_duplicate_ids.__len__()))


def detect_duplicate_emails_in_kyc_db_and_remove_them():
    grand_total_duplicates = 0
    all_duplicate_emails = []
    t = kyc_db_session.query(KYCCustomer.premia_email,
                             func.count(KYCCustomer.premia_email).label('no_of_duplicates')).filter(
        KYCCustomer.premia_email.isnot(None)).group_by(
        KYCCustomer.premia_email)
    total_duplicates = 0
    for premia_email, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_emails.append(premia_email)
            print ("premia_email is " + str(premia_email) + " two is " + str(number_of_times_duplicated))
    print ("total premia duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates

    t = kyc_db_session.query(KYCCustomer.isf_email,
                             func.count(KYCCustomer.isf_email).label('no_of_duplicates')).filter(
        KYCCustomer.isf_email.isnot(None)).filter(KYCCustomer.isf_id.isnot(None)).group_by(
        KYCCustomer.isf_email)
    total_duplicates = 0
    for isf_email, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_emails.append(isf_email)
            print ("isf_email is " + str(isf_email) + " two is " + str(number_of_times_duplicated))
    print ("total isf duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates

    t = kyc_db_session.query(KYCCustomer.actisure_phone_no,
                             func.count(KYCCustomer.actisure_phone_no).label('no_of_duplicates')).filter(
        KYCCustomer.actisure_phone_no.isnot(None)).filter(KYCCustomer.apex_id.isnot(None)).group_by(
        KYCCustomer.actisure_phone_no)
    total_duplicates = 0
    for actisure_phone_no, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_emails.append(actisure_phone_no)
            print ("actisure_phone_no is " + str(actisure_phone_no) + " two is " + str(number_of_times_duplicated))
    print ("total apex duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates

    t = kyc_db_session.query(KYCCustomer.apex_email,
                             func.count(KYCCustomer.apex_email).label('no_of_duplicates')).filter(
        KYCCustomer.apex_email.isnot(None)).filter(KYCCustomer.actisure_id.isnot(None)).group_by(
        KYCCustomer.apex_email)
    total_duplicates = 0
    for apex_email, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_emails.append(apex_email)
            print ("apex_email is " + str(apex_email) + " two is " + str(number_of_times_duplicated))
    print ("total actisure duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates
    print (all_duplicate_emails)

    # Now loop and delete
    for email in all_duplicate_emails:
        print ("deleting " + str(apex_email))
        dupliciate_customers = KYCCustomer.query.filter(or_(KYCCustomer.apex_email == email,
                                                          KYCCustomer.isf_email == email,
                                                          KYCCustomer.premia_email == email,
                                                          KYCCustomer.actisure_phone_no == email)).all()

        for dupliciate_customer in dupliciate_customers:
            kyc_db_session.delete(dupliciate_customer)
            kyc_db_session.commit()
    print ("Grand duplicates total are : " + str(all_duplicate_emails.__len__()))


def detect_duplicate_phone_numbers_in_kyc_db_and_remove_them():
    grand_total_duplicates = 0
    all_duplicate_phone_numbers = []
    t = kyc_db_session.query(KYCCustomer.premia_phone_no,
                             func.count(KYCCustomer.premia_phone_no).label('no_of_duplicates')).filter(
        KYCCustomer.premia_phone_no.isnot(None)).group_by(
        KYCCustomer.premia_phone_no)
    total_duplicates = 0
    for premia_phone_no, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_phone_numbers.append(premia_phone_no)
            print ("premia_phone_no is " + str(premia_phone_no) + " two is " + str(number_of_times_duplicated))
    print ("total premia duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates

    t = kyc_db_session.query(KYCCustomer.isf_phone_no,
                             func.count(KYCCustomer.isf_phone_no).label('no_of_duplicates')).filter(
        KYCCustomer.isf_phone_no.isnot(None)).filter(KYCCustomer.isf_id.isnot(None)).group_by(
        KYCCustomer.isf_phone_no)
    total_duplicates = 0
    for isf_phone_no, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_phone_numbers.append(isf_phone_no)
            print ("isf_phone_no is " + str(isf_phone_no) + " two is " + str(number_of_times_duplicated))
    print ("total isf duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates

    t = kyc_db_session.query(KYCCustomer.actisure_phone_no,
                             func.count(KYCCustomer.actisure_phone_no).label('no_of_duplicates')).filter(
        KYCCustomer.actisure_phone_no.isnot(None)).filter(KYCCustomer.apex_id.isnot(None)).group_by(
        KYCCustomer.actisure_phone_no)
    total_duplicates = 0
    for actisure_phone_no, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_phone_numbers.append(actisure_phone_no)
            print ("actisure_phone_no is " + str(actisure_phone_no) + " two is " + str(number_of_times_duplicated))
    print ("total apex duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates

    t = kyc_db_session.query(KYCCustomer.apex_phone_no,
                             func.count(KYCCustomer.apex_phone_no).label('no_of_duplicates')).filter(
        KYCCustomer.apex_phone_no.isnot(None)).filter(KYCCustomer.actisure_id.isnot(None)).group_by(
        KYCCustomer.apex_phone_no)
    total_duplicates = 0
    for apex_phone_no, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_phone_numbers.append(apex_phone_no)
            print ("apex_phone_no is " + str(apex_phone_no) + " two is " + str(number_of_times_duplicated))
    print ("total actisure duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates
    print (all_duplicate_phone_numbers)

    # Now loop and delete
    for phone in all_duplicate_phone_numbers:
        print ("deleting " + str(phone))
        dupliciate_customers = KYCCustomer.query.filter(or_(KYCCustomer.apex_phone_no == phone,
                                                          KYCCustomer.isf_phone_no == phone,
                                                          KYCCustomer.premia_phone_no == phone,
                                                          KYCCustomer.actisure_phone_no == phone)).all()

        for dupliciate_customer in dupliciate_customers:
            kyc_db_session.delete(dupliciate_customer)
            kyc_db_session.commit()

    print ("Grand duplicates total are : " + str(all_duplicate_phone_numbers.__len__()))


def detect_duplicate_apex_ids_in_kyc_db_and_remove_them():
    #do apex
    grand_total_duplicates = 0
    all_duplicate_apex_ids = []
    t = kyc_db_session.query(KYCCustomer.apex_id,
                             func.count(KYCCustomer.apex_id).label('no_of_duplicates')).filter(
        KYCCustomer.apex_id.isnot(None)).group_by(KYCCustomer.apex_id)
    total_duplicates = 0
    for apex_id, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_apex_ids.append(apex_id)
            print ("apex_id is " + str(apex_id) + " two is " + str(number_of_times_duplicated))
    print ("total apex duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates
    print (all_duplicate_apex_ids)

    # Now loop and delete
    for apex_id in all_duplicate_apex_ids:
        print ("deleting " + str(apex_id))
        dupliciate_customer = KYCCustomer.query.filter(KYCCustomer.apex_id == apex_id).first()
        kyc_db_session.delete(dupliciate_customer)
        kyc_db_session.commit()

    print ("Grand duplicates total are : " + str(all_duplicate_apex_ids.__len__()))

    #now do premia
    grand_total_duplicates = 0
    all_duplicate_premia_ids = []
    t = kyc_db_session.query(KYCCustomer.premia_id,
                             func.count(KYCCustomer.premia_id).label('no_of_duplicates')).filter(
        KYCCustomer.premia_id.isnot(None)).group_by(KYCCustomer.premia_id)
    total_duplicates = 0
    for premia_id, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_premia_ids.append(premia_id)
            print ("premia_id is " + str(premia_id) + " two is " + str(number_of_times_duplicated))
    print ("total premia duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates
    print (all_duplicate_premia_ids)

    # Now loop and delete
    for premia_id in all_duplicate_premia_ids:
        print ("deleting " + str(premia_id))
        dupliciate_customer = KYCCustomer.query.filter(KYCCustomer.premia_id == premia_id).first()
        kyc_db_session.delete(dupliciate_customer)
        kyc_db_session.commit()

    print ("Grand duplicates total are : " + str(all_duplicate_premia_ids.__len__()))

    #do actisure
    grand_total_duplicates = 0
    all_duplicate_actisure_ids = []
    t = kyc_db_session.query(KYCCustomer.actisure_id,
                             func.count(KYCCustomer.actisure_id).label('no_of_duplicates')).filter(
        KYCCustomer.actisure_id.isnot(None)).group_by(KYCCustomer.actisure_id)
    total_duplicates = 0
    for actisure_id, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_actisure_ids.append(actisure_id)
            print ("actisure_id is " + str(actisure_id) + " two is " + str(number_of_times_duplicated))
    print ("total actisure duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates
    print (all_duplicate_actisure_ids)

    # Now loop and delete
    for actisure_id in all_duplicate_actisure_ids:
        print ("deleting " + str(actisure_id))
        dupliciate_customer = KYCCustomer.query.filter(KYCCustomer.actisure_id == actisure_id).first()
        kyc_db_session.delete(dupliciate_customer)
        kyc_db_session.commit()

    #do isf
    grand_total_duplicates = 0
    all_duplicate_isf_ids = []
    t = kyc_db_session.query(KYCCustomer.isf_id,
                             func.count(KYCCustomer.isf_id).label('no_of_duplicates')).filter(
        KYCCustomer.isf_id.isnot(None)).group_by(KYCCustomer.isf_id)
    total_duplicates = 0
    for isf_id, number_of_times_duplicated in t:
        if number_of_times_duplicated > 1:
            total_duplicates += number_of_times_duplicated
            all_duplicate_isf_ids.append(isf_id)
            print ("isf_id is " + str(isf_id) + " two is " + str(number_of_times_duplicated))
    print ("total isf duplicates is " + str(total_duplicates))
    grand_total_duplicates += total_duplicates
    print (all_duplicate_isf_ids)

    # Now loop and delete
    for isf_id in all_duplicate_isf_ids:
        print ("deleting " + str(isf_id))
        dupliciate_customer = KYCCustomer.query.filter(KYCCustomer.isf_id == isf_id).first()
        kyc_db_session.delete(dupliciate_customer)
        kyc_db_session.commit()

    print ("Grand duplicates total are : " + str(all_duplicate_isf_ids.__len__()))


def load_kyc_customer_details_from_premia():
    kyc_customers = KYCCustomer.query.filter(KYCCustomer.premia_id.isnot(None)).all()

    for kyc_customer in kyc_customers:
        premia_id = kyc_customer.premia_id
        kyc_id = kyc_customer.kyc_id

        general_policy = GeneralPolicy.query.filter(GeneralPolicy.customer_id_premia == premia_id).first()
        name = general_policy.customer_name
        date_of_birth = general_policy.date_of_birth
        gender = general_policy.gender

        print ("Customer name is " + name)
        # customer_details = KYCCustomerDetails(kyc_id, name, date_of_birth, gender)
        # kyc_db_session.add(customer_details)
        # kyc_db_session.commit()


def load_kyc_customer_details_from_pension():
    kyc_table_kyc_ids_and_apex_ids = {}
    kyc_cust_details_table_kyc_ids = []
    kyc_customers = kyc_db_session.query(KYCCustomer).filter(KYCCustomer.apex_id.isnot(None)).all()
    for kyc_customer in kyc_customers:
        kyc_table_kyc_ids_and_apex_ids[kyc_customer.kyc_id] = kyc_customer.apex_id

    kyc_cust_details = kyc_db_session.query(KYCCustomerDetails.id).all()
    for row in kyc_cust_details:
        kyc_cust_details_table_kyc_ids.append(row[0])

    for detail_id in kyc_cust_details_table_kyc_ids:
        if detail_id in kyc_table_kyc_ids_and_apex_ids: del kyc_table_kyc_ids_and_apex_ids[detail_id]

    kyc_count = 0
    for kyc_table_kyc_id in kyc_table_kyc_ids_and_apex_ids.keys():
        kyc_count += 1
        print "Loading ..... " + str(kyc_count) + ". " + str(kyc_table_kyc_id) + " Into KYC DB"
        apex_id = kyc_table_kyc_ids_and_apex_ids[kyc_table_kyc_id]

        pension_customer = PensionPolicy.query.filter(PensionPolicy.customer_id_apex == apex_id) \
                                            .filter(PensionPolicy.customer_name.isnot(None)).first()
        if pension_customer is not None:
            name = pension_customer.customer_name
            date_of_birth = pension_customer.date_of_birth
            gender = pension_customer.gender
            if gender is not None and gender is not "M" and gender is not "F": gender = None
            if gender == "M": gender = "MALE"
            if gender == "F": gender = "FEMALE"
            print (gender)
            if name is not None:
                name = str(name).upper()
                print ("Customer name is " + name + "Date of Birth is " + str(date_of_birth) + "Gender is " + str(gender))

                try:
                    kyc_db_session.add(KYCCustomerDetails(kyc_table_kyc_id, name, date_of_birth, gender))
                    kyc_db_session.commit()
                except exc.DataError, e:
                    kyc_db_session.rollback()
                    continue
                except exc.IntegrityError, e:
                    kyc_db_session.rollback()
                    continue


def load_kyc_customer_details_from_premia():
    kyc_table_kyc_ids_and_premia_ids = {}
    kyc_cust_details_table_kyc_ids = []
    kyc_customers = kyc_db_session.query(KYCCustomer).filter(KYCCustomer.premia_id.isnot(None)).all()
    for kyc_customer in kyc_customers:
        kyc_table_kyc_ids_and_premia_ids[kyc_customer.kyc_id] = kyc_customer.premia_id

    kyc_cust_details = kyc_db_session.query(KYCCustomerDetails.id).all()
    for row in kyc_cust_details:
        kyc_cust_details_table_kyc_ids.append(row[0])

    for detail_id in kyc_cust_details_table_kyc_ids:
        if detail_id in kyc_table_kyc_ids_and_premia_ids: del kyc_table_kyc_ids_and_premia_ids[detail_id]

    kyc_count = 0
    for kyc_table_kyc_id in kyc_table_kyc_ids_and_premia_ids.keys():
        kyc_count += 1
        print "Loading ..... " + str(kyc_count) + ". " + str(kyc_table_kyc_id) + " Into KYC DB"
        premia_id = kyc_table_kyc_ids_and_premia_ids[kyc_table_kyc_id]
        premia_customer = GeneralPolicy.query.filter(GeneralPolicy.customer_id_premia == premia_id) \
                                            .filter(GeneralPolicy.customer_name.isnot(None)).first()
        if premia_customer is not None:
            name = premia_customer.customer_name
            date_of_birth = premia_customer.date_of_birth
            #Premia has no gender field
            gender = None
            if gender is not None and gender is not "M" and gender is not "F": gender = None
            if gender == "M": gender = "MALE"
            if gender == "F": gender = "FEMALE"
            if name is not None:
                name = str(name).upper()
                print ("Customer name is " + name + "Date of Birth is " + str(date_of_birth) + " Gender is " + str(gender))

                try:
                    kyc_db_session.add(KYCCustomerDetails(kyc_table_kyc_id, name, date_of_birth, gender))
                    kyc_db_session.commit()
                except exc.DataError, e:
                    kyc_db_session.rollback()
                    continue
                except exc.IntegrityError, e:
                    kyc_db_session.rollback()
                    continue

def load_kyc_customer_details_from_isf():
    kyc_table_kyc_ids_and_isf_ids = {}
    kyc_cust_details_table_kyc_ids = []
    kyc_customers = kyc_db_session.query(KYCCustomer).filter(KYCCustomer.isf_id.isnot(None)).all()
    for kyc_customer in kyc_customers:
        kyc_table_kyc_ids_and_isf_ids[kyc_customer.kyc_id] = kyc_customer.isf_id

    kyc_cust_details = kyc_db_session.query(KYCCustomerDetails.id).all()
    for row in kyc_cust_details:
        kyc_cust_details_table_kyc_ids.append(row[0])

    for detail_id in kyc_cust_details_table_kyc_ids:
        if detail_id in kyc_table_kyc_ids_and_isf_ids: del kyc_table_kyc_ids_and_isf_ids[detail_id]

    kyc_count = 0
    for kyc_table_kyc_id in kyc_table_kyc_ids_and_isf_ids.keys():
        kyc_count += 1
        print "Loading ..... "+ str(kyc_count) + ". " + str(kyc_table_kyc_id) + " Into KYC DB"
        isf_id = kyc_table_kyc_ids_and_isf_ids[kyc_table_kyc_id]
        life_customer = LifePolicy.query.filter(LifePolicy.customer_id_isf == isf_id) \
                                            .filter(LifePolicy.customer_name.isnot(None)).first()
        if life_customer:
            name = life_customer.customer_name
            date_of_birth = life_customer.date_of_birth
            gender = life_customer.gender
            if gender is not None and gender is not "M" and gender is not "F": gender = None
            if gender == "M": gender = "MALE"
            if gender == "F": gender = "FEMALE"
            print (gender)
            if name is not None:
                name = str(name).upper()
                print ("Customer name is " + name + "Date of Birth is " + str(date_of_birth) + " Gender is " + str(gender))

                try:
                    kyc_db_session.add(KYCCustomerDetails(kyc_table_kyc_id, name, date_of_birth, gender))
                    kyc_db_session.commit()
                except exc.DataError, e:
                    kyc_db_session.rollback()
                    continue
                except exc.IntegrityError, e:
                    kyc_db_session.rollback()
                    continue


def load_kyc_customer_details_from_actisure_failover():
    kyc_table_kyc_ids_and_actisure_ids = {}
    kyc_cust_details_table_kyc_ids = []
    kyc_customers = kyc_db_session.query(KYCCustomer).filter(KYCCustomer.actisure_id.isnot(None)).all()
    for kyc_customer in kyc_customers:
        kyc_table_kyc_ids_and_actisure_ids[kyc_customer.kyc_id] = kyc_customer.actisure_id

    kyc_cust_details = kyc_db_session.query(KYCCustomerDetails.id).all()
    for row in kyc_cust_details:
        kyc_cust_details_table_kyc_ids.append(row[0])

    for detail_id in kyc_cust_details_table_kyc_ids:
        if detail_id in kyc_table_kyc_ids_and_actisure_ids : del kyc_table_kyc_ids_and_actisure_ids[detail_id]

    kyc_count = 0
    for kyc_table_kyc_id in kyc_table_kyc_ids_and_actisure_ids.keys():
        kyc_count += 1
        print "Loading ..... " + str(kyc_count) + ". " + str(kyc_table_kyc_id) + " Into KYC DB"
        actisure_id = kyc_table_kyc_ids_and_actisure_ids[kyc_table_kyc_id]
        medical_customer = FailOverCustomerDetails.query.filter(FailOverCustomerDetails.customer_id_actisure == actisure_id) \
            .filter(FailOverCustomerDetails.customer_name.isnot(None)).first()
        if medical_customer:
            name = medical_customer.customer_name
            date_of_birth = medical_customer.date_of_birth
            gender = medical_customer.gender
            if gender is not None : gender = str(gender).upper()
            if name is not None:
                name = str(name).encode('ascii', 'ignore').decode('ascii')
                name = name.replace("MR & MRS ", "").replace("Mrs ", "").replace("Miss ", "").replace("MS ", "").replace("MRS ", "").replace("MR ", "").replace("Mr ", "").upper()
                print ("Customer name is " + name + "Date of Birth is " + str(date_of_birth))
                try:
                    kyc_db_session.add(KYCCustomerDetails(kyc_table_kyc_id, name, date_of_birth, gender))
                    kyc_db_session.commit()
                except exc.DataError, e:
                    kyc_db_session.rollback()
                    continue
                except exc.IntegrityError, e:
                    kyc_db_session.rollback()
                    continue


def load_kyc_policy_details_from_premia(min_offset, max_offset):
    for offset in range(min_offset, max_offset):
        interval = max_offset - min_offset
        current_offset_pos = (offset+1)-min_offset
        percentage = float(float(current_offset_pos)/float(interval)*100)
        print ("load_kyc_policy_details_from_premia offset is %d of %d is %f " % (offset, max_offset, percentage)
               + "% complete")

        # Get premia customers
        kyc_premia_customers = KYCCustomer.query.filter(KYCCustomer.premia_id.isnot(None)).offset(offset).limit(1).all()
        for index, kyc_premia_customer in enumerate(kyc_premia_customers):
            customer_kyc_id = kyc_premia_customer.kyc_id
            loaded_kyc_policy_numbers = [i[0] for i in kyc_db_session.query(KYCPolicy.policy_number)\
                .filter(KYCPolicy.kyc_id == customer_kyc_id).all()]

            premia_policies = PolicyDetails.query.filter(PolicyDetails.assured_id == kyc_premia_customer.premia_id).all()
            print ("%d. Customer %s has %d premia policies" % (index + 1, customer_kyc_id, premia_policies.__len__()))
            for premia_policy in premia_policies:
                if premia_policy.policy_no not in loaded_kyc_policy_numbers:
                    kyc_policy = KYCPolicy()
                    kyc_policy.id = str(uuid.uuid4())
                    kyc_policy.kyc_id = kyc_premia_customer.kyc_id
                    kyc_policy.policy_number = premia_policy.policy_no
                    kyc_policy.product = None
                    kyc_policy.pol_class = premia_policy.pol_class
                    kyc_policy.pol_sub_class = premia_policy.pol_subclass
                    kyc_policy.status = premia_policy.status
                    kyc_policy.date_of_purchase = premia_policy.from_date
                    kyc_policy.renewal_date = premia_policy.to_date
                    kyc_policy.maturity_date = premia_policy.to_date
                    kyc_policy.benefit_due_date = premia_policy.to_date
                    kyc_policy.last_updated = datetime.utcnow()
                    kyc_policy.line_of_business = 'GENERAL'

                    print (premia_policy)
                    # Save it to the kyc db
                    kyc_db_session.add(kyc_policy)
                    kyc_db_session.commit()


def load_kyc_policy_details_from_actisure(min_offset, max_offset):
    for offset in range(min_offset, max_offset):
        interval = max_offset - min_offset
        current_offset_pos = (offset+1)-min_offset
        percentage = float(float(current_offset_pos)/float(interval)*100)
        print ("load_kyc_policy_details_from_actisure offset is %d of %d is %f " % (offset, max_offset, percentage)
               + "% complete")

        # Get isf customers
        kyc_isf_customers = KYCCustomer.query.filter(KYCCustomer.actisure_id.isnot(None)).offset(offset).limit(1).all()

        for index, kyc_actisure_customer in enumerate(kyc_isf_customers):
            customer_kyc_id = kyc_actisure_customer.kyc_id
            actisure_policies = FailOverCustomerDetails.query.distinct(FailOverCustomerDetails.policy_no).filter(FailOverCustomerDetails.customer_id_actisure == kyc_actisure_customer.actisure_id).all()

            print ("%d. Customer %s has %d  actisure policies" % (index + 1, customer_kyc_id, actisure_policies.__len__()))
            for actisure_policy in actisure_policies:

                status = actisure_policy.policy_status
                if status is not None: str(status).upper()

                product = actisure_policy.policy_product
                if product is not None: str(product).upper()

                kyc_policy = KYCPolicy()
                kyc_policy.id = str(uuid.uuid4())
                kyc_policy.kyc_id = kyc_actisure_customer.kyc_id
                kyc_policy.policy_number = actisure_policy.policy_no
                kyc_policy.pol_class = product
                # kyc_policy.pol_sub_class = actisure_policy.pol_subclass
                kyc_policy.status = status
                #kyc_policy.surrender_value = actisure_policy.policy_amount
                kyc_policy.start_date = actisure_policy.policy_effective_date
                #kyc_policy.date_of_purchase = actisure_policy.from_date
                kyc_policy.renewal_date = actisure_policy.policy_end_date
                # kyc_policy.maturity_date = actisure_policy.to_date
                # kyc_policy.benefit_due_date = actisure_policy.to_date
                kyc_policy.last_updated = datetime.utcnow()
                kyc_policy.line_of_business = 'MEDICAL'

                print (kyc_policy)
                # Save it to the kyc db
                kyc_db_session.add(kyc_policy)
                kyc_db_session.commit()


def load_kyc_policy_details_from_apex(min_offset, max_offset):
    for offset in range(min_offset, max_offset):
        interval = max_offset - min_offset
        current_offset_pos = (offset+1)-min_offset
        percentage = float(float(current_offset_pos)/float(interval)*100)
        print ("load_kyc_policy_details_from_apex offset is %d of %d is %f " % (offset, max_offset, percentage)
               + "% complete")

        # Get isf customers
        kyc_apex_customers = KYCCustomer.query.filter(KYCCustomer.apex_id.isnot(None)).offset(offset).limit(1).all()

        for index, kyc_apex_customer in enumerate(kyc_apex_customers):
            customer_kyc_id = kyc_apex_customer.kyc_id
            loaded_kyc_policy_numbers = [i[0] for i in kyc_db_session.query(KYCPolicy.policy_number) \
                .filter(KYCPolicy.kyc_id == customer_kyc_id).all()]

            apex_policies = PensionPolicy.query.distinct(PensionPolicy.policy_no).filter(PensionPolicy.customer_id_apex == kyc_apex_customer.apex_id).all()

            print ("%d. Customer %s has %d  apex policies" % (index + 1, customer_kyc_id, apex_policies.__len__()))
            for apex_policy in apex_policies:
                if apex_policy.policy_no not in loaded_kyc_policy_numbers:
                    kyc_policy = KYCPolicy()
                    kyc_policy.id = str(uuid.uuid4())
                    kyc_policy.kyc_id = kyc_apex_customer.kyc_id
                    kyc_policy.policy_number = apex_policy.policy_no
                    # kyc_policy.pol_class = product
                    # # kyc_policy.pol_sub_class = actisure_policy.pol_subclass
                    # kyc_policy.status = status
                    # kyc_policy.surrender_value = actisure_policy.policy_amount
                    # kyc_policy.start_date = actisure_policy.policy_effective_date
                    # #kyc_policy.date_of_purchase = actisure_policy.from_date
                    # kyc_policy.renewal_date = actisure_policy.policy_end_date
                    # # kyc_policy.maturity_date = actisure_policy.to_date
                    # # kyc_policy.benefit_due_date = actisure_policy.to_date
                    kyc_policy.last_updated = datetime.utcnow()
                    kyc_policy.line_of_business = 'PENSIONS'

                    print (kyc_policy)
                    # Save it to the kyc db
                    kyc_db_session.add(kyc_policy)
                    kyc_db_session.commit()


def load_kyc_policy_details_from_isf(min_offset, max_offset):
    for offset in range(min_offset, max_offset):
        interval = max_offset - min_offset
        current_offset_pos = (offset + 1) - min_offset
        percentage = float(float(current_offset_pos) / float(interval) * 100)
        print ("load_kyc_policy_details_from_isf offset is %d of %d is %f " % (
        offset, max_offset, percentage)
               + "% complete")

        # Get isf customers
        kyc_isf_customers = KYCCustomer.query.filter(KYCCustomer.isf_id.isnot(None)).offset(
            offset).limit(1).all()

        for index, kyc_isf_customer in enumerate(kyc_isf_customers):
            customer_kyc_id = kyc_isf_customer.kyc_id
            loaded_kyc_policy_numbers = [i[0] for i in kyc_db_session.query(KYCPolicy.policy_number) \
                .filter(KYCPolicy.kyc_id == customer_kyc_id).all()]

            isf_policies = LifePolicy.query.filter(
                LifePolicy.customer_id == kyc_isf_customer.isf_id).all()
            print ("%d. Customer %s has %d isf policies" % (
            index + 1, customer_kyc_id, isf_policies.__len__()))
            for isf_policy in isf_policies:
                if isf_policy.policy_no not in loaded_kyc_policy_numbers:
                    kyc_policy = KYCPolicy()
                    kyc_policy.id = str(uuid.uuid4())
                    kyc_policy.kyc_id = kyc_isf_customer.kyc_id
                    kyc_policy.policy_number = isf_policy.policy_no
                    # kyc_policy.product = None
                    # kyc_policy.pol_class = isf_policy.pol_class
                    # kyc_policy.pol_sub_class = isf_policy.pol_subclass
                    # kyc_policy.status = isf_policy.status
                    # kyc_policy.date_of_purchase = isf_policy.from_date
                    # kyc_policy.renewal_date = isf_policy.to_date
                    # kyc_policy.maturity_date = isf_policy.to_date
                    # kyc_policy.benefit_due_date = isf_policy.to_date
                    kyc_policy.last_updated = datetime.utcnow()
                    kyc_policy.line_of_business = 'LIFE'

                    print (kyc_policy)
                    # Save it to the kyc db
                    kyc_db_session.add(kyc_policy)
                    kyc_db_session.commit()




                                        # for policy in LifePolicy.query.offset(offset).limit(1).all():
        #     kyc_customer = KYCCustomer.query.filter(KYCCustomer.isf_id == str(policy.customer_id_isf)).first()
        #     if kyc_customer:
        #         kyc_policy = KYCPolicy()
        #         kyc_policy.kyc_id = kyc_customer.kyc_id
        #         kyc_policy.pol_number = policy.policy_no
        #         kyc_policy.line_of_business = 'LIFE'
        #
        #         print (policy.policy_no)
        #         # kyc_db_session.add(kyc_policy)
        #         # kyc_db_session.commit()
        #
        #
        #         #print("count is "+ str(count))
        #         #print (policy)
        #         #print (kyc_policy)
        #     #else:
        #         #print("Error finding KYC Customer with assured ID " + policy.assured_id)


def push_payment_details_from_isf_to_kyc_db(min_offset, max_offset):
    for offset in range(min_offset, max_offset):
        interval = max_offset - min_offset
        current_offset_pos = (offset + 1) - min_offset
        percentage = float(float(current_offset_pos) / float(interval) * 100)
        print ("push_payment_details_from_isf_to_kyc_db offset is %d of %d is %f " % (
            offset, max_offset, percentage)
               + "% complete")

        life_policies = kyc_db_session.query(KYCPolicy).filter(KYCPolicy.line_of_business == "LIFE").offset(offset).limit(1).all()
        for life_policy in life_policies:
            policy_no = life_policy.policy_number

            remt_receipts = isf_uat_db_session.query(REMTReceipt) \
                .filter(REMTReceipt.amount.isnot(None)) \
                .filter(REMTReceipt.receipt_no.isnot(None)) \
                .filter(REMTReceipt.policy_no == policy_no) \
                .all()

            number_of_payments = remt_receipts.__len__()
            if number_of_payments > 0:
                print "===================Processing====" + policy_no + "=================="
                for remt_receipt in remt_receipts:
                    kyc_pay_details = KYCPaymentDetails()
                    kyc_pay_details.kyc_id = life_policy.kyc_id
                    kyc_pay_details.policy_number = policy_no
                    kyc_pay_details.receipt_number = remt_receipt.receipt_no
                    kyc_pay_details.amount = remt_receipt.amount
                    kyc_pay_details.status = remt_receipt.remarks
                    kyc_pay_details.date_of_payment = remt_receipt.date
                    kyc_pay_details.line_of_business = "LIFE"
                    kyc_pay_details.last_updated = datetime.utcnow()
                    kyc_db_session.add(kyc_pay_details)
                    kyc_db_session.commit()
                    print remt_receipt


def load_customer_data_from_kyc_to_crm():
    count = 0
    cust_details = KYCCustomerDetails.query.all()

    for kyc_customer_details in cust_details:
        kyc_id = kyc_customer_details.id
        #check if the customer has been pre-loaded on CRM
        exists_in_crm_count = Account.query.filter(Account.id == kyc_id).count()
        if exists_in_crm_count == 0:
            kyc_customer = KYCCustomer.query.filter(KYCCustomer.kyc_id == kyc_id).first()
            customer_name = kyc_customer_details.name
            customer_date_of_birth = kyc_customer_details.date_of_birth
            customer_gender = kyc_customer_details.gender
            print (kyc_customer_details)

            if kyc_customer is not None:
                #assign other values in memory to prepare the actual load
                email_addresses = []
                if kyc_customer.actisure_email is not None: email_addresses.append(kyc_customer.actisure_email)
                if kyc_customer.apex_email is not None: email_addresses.append(kyc_customer.apex_email)
                if kyc_customer.isf_email is not None: email_addresses.append(kyc_customer.isf_email)
                if kyc_customer.jubilee_insure_email is not None: email_addresses.append(kyc_customer.jubilee_insure_email)
                if kyc_customer.premia_email is not None: email_addresses.append(kyc_customer.premia_email)

                phone_numbers = []
                if kyc_customer.actisure_phone_no is not None: phone_numbers.append(kyc_customer.actisure_phone_no)
                if kyc_customer.apex_phone_no is not None: phone_numbers.append(kyc_customer.apex_phone_no)
                if kyc_customer.isf_phone_no is not None: phone_numbers.append(kyc_customer.isf_phone_no)
                if kyc_customer.jubilee_insure_phone_no is not None: phone_numbers.append(
                    kyc_customer.jubilee_insure_phone_no)
                if kyc_customer.premia_phone_no is not None: phone_numbers.append(kyc_customer.premia_phone_no)

                income_tax_number = kyc_customer.income_tax_number
                national_id_no = kyc_customer.national_id_no
                passport_no = kyc_customer.passport_no

                try:
                    email_address = random.choice(email_addresses)
                except IndexError:
                    email_address = None

                try:
                    phone_number = random.choice(phone_numbers)
                except IndexError:
                    phone_number = None

                account = Account()
                account.id = kyc_id
                account.name = customer_name
                account.national_id = national_id_no
                account.mobile_phone = phone_number
                account.created_by = '1'
                account.assigned_user_id = '1'
                account.crm_regn_ctry_name = 'kenya'

                account_cstm = AccountCSTM()
                account_cstm.id = kyc_id
                account_cstm.income_tax_number = income_tax_number
                account_cstm.passport_number = passport_no
                account_cstm.date_of_birth = customer_date_of_birth
                account_cstm.gender = customer_gender
                account_cstm.core_system_customer = '1'

                suite_crm_db_session.add(account)
                suite_crm_db_session.add(account_cstm)
                suite_crm_db_session.commit()

                print ("account Loaded into CRM kyc_id " + kyc_id)
                print ("account_cstm Loaded into CRM kyc_id " + kyc_id)
                count += 1
                print (str(kyc_id)+ " loaded at count " + str(count))

    print("Total count loaded is " + str(count))







    # insert_count = 0
    # kyc_customers = KYCCustomer.query.offset(m_offset).limit(1).all()
    # for kyc_customer in kyc_customers:
    #     kyc_id = kyc_customer.kyc_id
    #     #print("Looking up details for " + kyc_id)
    #     #check if the customer has been pre-loaded on CRM
    #     exists_in_crm_count = Account.query.filter(Account.id == kyc_id).count()
    #     if exists_in_crm_count == 0:
    #         #print("exists_in_crm_count is " + str(exists_in_crm_count))
    #         # Fetch customer deta
    #         insert_count += 1
    #         kyc_customer_details = KYCCustomerDetails.query.filter(KYCCustomerDetails.id == kyc_id).first()
    #
    #         if kyc_customer_details:
    #             customer_name = kyc_customer_details.name
    #             customer_date_of_birth = kyc_customer_details.date_of_birth
    #             customer_gender = kyc_customer_details.gender
    #             print (kyc_customer_details)
                # print
                # #assign other values in memory to prepare the actual load
                # email_addresses = []
                # if kyc_customer.actisure_email is not None: email_addresses.append(kyc_customer.actisure_email)
                # if kyc_customer.apex_email is not None: email_addresses.append(kyc_customer.apex_email)
                # if kyc_customer.isf_email is not None: email_addresses.append(kyc_customer.isf_email)
                # if kyc_customer.jubilee_insure_email is not None: email_addresses.append(kyc_customer.jubilee_insure_email)
                # if kyc_customer.premia_email is not None: email_addresses.append(kyc_customer.premia_email)


def update_old_pension_memb_no_to_new_memb_no():
    #pensions_cust_kyc = kyc_db_session.query(KYCCustomer).filter(KYCCustomer.apex_id.isnot(None)).all()
    apex_customers = apex_db_session.query(PensionAccountInfo).all()
    for apex_customer in apex_customers:
        print "apex_customer.policy_no " + str(apex_customer.policy_no) + " apex_customer.old_policy_no " + str(apex_customer.old_policy_no)
        pensions_kyc_policy = kyc_db_session.query(KYCPolicy).filter(KYCPolicy.policy_number == apex_customer.old_policy_no).first()
        if pensions_kyc_policy is not None:
            print pensions_kyc_policy.policy_number
        #kyc_db_session.query(KYCPaymentDetails).filter(KYCPaymentDetails.id == kyc_payment_detail.id).update({"id": payment_id})
        #kyc_db_session.commit()




    #
    # pensions_kyc_policies = kyc_db_session.query(KYCPolicy).filter(KYCPolicy.line_of_business == "PENSIONS").all()
    # for kyc_pension_policy in pensions_kyc_policies:
    #     old_policy_no = str(kyc_pension_policy.policy_number)
    #     print "apex_customer.old_policy_no "+str(old_policy_no)

        # #     print "KYC POLICY NUMBER TO BE REPLACED IS "+str(pension_policy.policy_number)+ " New Policy Number is "+ str(apex_customer.policy_no)
    #     kyc_payment_details = kyc_db_session.query(KYCPaymentDetails).offset(offset).limit(1).all()
    #     for kyc_payment_detail in kyc_payment_details:
    #         kyc_payment_detail_id = kyc_payment_detail.id
    #         if type(kyc_payment_detail_id) is int:
    #             print "offset is " + str(offset)
    #             payment_id = str(uuid.uuid4())
    #             print payment_id
    #             kyc_db_session.query(KYCPaymentDetails).filter(KYCPaymentDetails.id == kyc_payment_detail.id)\
    #                 .update({"id": payment_id})
    #             kyc_db_session.commit()

if __name__ == '__main__':
    print("=================Started ETL Jobs =================")
    reload(sys)
    sys.setdefaultencoding('utf-8')
    #TODO:: Regex Expression Validation for Core System Data
    # kenyan_pattern = "/(0|+?254)7(\d){8}/"
    # two_five_four_only_kenyan_pattern = "(\+254|^){1}[ ]?[7]{1}([0-3]{1}[0-9]{1})[ ]?[0-9]{3}[ ]?[0-9]{3}\z"
    # just_a_number_pattern = "^[0-9]*$"
    # kenyan_mobile_number_reg = re.compile(kenyan_pattern)
    # two_five_four_only_kenyan_mobile_number_reg = re.compile(two_five_four_only_kenyan_pattern)


    #update_old_pension_memb_no_to_new_memb_no()
    #load_isf_customer_data_into_kyc_db()
    #load_premia_customer_kra_pin_into_kyc_db()
    #load_premia_customer_phone_number_into_kyc_db()
    #load_premia_customer_landline_phone_number_into_kyc_db()
    #load_actisure_customer_data_into_kyc_db()
    #load_apex_customer_data_into_kyc_db()
    #load_premia_customer_id_numbers_into_kyc_db()
    #load_apex_customer_phone_data_into_kyc_db()
    #load_apex_customer_email_data_into_kyc_db()
    #load_apex_customer_income_tax_numbr_into_kyc_db()
    #load_apex_customer_id_numbers_into_kyc_db()
    #load_actisure_customer_id_numbers_into_kyc_db()
    # for offset in range(0, 50):#get_number_of_actisure_numbers()):
    #     print(offset)
    #     load_actisure_customer_id_numbers_into_kyc_db(offset)
    #load_actisure_customer_id_numbers_into_kyc_db()
    #load_actisure_customer_id_numbers_and_phone_numbers_into_kyc_db()
    #load_actisure_customer_passport_numbers_into_kyc_db()
    #detect_duplicate_national_ids_in_kyc_db_and_remove_them()
    #detect_duplicate_emails_in_kyc_db_and_remove_them()
    #detect_duplicate_phone_numbers_in_kyc_db_and_remove_them()
    #detect_duplicate_passport_numbers_in_kyc_db_and_remove_them()
    #detect_duplicate_apex_ids_in_kyc_db_and_remove_them()
    #load_jubilee_insure_products()
    #load_jubilee_insure_customers()
    # print (load_kyc_customer_details_from_premia())
    #load_kyc_customer_details_from_pension()
    #load_kyc_customer_details_from_isf()
    # for offset in range(30000, 40000):
    #       load_kyc_customer_details_from_isf(offset)
    #     load_kyc_customer_details_from_premia(offset)

    #load_kyc_customer_details_from_isf()
    #load_kyc_customer_details_from_premia()
    #load_kyc_customer_details_from_pension()
    #load_kyc_customer_details_from_actisure_failover()
    #load_kyc_policy_details_from_premia(50000, 60000)
    #load_kyc_policy_details_from_isf(0, 6000)
    #load_kyc_policy_details_from_actisure(50000, 60000)
    #load_kyc_policy_details_from_apex(8000, 18000)
    #load_customer_data_from_kyc_to_crm()
    #load_kyc_policy_details_from_premia()
    # for offset in range(51000, 55000):
    #     print("load_kyc_customer_details_from_actisure offset is " + str(offset))
    #     load_kyc_customer_details_from_actisure(offset)


    # for offset in range(0, 60800):
    #     print("load_kyc_customer_details_from_actisure_failover offset is " + str(offset))
    #     load_kyc_customer_details_from_actisure_failover(offset)

    #push_payment_details_from_isf_to_kyc_db(115000, 120000)



    # min_offset = 800000
    # max_offset = 900000
    # for offset in range(min_offset, max_offset):
    #     interval = max_offset - min_offset
    #     current_offset_pos = (offset + 1) - min_offset
    #     percentage = float(float(current_offset_pos) / float(interval) * 100)
    #     print ("clean_up_ids offset is %d of %d is %f " % (
    #     offset, max_offset, percentage)
    #            + "% complete")
    #
    #     kyc_payment_details = kyc_db_session.query(KYCPaymentDetails).offset(offset).limit(1).all()
    #     for kyc_payment_detail in kyc_payment_details:
    #         kyc_payment_detail_id = kyc_payment_detail.id
    #         if type(kyc_payment_detail_id) is int:
    #             print "offset is " + str(offset)
    #             payment_id = str(uuid.uuid4())
    #             print payment_id
    #             kyc_db_session.query(KYCPaymentDetails).filter(KYCPaymentDetails.id == kyc_payment_detail.id)\
    #                 .update({"id": payment_id})
    #             kyc_db_session.commit()

    # for offset in range(280000, 300000):
    #     print "offset is "+ str(offset)
    #     sms_proxies = online_bima_session.query(BulkSMS).offset(offset).limit(1).all()
    #     for sms_proxy in sms_proxies:
    #         print sms_proxy
    #
    #         phone_no = sms_proxy.phone_number
    #         # Look for the customer bearing this name
    #         customer = kyc_db_session.query(KYCCustomer).filter(or_(KYCCustomer.actisure_phone_no == phone_no,
    #                     KYCCustomer.isf_phone_no == phone_no,
    #                     KYCCustomer.apex_phone_no == phone_no,
    #                     KYCCustomer.premia_phone_no == phone_no)).first()
    #
    #         if customer is not None:
    #             print "found customer with phone number " + phone_no
    #             # Step One
    #             communications_account = CommunicationsAccount()
    #             communications_account.id = sms_proxy.id
    #             communications_account.account_id = customer.kyc_id
    #             communications_account.communication_id = sms_proxy.id
    #             suite_crm_db_session.add(communications_account)
    #             suite_crm_db_session.commit()
    #
    #             # Step Two
    #             communication = Communications()
    #             communication.id = sms_proxy.id
    #             communication.deleted = 0
    #             communication.mean_of_communication = "SMS Text Message"
    #             communication.date_of_communication = sms_proxy.delivery_timestamp
    #             communication.agent_serving_customer = "ISF System"
    #             communication.content_of_communication =  sms_proxy.text_content.replace("+", " ")
    #             communication.last_updated = datetime.utcnow()
    #             suite_crm_db_session.add(communication)
    #             suite_crm_db_session.commit()
    #

    # while True:
    #     communications = suite_crm_db_session.query(Communications).filter(Communications.content_of_communication.like("%+%")).offset(0).limit(3).all()
    #     for communication in communications:
    #         old_content =  communication.content_of_communication
    #         new_content = old_content.replace("+", " ")
    #         print new_content
    #         suite_crm_db_session.query(Communications).filter_by(id=communication.id).update({"content_of_communication": new_content})
    #         suite_crm_db_session.commit()


    #kyc_db_session.query(KYCPolicy).filter(KYCPolicy.status)

    def load_isf_premium_statements():
        '''JHLISFADMIN_LIFE_POLICIES.csv'''
        policy_amounts_dict = {}
        with open('JHLISFADMIN_LIFE_POLICIES.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:

                isf_id = row['N_CUST_REF_NO']
                policy_no = row['POLICY_NO']
                premium_amount = row['PREMIUM_AMT']
                premium_frequency = row['PREM_FREQUENCY']
                print 'policy_no ' + str(policy_no) + ' premium_amount ' + str(
                    premium_amount) + ' premium_frequency ' + str(premium_frequency)
                #if kyc_policy is not None:
                #print "found " + kyc_policy.policy_number
                try:
                    kyc_db_session.query(KYCPolicy).filter_by(policy_number=policy_no).update({"premium_amount": premium_amount, "premium_frequency": premium_frequency})
                    kyc_db_session.commit()
                except exc.DataError, e:
                    kyc_db_session.rollback()
                    print "Unable to update Policy " #+ str(kyc_policy.policy_no)
                    continue
                except exc.IntegrityError, e:
                    kyc_db_session.rollback()
                    print "Unable to update Policy " #+ str(kyc_policy.policy_no)
                    continue
                except exc.OperationalError, e:
                    kyc_db_session.rollback()
                    print "Unable to update Policy " #+ str(kyc_policy.policy_no)
                    continue
                # suite_crm_db_session.query(AccountCSTM).filter_by(id=kyc_policy.kyc_id).update({"total_premium_value": policy_amounts_dict[policy_no]})
                # suite_crm_db_session.commit()
                #
                # last_year_same_date = datetime.now() - relativedelta(years=1)
                #
                # # date_time1 = '2017-04-15 00:00:00'
                # # date_time2 = '2017-04-17 15:35:19+00:00'
                # # input_date_time = '2017-04-16 12:11:42+00:00'
                # #
                # # parsed1 = dateutil.parser.parse(date_time1)
                # parsed_receipt_date = dateutil.parser.parse(receipt_date).replace(tzinfo=None)
                # # input_parsed = dateutil.parser.parse(input_date_time).replace(tzinfo=None)
                #
                # if parsed_receipt_date >= last_year_same_date:
                #     print str(parsed_receipt_date)
                #     if policy_amounts_dict.has_key(policy_no):
                #         policy_amounts_dict[policy_no] = float(policy_amounts_dict[policy_no]) + float(amount)
                #     else:
                #         policy_amounts_dict[policy_no] = float(amount)
                # else:
                #     print "receipt date out of annum " + str(parsed_receipt_date)

        # for policy_no in policy_amounts_dict.keys():
        #     kyc_policy = kyc_db_session.query(KYCPolicy.kyc_id).filter(KYCPolicy.policy_number == policy_no).first()
        #     if kyc_policy is not None:
        #         if policy_amounts_dict.has_key(policy_no):
        #             try:
        #                 suite_crm_db_session.query(AccountCSTM).filter_by(id=kyc_policy.kyc_id).update({"total_premium_value": policy_amounts_dict[policy_no]})
        #                 suite_crm_db_session.commit()
        #             except exc.DataError, e:
        #                 suite_crm_db_session.rollback()
        #                 print "Unable to add Policy " #+ str(kyc_policy.policy_no)
        #                 continue
        #             except exc.IntegrityError, e:
        #                 suite_crm_db_session.rollback()
        #                 print "Unable to add Policy " #+ str(kyc_policy.policy_no)
        #                 continue
        #             except exc.OperationalError, e:
        #                 suite_crm_db_session.rollback()
        #                 print "Unable to add Policy " #+ str(kyc_policy.policy_no)
        #                 continue


                #
                # print policy_amounts_dict
                # kyc_customer = kyc_db_session.query(KYCCustomer).filter(KYCCustomer.premia_cust_code == sub_account).first()
                # if kyc_customer is not None:
                #         payment_id = str(uuid.uuid4())
                #         # Instantiate payment
                #         payments = Payments()
                #         payments.id = payment_id
                #         payments.deleted = 0
                #         payments.status = None
                #         payments.last_updated = datetime.utcnow()
                #         payments.receipt_number = receipt_no
                #         payments.amount = amount
                #         payments.policy_number = None
                #         payments.date_of_payment = receipt_date
                #         payments.kyc_id = kyc_customer.kyc_id
                #         payments.line_of_business = "GENERAL"
                #
                #         suite_crm_db_session.add(payments)
                #         suite_crm_db_session.commit()
                #
                #         payment_account = PaymentAccount()
                #         payment_account.id = str(uuid.uuid4())
                #         payment_account.account_id = kyc_customer.kyc_id
                #         payment_account.payment_id = payment_id
                #         suite_crm_db_session.add(payment_account)
                #         suite_crm_db_session.commit()
                #
                #         print kyc_customer

    def load_general_receipts():
        with open('general_receipts.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:

                receipt_no = row['FLEX03']
                receipt_date = row['DUE_DATE']
                sub_account = row['SUB_ACCOUNT']
                amount = row['AMOUNT']

                kyc_customer = kyc_db_session.query(KYCCustomer).filter(KYCCustomer.premia_cust_code == sub_account).first()
                if kyc_customer is not None:
                        payment_id = str(uuid.uuid4())
                        # Instantiate payment
                        payments = Payments()
                        payments.id = payment_id
                        payments.deleted = 0
                        payments.status = None
                        payments.last_updated = datetime.utcnow()
                        payments.receipt_number = receipt_no
                        payments.amount = amount
                        payments.policy_number = None
                        payments.date_of_payment = receipt_date
                        payments.kyc_id = kyc_customer.kyc_id
                        payments.line_of_business = "GENERAL"

                        suite_crm_db_session.add(payments)
                        suite_crm_db_session.commit()

                        payment_account = PaymentAccount()
                        payment_account.id = str(uuid.uuid4())
                        payment_account.account_id = kyc_customer.kyc_id
                        payment_account.payment_id = payment_id
                        suite_crm_db_session.add(payment_account)
                        suite_crm_db_session.commit()

                        print kyc_customer


    def load_pension_receipts():
        with open('pension_receipts.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                scheme_account = row['SCHEME_ACCOUNT']
                individual_account = row['INDIVIDUAL_ACCOUNT']
                receipt_no = row['RECEIPT_NO']
                receipt_date = row['RECEIPT_DATE']
                amount = row['AMOUNT']
                status = row['STATUS']

                pension_policy = kyc_db_session.query(KYCPolicy).filter(KYCPolicy.policy_number == individual_account).first()
                if pension_policy is not None:
                    payment_id = str(uuid.uuid4())
                    # Instantiate payment
                    payments = Payments()
                    payments.id = payment_id
                    payments.deleted = 0
                    payments.status = status
                    payments.last_updated = datetime.utcnow()
                    payments.receipt_number = receipt_no
                    payments.amount = amount
                    payments.policy_number = individual_account
                    payments.date_of_payment = receipt_date
                    payments.kyc_id = pension_policy.kyc_id
                    payments.line_of_business = "PENSIONS"

                    suite_crm_db_session.add(payments)
                    suite_crm_db_session.commit()

                    payment_account = PaymentAccount()
                    payment_account.id = str(uuid.uuid4())
                    payment_account.account_id = pension_policy.kyc_id
                    payment_account.payment_id = payment_id
                    suite_crm_db_session.add(payment_account)
                    suite_crm_db_session.commit()

                    print pension_policy

                # kyc_customer = kyc_db_session.query(KYCCustomer).filter(KYCCustomer.premia_id == assured_code).filter(
                #     KYCCustomer.premia_cust_code == None).first()
                # if kyc_customer is not None:
                #     kyc_db_session.query(KYCCustomer).filter_by(id=kyc_customer.id).update({"premia_cust_code": cust_code})
                #     kyc_db_session.commit()
                #     #
                #     # print cust_code
                #     print "cust_code is " + str(cust_code) + " assured_code is " + str(assured_code)


    def load_actisure_customer_details_to_failover_db():
        with open('actisure_customer_details.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                actisure_fail_over_customer = FailOverCustomerDetails()
                actisure_fail_over_customer.customer_id_actisure = row['ENTITYID']
                actisure_fail_over_customer.customer_name = row['BENEFICIARYNAME']
                actisure_fail_over_customer.policy_no = row['POLICYNUMBER']
                actisure_fail_over_customer.policy_status = row['POLICYSTATUS']
                actisure_fail_over_customer.policy_effective_date = row['EFFECTIVEDATE']
                actisure_fail_over_customer.policy_end_date = row['ENDDATE']
                actisure_fail_over_customer.policy_product = row['PRODUCT']
                actisure_fail_over_customer.email_address_actisure = row['EMAIL']
                actisure_fail_over_customer.phone_no_actisure = row['PHONENUMBER']
                actisure_fail_over_customer.income_tax_no = row['PINNO']
                actisure_fail_over_customer.date_of_birth = row['DOB']
                actisure_fail_over_customer.national_id_no = row['IDNO']
                actisure_fail_over_customer.agent_code = row['AGENTCODE']
                actisure_fail_over_customer.agent_name = row['AGENTNAME']
                actisure_fail_over_customer.gender = row['GENDER']
                actisure_fail_over_customer.relationship = row['RELATIONSHIP']
                actisure_fail_over_customer.relationshipid = row['RELATIONSHIPID']
                actisure_fail_over_customer.policyholdername = row['POLICYHOLDERNAME']
                actisure_fail_over_customer.policyholderid = row['POLICYHOLDERID']
                actisure_fail_over_customer.policyid = row['POLICYID']

                print(actisure_fail_over_customer.policyholdername)
                actisure_fail_over_db_session.add(actisure_fail_over_customer)
                actisure_fail_over_db_session.commit()

    def load_assured_code():
        with open('assr_cust.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                cust_code = row['CUST_CODE']
                assured_code = row['ASSR_CODE']

                kyc_customer = kyc_db_session.query(KYCCustomer).filter(KYCCustomer.premia_id == assured_code).filter(KYCCustomer.premia_cust_code == None).first()
                if kyc_customer is not None:
                    kyc_db_session.query(KYCCustomer).filter_by(id=kyc_customer.id).update({"premia_cust_code": cust_code})
                    kyc_db_session.commit()
                    #
                    # print cust_code
                    print "cust_code is " + str(cust_code) + " assured_code is " + str(assured_code)


    def load_jubilee_suitecrm_db_temp_payments():
        temp_payments = suite_crm_db_session.query(TempPayments).limit(1).all()
        for temp_payment in temp_payments:
            print temp_payment

    gpnt_statements = jhl_online_session.query(GPNTStatement).filter(GPNTStatement.email == "vick.otieno.2010@gmail.com").all()
    for gpnt_statement in gpnt_statements:

        registered_member_value = (gpnt_statement.n_member_op_reg or 0) + (gpnt_statement.n_member_op_reg_vol or 0) \
                       + (gpnt_statement.n_member_total_reg or 0) + (gpnt_statement.n_member_reg_transfer or 0)\
                       - (gpnt_statement.n_member_reg_expense or 0) + (gpnt_statement.n_member_op_reg_interest or 0)\
                       - (gpnt_statement.n_member_reg_rba_levy or 0)+(gpnt_statement.n_member_reg_interest or 0)

        unregistered_member_value = (gpnt_statement.n_member_op_unreg or 0)+(gpnt_statement.n_member_op_unreg_vol or 0)+(gpnt_statement.n_member_total_unreg or 0)+(gpnt_statement.n_member_unreg_transfer or 0)-(gpnt_statement.n_member_unreg_expense or 0)+(gpnt_statement.n_member_op_unreg_interest or 0)-(gpnt_statement.n_member_unreg_rba_levy or 0)+(gpnt_statement.n_member_unreg_interest or 0)+(gpnt_statement.n_member_unreg_tax or 0)

        total_member_value = registered_member_value + unregistered_member_value

        registered_employer_value = (gpnt_statement.n_employer_op_reg or 0)+(gpnt_statement.n_employer_op_reg_sp or 0)+(gpnt_statement.n_employer_total_reg or 0)+(gpnt_statement.n_employer_reg_transfer or 0)-(gpnt_statement.n_employer_reg_expense or 0)+(gpnt_statement.n_employer_op_reg_interest or 0)-(gpnt_statement.n_employer_reg_rba_levy or 0)+(gpnt_statement.n_employer_reg_interest or 0)
        unregistered_employer_value = (gpnt_statement.n_employer_op_unreg or 0)+(gpnt_statement.n_employer_op_unreg_sp or 0)+(gpnt_statement.n_employer_total_unreg or 0)+(gpnt_statement.n_employer_unreg_transfer or 0)-(gpnt_statement.n_employer_un_reg_expense or 0)+(gpnt_statement.n_employer_op_unreg_interest or 0)-(gpnt_statement.n_employer_unreg_rba_levy or 0)+(gpnt_statement.n_employer_unreg_interest or 0)

        print unregistered_employer_value

        #total_employer_value = registered_employer_value + unregistered_employer_value

        # with open('jubilee_suitecrm_db_temp_payments.csv') as csvfile:
        #     reader = csv.dictreader(csvfile)
        #     for row in reader:
        #         print row
#                 policy_number = row['GL201100000220']
#                 receipt_no = row['HO110000040']
#                 receipt_date = row['2011 - 03 - 31
#     00:00:00']
#                 amount = row['AMOUNT']
#                 status = row['STATUS']
#
#                 pension_policy = kyc_db_session.query(KYCPolicy).filter(
#                     KYCPolicy.policy_number == policy_number).first()
#                 if pension_policy is not None:
#                     payment_id = str(uuid.uuid4())
#
#
#                     '''
#                     [
#   {
#     "id": "f10b81f9-2c5e-11e7-be46-0242ac120003",
#     "name": null,
#     "date_entered": null,
#     "date_modified": null,
#     "modified_user_id": null,
#     "created_by": null,
#     "description": null,
#     "deleted": 0,
#     "assigned_user_id": null,
#     "status": "RE001",
#     "date_of_last_payment": null,
#     "mode_of_payment": null,
#     "next_pay_due_date": null,
#     "last_updated": null,
#     "receipt_number": "HO170001854",
#     "amount": "440.000000",
#     "policy_number": "133384",
#     "date_of_payment": null,
#     "kyc_id": "7807f6e7-319c-44eb-bb6c-3e7c5e3e8196",
#     "line_of_business": "LIFE",
#     "receipt_date": "2017-01-10 00:00:00",
#     "ilanga_receipt_no": "NHQ01236827",
#     "ilanga_receipt_date": "2016-12-05 00:00:00"
#   }
# ]
#
#                     '''
#
#
#                     # Instantiate payment
#                     payments = Payments()
#                     payments.id = payment_id
#                     payments.deleted = 0
#                     payments.status = status
#                     payments.last_updated = datetime.utcnow()
#                     payments.ilanga = receipt_no
#                     payments.receipt_number = receipt_no
#                     payments.receipt_date = receipt_date
#                     payments.ilanga_receipt_no = ilanga_receipt_no
#                     payments.ilanga_receipt_date = ilanga_receipt_date
#                     payments.amount = amount
#                     payments.policy_number = policy_number
#                     payments.date_of_payment = receipt_date
#                     payments.kyc_id = pension_policy.kyc_id
#                     payments.line_of_business = "LIFE"
#
#                     suite_crm_db_session.add(payments)
#                     suite_crm_db_session.commit()
#
#                     payment_account = PaymentAccount()
#                     payment_account.id = str(uuid.uuid4())
#                     payment_account.account_id = pension_policy.kyc_id
#                     payment_account.payment_id = payment_id
#                     suite_crm_db_session.add(payment_account)
#                     suite_crm_db_session.commit()
#
#                     print pension_policy
#
#     true
#     GL201100000220
#     HO110000040
#     2011 - 03 - 31
#     00:00:00
#     677781
#     NHQ00029962
#     2011 - 02 - 17
#     00:00:00
#     RE001
#     LIFE


    #load_pension_receipts()
    #load_general_receipts()
    #load_jubilee_suitecrm_db_temp_payments()

    # premia_policies = isf_uat_db_session.query(LifePolicy).limit(150).all()
    # for policy in premia_policies:
    #     print policy
    #load_missing_isf_policies_into_kyc_db(0, 100)
    #
    # raf_customers = refer_a_friend_db_session.query(RAFFriend).all()
    # for raf_customer in raf_customers:
    #     print str(raf_customer.name).upper()

    # for offset in range(2000, 3000):
    #     transactions = mpesa_api_session.query(Transaction).limit(1).offset(offset).all()
    #     for transaction in transactions:
    #         print str(transaction.bill_reference_number).upper()
    #
    #         mpesa_api_session.query(Transaction).filter_by(id=transaction.id).update(
    #             {
    #                 "bill_reference_number": str(transaction.bill_reference_number).upper()
    #              })
    #         mpesa_api_session.commit()
    #
    # for offset in range(1230, 1650):
    #     receipts = mpesa_api_session.query(Receipt).limit(1).offset(offset).all()
    #     for receipt in receipts:
    #         print parser.parse(receipt.transaction_time)
    #
    #         mpesa_api_session.query(Receipt).filter_by(id=receipt.id).update(
    #             {
    #                 "transaction_datetime": parser.parse(receipt.transaction_time)
    #             })
    #         mpesa_api_session.commit()

        #date_string = float(transaction.transaction_time[8])
        #print datetime.fromtimestamp(date_string)
        #s_datetime = datetime.date(date_string[:3], date_string[4:6], date_string[6:8])
        #print date_string[:3], date_string[4:6], date_string[6:8]


    # user_metas = jcare_session.query(UserMeta).all()
    # for user_meta in user_metas:
    #     old_user_id = user_meta.user_id
    #     new_user_id = user_meta.user_id+100
    #     print new_user_id
    #     jcare_session.query(UserMeta).filter_by(id=user_meta.id).update(
    #         {
    #             "user_id": new_user_id,
    #             "updated_at": datetime.utcnow(),
    #          })
    #     jcare_session.commit()
    #
    # users = jcare_session.query(Dependents).all()
    # new_users = {}
    # user_mappings = []
    # for user in users:
    #     old_user_id = user.principal_id
    #     new_user_id = user.principal_id+400
    #
    #     new_users[old_user_id] = new_user_id
    #     user_mappings.append({
    #     'id': user.id,
    #     'principal_id': new_user_id,  # This is pk?
    #     'updated_at': datetime.utcnow(),
    #     })
    #
    # print user_mappings
    #
    # jcare_session.bulk_update_mappings(Dependents, user_mappings)
    # jcare_session.commit()

    # for old_user_id in new_users:
    #     new_user_id = new_users[old_user_id]
    #     print new_user_id
    #     jcare_session.query(JCareQuotes).filter_by(id=old_user_id).update(
    #         {
    #             "user_id": new_user_id,
    #             "updated_at": datetime.utcnow(),
    #          })
    #     jcare_session.commit()



    # User, UserMeta, JCareQuotes, Dependents

    # pension_policies = MedicalPolicy.query.limit(10).all()
    # for medical_policy in pension_policies:
    #     print medical_policy

    #load_apex_missing_customer_id_number_into_kyc_db()
    load_isf_premium_statements()
    # premium_statements = PremiumStatement.query.limit(10).all()
    # for premium_statement in premium_statements:
    #     print premium_statement


    # min_offset = 1001
    # max_offset = 2000

    # min_offset = 40000
    # max_offset = 50000

    # for offset in range(min_offset, max_offset):
    #     interval = max_offset - min_offset
    #     current_offset_pos = (offset + 1) - min_offset
    #     percentage = float(float(current_offset_pos) / float(interval) * 100)
    #     print ("load actisure_fail_over_db_session offset is %d of %d is %f " % (
    #     offset, max_offset, percentage)
    #            + "% complete")
    #
    #     load_actisure_cust_details = actisure_db_session.query(MedicalPolicy).offset(offset).limit(1).all()
    #     for detail in load_actisure_cust_details:
    #         print detail.customer_id_actisure
    #
    #         fail_over_medical_policy = FailOverMedicalPolicy()
    #         fail_over_medical_policy.customer_id_actisure = detail.customer_id_actisure
    #         fail_over_medical_policy.customer_name = detail.customer_name
    #         fail_over_medical_policy.policy_no = detail.policy_no
    #         fail_over_medical_policy.policy_status = detail.policy_status
    #         fail_over_medical_policy.policy_effective_date = detail.policy_effective_date
    #         fail_over_medical_policy.policy_end_date = detail.policy_end_date
    #         fail_over_medical_policy.policy_product = detail.policy_product
    #         fail_over_medical_policy.email_address_actisure = detail.email_address_actisure
    #         fail_over_medical_policy.phone_no_actisure = detail.phone_no_actisure
    #         fail_over_medical_policy.date_of_birth = detail.date_of_birth
    #         fail_over_medical_policy.national_id_no = detail.national_id_no
    #         fail_over_medical_policy.agent_code = detail.agent_code
    #         fail_over_medical_policy.agent_name = detail.agent_name
    #         fail_over_medical_policy.gender = detail.gender
    #         actisure_fail_over_db_session.add(fail_over_medical_policy)
    #         actisure_fail_over_db_session.commit()

    # min_offset = 7000000
    # max_offset = 8000000
    # for offset in range(min_offset, max_offset):
    #     interval = max_offset - min_offset
    #     current_offset_pos = (offset + 1) - min_offset
    #     percentage = float(float(current_offset_pos) / float(interval) * 100)
    #
    #     print ("load actisure_fail_over_db_session medical_info offset is %d of %d is %f " % (
    #     offset, max_offset, percentage)
    #            + "% complete")
    #
    #     load_actisure_cust_details = actisure_db_session.query(MedicalInformation).offset(offset).limit(1).all()
    #     for detail in load_actisure_cust_details:
    #         print detail.member_no
    #
    #         fail_over_medical_policy = FailOverMedicalInformation()
    #         fail_over_medical_policy.member_no = detail.member_no
    #         fail_over_medical_policy.policy_status = detail.policy_status
    #         fail_over_medical_policy.policy_start_date = detail.policy_start_date
    #         fail_over_medical_policy.policy_class = detail.policy_class
    #         fail_over_medical_policy.renewal_date = detail.renewal_date
    #         fail_over_medical_policy.scope_of_benefit = detail.scope_of_benefit
    #         fail_over_medical_policy.cover_limit = detail.cover_limit
    #         actisure_fail_over_db_session.add(fail_over_medical_policy)
    #         actisure_fail_over_db_session.commit()

    # min_offset = 42545
    # max_offset = 43545
    # for offset in range(min_offset, max_offset):
    #     interval = max_offset - min_offset
    #     current_offset_pos = (offset + 1) - min_offset
    #     percentage = float(float(current_offset_pos) / float(interval) * 100)
    #     print ("load actisure_fail_over_db_session medical_info offset is %d of %d is %f " % (
    #     offset, max_offset, percentage)
    #            + "% complete")
    #
    #     load_actisure_cust_details = actisure_db_session.query(MedicalInformation).offset(offset).limit(1).all()
    #     for detail in load_actisure_cust_details:
    #         print detail.member_no
    #
    #         fail_over_medical_policy = FailOverMedicalInformation()
    #         fail_over_medical_policy.member_no = detail.member_no
    #         fail_over_medical_policy.policy_status = detail.policy_status
    #         fail_over_medical_policy.policy_start_date = detail.policy_start_date
    #         fail_over_medical_policy.policy_class = detail.policy_class
    #         fail_over_medical_policy.renewal_date = detail.renewal_date
    #         fail_over_medical_policy.scope_of_benefit = detail.scope_of_benefit
    #         fail_over_medical_policy.cover_limit = detail.cover_limit
    #         actisure_fail_over_db_session.add(fail_over_medical_policy)
    #         actisure_fail_over_db_session.commit()

        # load_actisure_cust_details = actisure_fail_over_db_session.query(FailOverMedicalPolicy).limit(500).all()
        # for detail in load_actisure_cust_deta
    # ils:
        #     print detail.customer_id_actisure

    # fakes = actisure_fail_over_db_session.query(FailOverMedicalInformation).filter(FailOverMedicalInformation.policy_class == 'J-Care').all()
    # for fake in fakes:
    #     print fake.member_no
    #     kyc_cust = kyc_db_session.query(KYCCustomer).filter(KYCCustomer.actisure_id == fake.member_no).first()
    #     if kyc_cust is not None :
    #         if kyc_cust.actisure_phone_no is not None:
    #             print kyc_cust.actisure_phone_no
    #     load_actisure_cust_details = actisure_fail_over_db_session.query(FailOverMedicalPolicy).limit(500).all()
    #     #for detail in load_actisure_cust_details:
    #         #print detail.customer_id_actisure
    #
    #
    # #load_actisure_customer_data_into_kyc_db()
    # load_jcare_actisure_customer_data_into_kyc_db()

    # actisure_prodcution_medical_policies = actisure_production_db_session.query(ProdMedicalPolicy).limit(10).all()
    # for policy in actisure_prodcution_medical_policies:
    #     print policy.customer_id_actisure

    #load_actisure_customer_details_to_kyc_db()
    #load_actisure_failover_customer_data_into_kyc_db()
    #load_actisure_customer_details_to_failover_db()
    print("=================Finished ETL Jobs=================")


