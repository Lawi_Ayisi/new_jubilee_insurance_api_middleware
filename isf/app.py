#!/usr/bin/env python

from flask import Flask
from flask_graphql import GraphQLView

from models.database import isf_db_session
from isf_schema import isf_schema

app = Flask(__name__)
app.debug = True

default_query = '''
{
  allEmployees {
    edges {
      node {
        id,
        name,
        department {
          id,
          name
        },
        role {
          id,
          name
        }
      }
    }
  }
}'''.strip()


customer_details_query = '''{
  customerDetails(policyNo: "IL201300328596") {
    edges {
      node {
        policyNo
        phoneNumber
        customerName
        email
        kraPin
        nationalId
        dateOfBirth
        customerId
      }
    }
  }
}
'''.strip()


app.add_url_rule('/isf_graphql', view_func=GraphQLView.as_view('graphql', schema=isf_schema, graphiql=True))

@app.teardown_appcontext
def shutdown_session(exception=None):
    isf_db_session.remove()


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)
