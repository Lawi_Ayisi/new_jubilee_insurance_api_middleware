import graphene
from graphene import resolve_only_args, relay
from graphene_sqlalchemy import SQLAlchemyConnectionField, SQLAlchemyObjectType
from models import CustomerDetails as CustomerDetailsModel

def connection_for_type(_type):
    class Connection(graphene.Connection):
        total_count = graphene.Int()

        class Meta:
            name = _type._meta.name + 'Connection'
            node = _type

        def resolve_total_count(self, args, context, info):
            return self.length

    return Connection

class CustomerDetails(SQLAlchemyObjectType):

    class Meta:
        model = CustomerDetailsModel
        interfaces = (relay.Node, )


class Query(graphene.ObjectType):
    customer_details = SQLAlchemyConnectionField(CustomerDetails,  policy_no=graphene.String())
    customer_detail = relay.Node.Field(CustomerDetails)
    node = relay.Node.Field()
    get_customer_detail = relay.Node.Field(CustomerDetails, policy_no=graphene.String())

    def resolve_customer_details(self, args, context, info):
        query = CustomerDetails.get_query(context) # SQLAlchemy query
        policy_number = args.get('policy_no')
        if policy_number is None:
            return query.all()
        else:
            return query.filter_by(policy_no=policy_number).all()

isf_schema = graphene.Schema(query=Query, types=[CustomerDetails])
